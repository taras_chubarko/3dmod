<?php

use Modules\Groups\Mutations;
use Modules\Groups\Queries;
use Modules\Groups\Types;
use Modules\Groups\Enums;

return [

    // The prefix for routes
    'prefix'      => 'graphql',

    // The controller to use in GraphQL request. Either a string that will apply
    // to both query and mutation or an array containing the key 'query' and/or
    // 'mutation' with the according Controller and method
    //
    // Example:
    //
    // 'controllers' => [
    //     'query' => '\M1naret\GraphQL\GraphQLController@query',
    //     'mutation' => '\M1naret\GraphQL\GraphQLController@mutation'
    // ]
    //
    'controllers' => \M1naret\GraphQL\GraphQLController::class . '@query',

    // Any middleware for the graphql route group
    'middleware'  => [],

    'default_schema' => 'default',

    'schemas' => [

        'default' => [
            'query'      => [
                #region Groups module
                Queries\GroupsQuery::NAME  => Queries\GroupsQuery::class,
                Queries\FriendsQuery::NAME => Queries\FriendsQuery::class,
                Queries\InvitesQuery::NAME => Queries\InvitesQuery::class,
                #endregion
            ],
            'mutation'   => [
                #region Groups module
                Mutations\GroupsMutation::NAME               => Mutations\GroupsMutation::class,
                Mutations\SendInvitesMutation::NAME          => Mutations\SendInvitesMutation::class,
                Mutations\Invites\AcceptNotifyMutation::NAME => Mutations\Invites\AcceptNotifyMutation::class,
                Mutations\Invites\CancelNotifyMutation::NAME => Mutations\Invites\CancelNotifyMutation::class,
                #endregion
            ],
            'middleware' => [
                'auth.apikey',
            ],
        ],
    ],

    'types' => [
        #region Enums
        Enums\GraphQL\GroupTypesGraphQLEnum::NAME   => Enums\GraphQL\GroupTypesGraphQLEnum::class,
        Enums\GraphQL\GroupInvitesGraphQLEnum::NAME => Enums\GraphQL\GroupInvitesGraphQLEnum::class,
        Enums\GraphQL\ViewTypeGraphQLEnum::NAME     => Enums\GraphQL\ViewTypeGraphQLEnum::class,
        Enums\GraphQL\InviteTypesGraphQLEnum::NAME  => Enums\GraphQL\InviteTypesGraphQLEnum::class,
        #endregion

        #region Groups module
        Types\GroupType::NAME                       => Types\GroupType::class,
        Types\UserType::NAME                        => Types\UserType::class,
        Types\FileType::NAME                        => Types\FileType::class,
        Types\InviteType::NAME                      => Types\InviteType::class,
        #endregion
    ],

    'error_formatter' => [
        \M1naret\GraphQL\Error\ErrorFormatter::class,
        'format',
    ],

    /*
     * Options to limit the query complexity and depth. See the doc
     * @ https://github.com/webonyx/graphql-php#security
     */
    'security'        => [
        'query_max_complexity'  => null,
        'query_max_depth'       => null,
        'disable_introspection' => false,
    ],

    'variables_key' => 'variables',
];
