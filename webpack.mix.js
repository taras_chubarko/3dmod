let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/themes/site/assets/js')
   .sass('resources/assets/sass/app.scss', 'public/themes/site/assets/css');
   
mix.js('resources/assets/js/admin/admin.js', 'public/themes/admin/assets/js')
   .sass('resources/assets/sass/admin.scss', 'public/themes/admin/assets/css');

// mix.scripts([
//     'public/themes/site/assets/libs/owlcarousel/owl.carousel.min.js',
// ], 'public/themes/admin/assets/js/all.js');
   
mix.options({
    processCssUrls: false
});
