@if ( Session::has('errors') )
    <notification :type="'error'" :title="'Ошибка!'" :text="'{!! $errors->all() !!}'"></notification>
@endif

@if ( Session::has('success') )
    <notification :type="'success'" :title="'Отлично!'" :text="'{!! Session::get('success') !!}'"></notification>
@endif

@if ( Session::has('warning') )
    <notification :type="'warning'" :title="'Внимание!'" :text="'{!! Session::get('warning') !!}'"></notification>
@endif

@if ( Session::has('error') )
    <notification :type="'error'" :title="'Ошибка!'" :text="'{!! Session::get('error') !!}'"></notification>
@endif

@if ( Session::has('info') )
    <notification :type="'info'" :title="'Внимание!'" :text="'{!! Session::get('info') !!}'"></notification>
@endif

