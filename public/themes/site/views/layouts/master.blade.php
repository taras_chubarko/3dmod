<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name') }}</title>
    <script>
        if (location.protocol !== "https:") location.protocol = "https:";
    </script>
    <link href="/favicon.ico" type="image/x-icon" rel="icon" />
    <link href="{{ mix('/themes/site/assets/css/app.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>

<div id="app">
    <div class="wrapper">
        <header class="header" :class="headerClass" :style="{backgroundImage: bg}" :data-canvas-image="bg">
            <header-top></header-top>
            <header-center></header-center>
            <header-catalog></header-catalog>
            <component :is="currentBlock"></component>
        </header>
        <!-- .header-->
        <div class="content">
            <router-view></router-view>
            <vue-progress-bar></vue-progress-bar>
            <notifications classes="cp"/>
        </div>
        <!-- .content -->
    </div>
    <!-- .wrapper -->
    <footer class="footer">
        <footers></footers>
    </footer>
    <!-- .footer -->
</div>


<script>
    @if(auth()->check())
        window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
        'user' => User::get(),
    ]); ?>
    @else
        window.Laravel = null;
    @endif
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>
<script id="goo" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-D9ab5aN_5JZGP05u5izNtgvS0c-gezI&libraries=places&language=uk"></script>
<script src="{{ mix('/themes/site/assets/js/app.js') }}"></script>
<script src="//ulogin.ru/js/ulogin.js?lang=en"></script>
</body>

</html>
