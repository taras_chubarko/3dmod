@extends('site::mail.main')

@section('title')
Новий користувач
@stop

@section('content')
<p><b>E-mail:</b> {{ $user->email }}</p>
@stop