<!DOCTYPE html>
<html lang="ru">
	<head>
            <meta charset="utf-8">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>{{ config('app.name') }}</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
	    <tr>
                <td style="height: 50px; background: #212121; padding: 10px 20px; text-align:center;">
			<a class="pull-left" href="{{ config('app.url') }}" id="logo">
			    <img alt="" src="{{ config('app.url') }}/themes/site/assets/img/logo_white.png">
			</a>
                </td>
            </tr>
            <tr>
                <td style="height: 50px; background: #434444; color: #fff; padding: 0 20px;">
                    <h1 style="text-align: center;">
                        @yield('title')
                    </h1>
                </td>
            </tr>
            <tr>
                <td style="min-height: 200px; background: #fff; padding: 50px; vertical-align: top;">
			@yield('content')
			<br>
			<br>
			<br>
			
                </td>
            </tr>
            <tr>
                <td style="height: 50px; background: #212121; color: #fff; text-align: center;  padding: 0 20px;">
                    © {{ date('Y') }}, {{ config('app.name') }}
                </td>
            </tr>
	</table>	    
	</body>
</html>