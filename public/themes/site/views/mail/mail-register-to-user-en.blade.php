@extends('site::mail.main')

@section('title')
Thank you for registering!
@stop

@section('content')
    <p>Hello!</p>
    <br>
    <p>Congratulations on successful registration on the site {{ config('app.name') }}.</p>
    <br>
    <p>your login: {{ $user->email }}</p>
    @if(!empty($request->password))
        <p>your password: {{ $request->password }}</p>
    @endif
    <br>
@stop