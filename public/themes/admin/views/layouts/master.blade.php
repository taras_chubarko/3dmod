<!doctype html>
<html lang="uk">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>3D Models - Admin</title>
    <link href="{{ mix('/themes/admin/assets/css/admin.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <main-section></main-section>
</div>
<script>
    @if(auth()->check())
        window.Laravel = <?php echo json_encode([
        'csrfToken' => csrf_token(),
        'user' => User::get(),
    ]); ?>
            @else
        window.Laravel = null;
    @endif
</script>

<script src="{{ mix('/themes/admin/assets/js/admin.js') }}"></script>
</body>
</html>
