<?php

namespace Modules\Site\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SiteRepository
 * @package namespace App\Repositories;
 */
interface SiteRepository extends RepositoryInterface
{
    /*
     *
     */
    public function setAliases();
    /*
     *
     */
    public function mb_ucfirst($str, $encoding, $lower_str_end);
    //
    public function fakeTpl($tpl);

}
