<?php

namespace Modules\Site\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Site\Repositories\SiteRepository;
use Modules\Site\Entities\Site;
use Modules\Site\Entities\CityRegion;
use Modules\Site\Entities\TariffRegion;
use Faker\Factory as Faker;

/**
 * Class SiteRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SiteRepositoryEloquent extends BaseRepository implements SiteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Site::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /* public function setAliases
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setAliases()
    {
        if(\Request::segment(1) == 'admin')
        {
            return 'admin/*';
        }
        elseif(\Request::segment(1) == 'captcha')
        {
            return 'captcha/*';
        }
        elseif(\Request::segment(1) == 'api')
        {
            return 'api/*';
        }
        elseif(\Request::segment(1) == 'data')
        {
            return 'data/*';
        }
        elseif(\Request::segment(1) == 'sw')
        {
            return 'sw/*';
        }
        elseif(\Request::segment(1) == 'node_modules')
        {
            return 'node_modules/*';
        }
        elseif(\Request::segment(1) == 'broadcasting')
        {
            return 'broadcasting/*';
        }
        elseif(\Request::segment(1) == 'taxonomy')
        {
            return 'taxonomy/*';
        }
        elseif(\Request::segment(1) == 'filem')
        {
            return 'filem/*';
        }
        elseif(\Request::segment(1) == 'image')
        {
            return 'image/*';
        }
        elseif(\Request::segment(1) == 'socket.io')
        {
            return 'socket.io/*';
        }
        elseif(\Request::segment(1) == 'graphql')
        {
            return 'graphql/*';
        }
        else
        {
            return '[\/\w\.-]*';
        }
    }
    /* public function mb_ucfirst
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
        $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
        $str_end = "";
        if ($lower_str_end) {
          $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        }
        else {
          $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }
        $str = $first_letter . $str_end;
        return $str;
    }

    /* public function fakeTpl($tpl)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function fakeTpl($tpl)
    {
        $faker = Faker::create('uk_UA');
        \Carbon::setLocale('uk');
        
        $name = $faker->lastName.' '.$faker->firstName;

        switch ($tpl)
        {
            case 'model':

                $countIm = $faker->numberBetween(1, 10);
                $images = [];
                for ($x=0; $x < $countIm; $x++)
                {
                    $images[$x] = route('fake.image', ['480x360', $faker->numberBetween(1, 151)]);
                }

                $item = [
                    'id' => uniqid(),
                    'tpl' => $tpl,
                    'img' => route('fake.image', ['217x160', $faker->numberBetween(1, 151)]),
                    'division' => $faker->randomElement(['midi', 'maxi', 'mini', 'ultra', 'free']),
                    'name' => $faker->sentence(3),
                    'category' => $faker->word,
                    'downloads' => $faker->numberBetween(0, 500),
                    'comments' => $faker->numberBetween(0, 500),
                    'views' => $faker->numberBetween(0, 500),
                    'likes' => $faker->numberBetween(0, 500),
                    'price' => $faker->numberBetween(0, 500),
                    'user' => [
                        'link' 	=> '/profile',
                        'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                        'name' 	=> $name,
                    ],
                    'images' => $images,
                    'currentImage' => 1,
                    'rating' => $faker->numberBetween(0, 100),
                ];

                break;
            //
            case 'image':

                $images[0] = route('fake.image', ['480x360', $faker->numberBetween(1, 151)]);

                $item = [
                    'id' => uniqid(),
                    'tpl' => $tpl,
                    'img' => route('fake.image', ['217x160', $faker->numberBetween(1, 151)]),
                    'division' => $faker->randomElement(['midi', 'maxi', 'mini', 'ultra', 'free']),
                    'name' => $faker->sentence(3),
                    'category' => $faker->word,
                    'downloads' => $faker->numberBetween(0, 500),
                    'comments' => $faker->numberBetween(0, 500),
                    'views' => $faker->numberBetween(0, 500),
                    'likes' => $faker->numberBetween(0, 500),
                    'price' => $faker->numberBetween(0, 500),
                    'user' => [
                        'link' 	=> '/profile',
                        'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                        'name' 	=> $name,
                    ],
                    'images' => $images,
                    'currentImage' => 1,
                    'rating' => $faker->numberBetween(0, 100),
                ];


                break;

            case 'video':

                $num_video = $faker->numberBetween(1, 5);

                $item = [
                    'id' => uniqid(),
                    'tpl' => $tpl,
                    'img' => route('filem.filem', ['default', '217x160', 'default_video.png']),
                    'division' => $faker->randomElement(['midi', 'maxi', 'mini', 'ultra', 'free']),
                    'name' => $faker->sentence(3),
                    'category' => $faker->word,
                    'downloads' => $faker->numberBetween(0, 500),
                    'comments' => $faker->numberBetween(0, 500),
                    'views' => $faker->numberBetween(0, 500),
                    'likes' => $faker->numberBetween(0, 500),
                    'price' => $faker->numberBetween(0, 500),
                    'user' => [
                        'link' 	=> '/profile',
                        'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                        'name' 	=> $name,
                    ],
                    'video' => [
                        'url' => route('fake.video', $num_video),
                        'duration' => get_duration('/video/any/'.$num_video.'.mp4'),
                        'pic' => route('fake.video.thumbnail', $num_video),
                    ],
                    'currentImage' => 1,
                    'rating' => $faker->numberBetween(0, 100),
                ];

                break;

            case 'audio':

                $num_audio = $faker->numberBetween(1, 5);

                $item = [
                    'id' => uniqid(),
                    'tpl' => $tpl,
                    'img' => route('fake.image', ['217x160', $faker->numberBetween(1, 151)]),
                    'division' => $faker->randomElement(['midi', 'maxi', 'mini', 'ultra', 'free']),
                    'name' => $faker->sentence(3),
                    'category' => $faker->word,
                    'downloads' => $faker->numberBetween(0, 500),
                    'comments' => $faker->numberBetween(0, 500),
                    'views' => $faker->numberBetween(0, 500),
                    'likes' => $faker->numberBetween(0, 500),
                    'price' => $faker->numberBetween(0, 500),
                    'user' => [
                        'link' 	=> '/profile',
                        'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                        'name' 	=> $name,
                    ],
                    'audio' => [
                        'url' => route('fake.audio', $num_audio),
                        'duration' => '0:00',
                        'pic' => 'https://3dmodels.progim.net/uploads/default/picaudio.png',
                    ],
                    'currentImage' => 1,
                    'rating' => $faker->numberBetween(0, 100),
                ];

                break;

            default: $item = [];
        }
        return $item;
    }

    
    
}
