<?php

namespace Modules\Site\Entities;

use Illuminate\Database\Eloquent\Model;

class TariffRegion extends Model
{
    protected $table = 'tariffregion';

    protected $connection = 'mysql';
    
    protected $fillable = [];
    
    protected $guarded = ['_token'];
}
