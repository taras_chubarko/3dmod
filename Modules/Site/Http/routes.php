<?php
/*
 *
 */
Route::group(['middleware' => 'web', 'namespace' => 'Modules\Site\Http\Controllers'], function()
{
    Route::get('/{vue?}', 'SiteController@index')->where('vue', Site::setAliases());
});
/*
 *
 */

Route::group(['middleware' => 'auth.apikey', 'prefix' => 'api/v1', 'namespace' => 'Modules\Site\Http\Controllers'], function()
{
    Route::post('get/catalog', [
        'as' => 'get.catalog',
        'uses' => 'SiteController@get_catalog'
    ]);
    
    Route::post('get/top/disainers', [
        'as' => 'get.top.disainers',
        'uses' => 'SiteController@get_top_disainers'
    ]);
    
    Route::post('get/block/work', [
        'as' => 'get.block.work',
        'uses' => 'SiteController@get_block_work'
    ]);
    
    
    Route::post('get/block/new', [
        'as' => 'get.block.new',
        'uses' => 'SiteController@get_block_new'
    ]);
    
    Route::post('get/block/group', [
        'as' => 'get.block.group',
        'uses' => 'SiteController@get_block_group'
    ]);
    
    Route::post('get/block/gallery', [
        'as' => 'get.block.gallery',
        'uses' => 'SiteController@get_block_gallery'
    ]);

    Route::get('get/vid/diyalnosti', [
        'as' => 'get.vid.diyalnosti',
        'uses' => 'SiteController@get_vid_diyalnosti'
    ]);

    Route::get('get/po', [
        'as' => 'get.po',
        'uses' => 'SiteController@get_po'
    ]);

    Route::post('get/geo', [
        'as' => 'get.vid.diyalnosti',
        'uses' => 'SiteController@get_geo'
    ]);

    Route::post('get/last-product-in-category/{id}', [
        'uses' => 'SiteController@get_last_product_in_category'
    ]);
    
    
});







