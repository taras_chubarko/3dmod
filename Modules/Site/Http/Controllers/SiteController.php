<?php

namespace Modules\Site\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Site\Repositories\SiteRepository;
use Modules\Site\Entities\CityRegion;
use Ixudra\Curl\Facades\Curl;
use Faker\Factory as Faker;

class SiteController extends Controller
{
    /*
     *
     */
    protected $site;
    /*
     *
     */
    public function __construct(SiteRepository $site)
    {
        $this->site = $site;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('site::layouts.master');
        //return view('test');
    }
    /* public function get_catalog
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_catalog()
    {
        $items = \Catalog::treeMenu('5b052be6bd302512873e20b2', null, ['status' => true]);
        return response()->json($items, 200);

    }
    /* public function get_top_disainers
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_top_disainers()
    {
        $faker = Faker::create('uk_UA');
        $data = [];
        foreach(range(1, 3) as $i)
        {
            $images = [];
            foreach(range(1, 4) as $y)
            {
                $images[] = 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg';
            }


            $data[] = [
                'user' => [
                    'name' => $faker->lastName.' '.$faker->firstName,
                    'avatar' => 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                ],
                'images' => $images,
                'flag' => $faker->numberBetween(0, 1000),
                'views' => $faker->numberBetween(0, 1000),
                'likes' => $faker->numberBetween(0, 1000),
                'follows' => $faker->numberBetween(0, 1000),
                'positiops' => $faker->numberBetween(0, 1000),
            ];
        }
        return response()->json($data, 200);
    }
    /* public function get_block_work
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_block_work()
    {
        $faker = Faker::create('uk_UA');
        $data = [];

        foreach(range(1, 3) as $i)
        {
            $frilancers[] = [
                'name' => $faker->lastName.' '.$faker->firstName,
                'avatar' => 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                'models' => $faker->numberBetween(0, 1000),
                'reputation' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100),
            ];

            $works[] = [
                'user' => [
                    'name' => $faker->lastName.' '.$faker->firstName,
                    'avatar' => 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'reputation' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100),
                    'last_visit' => $faker->numberBetween(1, 55),
                ],
                'work' => [
                    'title' => $faker->sentence(3),
                    'price' => [
                        'min' => $faker->numberBetween(1, 100),
                        'max' => $faker->numberBetween(100, 900),
                    ],
                ],
            ];
        }

        $data['frilancers'] = $frilancers;
        $data['works'] = $works;

        return response()->json($data, 200);
    }
    /* public function get_block_new
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_block_new(Request $request)
    {
        $faker = Faker::create('uk_UA');

        $tpls = [
            'model',
            'image',
            'audio',
            'video'
        ];

        foreach(range(1, 8) as $i)
        {
            $tpl[] = $this->site->fakeTpl($faker->randomElement($tpls));
            $faker->unique(true);
        }
        return response()->json($tpl, 200);
    }
    /* public function get_block_group
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_block_group(Request $request)
    {
        $faker = Faker::create('uk_UA');

        \Carbon::setLocale('uk');

        foreach(range(1, 5) as $y)
        {
            $name = $faker->lastName.' '.$faker->firstName;
            $uchashyki[] = [
                'link' 	=> '/profile',
                'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                'name' 	=> $name,
            ];
        }


        foreach(range(1, 3) as $i)
        {
            $g1[1] = 'Приватна';
            $g1[2] = 'Публічна';
            $gIco[1] = 'models-svg-176';
            $gIco[2] = 'models-svg-175';

            $g = $faker->numberBetween(1, 2);

            $tpl[] = [
                'tpl' => 'group',
                'img' => route('fake.image', ['227x250', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'subname' => $faker->sentence(3),
                'uchashykiv' => $faker->numberBetween(0, 5000),
                'users' => $uchashyki,
                'group' => [
                    'id' => $g,
                    'name' => $g1[$g],
                    'ico'  => $gIco[$g],
                ],
                'join' => ($g == 2) ? 'join' : null,
                'views' => $faker->numberBetween(1, 1000),
                'likes' => $faker->numberBetween(1, 1000),
                'follows' => $faker->numberBetween(1, 1000),
            ];
        }
        return response()->json($tpl, 200);
    }
    /* public function get_block_gallery
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_block_gallery(Request $request)
    {
        $faker = Faker::create('uk_UA');

        \Carbon::setLocale('uk');
        foreach(range(1, 8) as $i)
        {

            $countIm = $faker->numberBetween(1, 10);
            $images = [];
            for ($x=0; $x < $countIm; $x++)
            {
                $images[$x] = route('fake.image', ['480x360', $faker->numberBetween(1, 151)]);
            }

            $name = $faker->lastName.' '.$faker->firstName;
            $tpl[] = [
                'id' => uniqid(),
                'tpl' => 'gallery',
                'img' => route('fake.image', ['217x160', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'category' => $faker->word,
                'scale' => '1:'.$countIm,
                'downloads' => $faker->numberBetween(0, 500),
                'comments' => $faker->numberBetween(0, 500),
                'views' => $faker->numberBetween(0, 500),
                'likes' => $faker->numberBetween(0, 500),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'images' => $images,
                'currentImage' => 1,
            ];
        }
        return response()->json($tpl, 200);
    }

    /* public function get_vid_diyalnosti
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_vid_diyalnosti()
    {
        $items =  \TaxonomyTerm::getOptGroup(2, null, ['status' => 1]);
        return response()->json($items, 200);

    }

    /* public function get_geo
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_geo(Request $request)
    {
        $response = \Curl::to('http://api.sypexgeo.net/json/'.$request->ip())
            ->asJson()
            ->get();
        //
        if($response->city)
        {
            $geo = [
                'lat' => $response->city->lat,
                'lng' => $response->city->lon,
            ];
        }
        else
        {
            $geo = [
                'lat' => 50.4547,
                'lng' => 30.5238,
            ];
        }

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($geo, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function get_last_product_in_category
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_last_product_in_category($id)
    {
        $faker = Faker::create('uk_UA');

        \Carbon::setLocale('uk');
        $tpls = [
            'model',
            'image',
            'audio',
            'video'
        ];
        foreach(range(1, 4) as $i)
        {
            $tpl[] = $this->site->fakeTpl($faker->randomElement($tpls));
            $faker->unique(true);
        }
        return response()->json($tpl, 200);
    }

    /* public function get_po
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_po(Request $request)
    {
        $items =  \TaxonomyTerm::getOptGroup(9, null, ['status' => 1]);
        return response()->json($items, 200);
    }

}
