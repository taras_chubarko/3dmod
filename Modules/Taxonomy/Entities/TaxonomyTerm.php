<?php namespace Modules\Taxonomy\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Baum;
//use Watson\Rememberable\Rememberable;
use Jenssegers\Mongodb\Eloquent\HybridRelations;

class TaxonomyTerm extends Baum\Node implements Transformable {
    
    use Sluggable;
    use TransformableTrait;
    //use Rememberable;
    use HybridRelations;


    protected $connection = 'mysql';

    protected $fillable = [];
    
    protected $table = 'taxonomy_term';
    
    protected $scoped = array('taxonomy_id');
    
    protected $orderColumn = 'sort';
    
    protected $guarded = ['_token'];
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source'    => 'name',
                'onUpdate'  => true,
            ]
        ];
    }
    /* public function image
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function image()
    {
        return $this->belongsToMany('Modules\Filem\Entities\Filem', 'taxonomy_term_image', 'taxonomy_term_id', 'fid')//->remember(60)
        ->withPivot('fid');
    }
    /* public function description
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function description()
    {
        return $this->belongsToMany('Modules\Taxonomy\Entities\TaxonomyTerm', 'taxonomy_term_description')
        ->withPivot('description');
    }
    /* public function description
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function nameLocale()
    {
        return $this->belongsToMany('Modules\Taxonomy\Entities\TaxonomyTerm', 'taxonomy_term_name')
        ->withPivot('name', 'locale');
    }
    /* public function getFieldNameAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldNameAttribute()
    {
        $field = [];
        
        if($this->nameLocale->count())
        {
            foreach($this->nameLocale as $name)
            {
                $field[$name->pivot->locale] = $name->pivot->name;
            }
        }
        return $field;
    }

    /* public function getNameEnAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getNameEnAttribute()
    {
        $field = '';

        if($this->nameLocale->count())
        {
            foreach($this->nameLocale as $name)
            {
                if($name->pivot->locale == 'en')
                {
                    $field = $name->pivot->name;
                }

            }
        }
        return $field;
    }

    /* public function getNameUkAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getNameUkAttribute()
    {
        $field = '';

        if($this->nameLocale->count())
        {
            foreach($this->nameLocale as $name)
            {
                if($name->pivot->locale == 'uk')
                {
                    $field = $name->pivot->name;
                }

            }
        }
        return $field;
    }

    /* public function getNameRuAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getNameRuAttribute()
    {
        $field = '';

        if($this->nameLocale->count())
        {
            foreach($this->nameLocale as $name)
            {
                if($name->pivot->locale == 'ru')
                {
                    $field = $name->pivot->name;
                }

            }
        }
        return $field;
    }
    
    

}