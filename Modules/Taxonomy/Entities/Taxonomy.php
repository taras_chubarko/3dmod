<?php namespace Modules\Taxonomy\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Taxonomy extends Model implements Transformable
{
    use TransformableTrait;
     
    protected $fillable = [];

    protected $connection = 'mysql';
    
    protected $table = 'taxonomy';
    
    protected $guarded = ['_token', 'page'];
    
    public function terms()
    {
        return $this->hasMany('Modules\Taxonomy\Entities\TaxonomyTerm')->orderBy('sort', 'ASC');
    }

}