<?php

namespace Modules\Taxonomy\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Taxonomy\Repositories\TaxonomyTermRepository;
use Modules\Taxonomy\Entities\TaxonomyTerm;

/**
 * Class TaxonomyTermRepositoryEloquent
 * @package namespace App\Repositories;
 */
class TaxonomyTermRepositoryEloquent extends BaseRepository implements TaxonomyTermRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TaxonomyTerm::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /*
    /* public function itemsArray($taxonomy_id);
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function itemsArray($taxonomy_id)
    {
        $items = $this->model->where('taxonomy_id', $taxonomy_id)->get()->toHierarchy()->toArray();

        $menus = $this->parseArray($items);

        $arr = [];
        $arr[0] = '- Корень -';

        foreach($menus as $item)
        {
            $depth = str_repeat('-', $item['depth']);

            $arr[$item['id']] = $depth . $item['name'];
        }

        return $arr;
    }
    /*
     *
     */
    public static function parseArray($parseArray, $parentID = 0)
    {
        $return = array();
        foreach ($parseArray as $subArray) {
            $returnSubSubArray = array();
            if (isset($subArray['children'])) {
                $returnSubSubArray = self::parseArray($subArray['children'], $subArray['id']);
            }
            $return[] = array(
                'id'        => $subArray['id'],
                'parent_id' => $parentID,
                'name'      => $subArray['name'],
                'depth'     => $subArray['depth'],
            );
            $return = array_merge($return, $returnSubSubArray);
        }
        return $return;
    }
    /* public function get($taxonomy_id, $parent_id, $params = array())
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAll($taxonomy_id, $parent_id = null, $params = array())
    {
        $items = $this->model->where('taxonomy_id', $taxonomy_id)->where('parent_id', $parent_id)->orderBy('sort', 'ASC')->get();

        $terms = array();

        if(!empty($params['title']))
        {
            $terms[''] = $params['title'];
        }

        if(!empty($params['group']) && $params['group'] == true)
        {
            foreach($items as $item)
            {
                $groups = $this->getAll($taxonomy_id, $item->id);

                $terms[$item->name] = $groups;
            }
        }
        else
        {
            foreach($items as $item)
            {
                $terms[$item->id] = $item->name;
            }
        }
        return $terms;
    }
    /* public function tree($taxonomy_id);
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function tree($taxonomy_id)
    {
        return $this->model->where('taxonomy_id', $taxonomy_id)->get()->toHierarchy();
    }
    /* public function treeAll($taxonomy_id);
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function treeAll($taxonomy_id)
    {
        return $this->model->where('taxonomy_id', $taxonomy_id)->get()->toHierarchy();
    }
    /* public function make
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function make($request)
    {
        if(is_null($request->parent_id))
        {
            $term = $this->model->create([
                'taxonomy_id' 	=> $request->taxonomy_id,
                'name' 		    => $request->name['en'],
                'sort' 		    => $request->sort,
                'ico'           => $request->ico,
                'status'        => $request->status,
            ]);
        }
        else
        {
            $term = $this->model->create([
                'taxonomy_id' 	=> $request->taxonomy_id,
                'name' 		    => $request->name['en'],
                'sort' 		    => $request->sort,
                'ico'           => $request->ico,
                'status'        => $request->status,
            ]);
            $term->makeChildOf($request->parent_id);
        }
        //
        if(!empty($request->image))
        {
            $term->image()->attach($term->id, [
                'fid' => $request->image['fid'],
            ]);
            \Filem::setStatus($request->image['fid'], $status = 1);
        }
        //
        if(!empty($request->name))
        {
            foreach($request->name as $k => $v)
            {
                $term->nameLocale()->attach($term->id, [
                    'name' => ($request->name[$k]) ? $request->name[$k] : $request->name['en'],
                    'locale' => $k,
                ]);
            }
        }
        //
        return $term;
    }
    /* public function change
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function change($request, $id)
    {
        $term = $this->model->find($id);
        $term->name 		= $request->name['en'];
        $term->parent_id 	= $request->parent_id;
        $term->sort 		= ($request->has('sort')) ? $request->sort : 0;
        $term->ico          = $request->ico;
        $term->status 		= ($request->has('status')) ? $request->status : 0;
        $term->save();
        //
        if($request->image)
        {
            $term->image()->detach();
            $term->image()->attach($term->id, [
                'fid' => $request->image['fid'],
            ]);
            \Filem::setStatus($request->image['fid'], $status = 1);
        }
        //
        if($request->name)
        {
            $term->nameLocale()->detach();
            foreach($request->name as $k => $v)
            {
                $term->nameLocale()->attach($term->id, [
                    'name' => ($request->name[$k]) ? $request->name[$k] : $request->name['en'],
                    'locale' => $k,
                ]);
            }
        }
        //
        return $term;
    }
    /* public function destroy
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function destroy($id)
    {
        $term = $this->model->find($id);
        $term->load('image');

        $im = data_get($term, 'image.0.id');
        if($im)
        {
            \Filem::destroy($im);
        }

        $term->image()->detach();
        $term->nameLocale()->detach();

        $term->delete();
        return $term;
    }
    /* public function findBySlug($slug)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }
    /* public function get($taxonomy_id, $parent_id, $params = array())
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get2($taxonomy_id, $parent_id = null)
    {
        $items = $this->model->where('taxonomy_id', $taxonomy_id)->where('parent_id', $parent_id)->orderBy('sort', 'ASC')->get();

        $terms = array();

        foreach($items as $item)
        {
            $terms[] = $item;
        }

        return collect($terms);
    }

    /* public function getTreeLocale($taxonomy_id, $locale)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getTreeLocale($taxonomy_id, $parent_id = null, $options = [])
    {
        $status = !empty($options['status']) ? $options['status'] : null;

        if(is_null($parent_id))
        {
            $q = $this->model->where('taxonomy_id', $taxonomy_id)->whereNull('parent_id')->orderBy('sort', 'ASC');
            if($status)
            {
                $q->where('status', $status);
            }
            $data = $q->get();
        }
        else
        {
            $q = $this->model->where('taxonomy_id', $taxonomy_id)->where('parent_id', $parent_id)->orderBy('sort', 'ASC');
            if($status)
            {
                $q->where('status', $status);
            }
            $data = $q->get();
        }
        //
        $items = [];
        //
        foreach ($data as $item)
        {
            $items[] = [
                'id'        => $item->id,
                'taxonomy_id' => $item->taxonomy_id,
                'parent_id' => $item->parent_id,
                'name'  => [
                    'en' => $item->nameEn,
                    'ru' => $item->nameRu,
                    'uk' => $item->nameUk,
                ],
                'depth'     => $item->depth,
                'sort'      => $item->sort,
                'ico'       => $item->ico,
                'slug'      => $item->slug,
                'status'    => $item->status,
                'children'  => $this->getTreeLocale($taxonomy_id, $item->id, $options),
            ];
        }
        //
        return $items;
    }

    /* public function getOptGroup($taxonomy_id, $parent_id, $options)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getOptGroup($taxonomy_id, $parent_id, $options)
    {
        $status = !empty($options['status']) ? $options['status'] : null;

        if(is_null($parent_id))
        {
            $q = $this->model->where('taxonomy_id', $taxonomy_id)->whereNull('parent_id')->orderBy('sort', 'ASC');
            if($status)
            {
                $q->where('status', $status);
            }
            $data = $q->get();
        }
        else
        {
            $q = $this->model->where('taxonomy_id', $taxonomy_id)->where('parent_id', $parent_id)->orderBy('sort', 'ASC');
            if($status)
            {
                $q->where('status', $status);
            }
            $data = $q->get();
        }
        //
        $items = [];
        //
        foreach ($data as $item)
        {
            $items[] = [
                'id'        => $item->id,
                'name'  => [
                    'en' => $item->nameEn,
                    'ru' => $item->nameRu,
                    'uk' => $item->nameUk,
                ],
                'ico'       => $item->ico,
                'items'  => $this->getOptGroup($taxonomy_id, $item->id, $options),
            ];
        }
        //
        return $items;
    }
    /* public function getOptGroup($taxonomy_id, $parent_id, $options)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getOpt($taxonomy_id, $parent_id, $options)
    {
        $status = !empty($options['status']) ? $options['status'] : null;

        if(is_null($parent_id))
        {
            $q = $this->model->where('taxonomy_id', $taxonomy_id)->whereNull('parent_id')->orderBy('sort', 'ASC');
            if($status)
            {
                $q->where('status', $status);
            }
            $data = $q->get();
        }
        else
        {
            $q = $this->model->where('taxonomy_id', $taxonomy_id)->where('parent_id', $parent_id)->orderBy('sort', 'ASC');
            if($status)
            {
                $q->where('status', $status);
            }
            $data = $q->get();
        }
        //
        $items = [];
        //
        foreach ($data as $item)
        {
            $items[] = [
                'id'        => $item->id,
                'name'  => [
                    'en' => $item->nameEn,
                    'ru' => $item->nameRu,
                    'uk' => $item->nameUk,
                ],
                'ico'       => $item->ico,
                'items'  => [],
            ];
        }
        //
        return $items;
    }
}
