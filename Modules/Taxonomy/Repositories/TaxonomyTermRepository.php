<?php

namespace Modules\Taxonomy\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TaxonomyTermRepository
 * @package namespace App\Repositories;
 */
interface TaxonomyTermRepository extends RepositoryInterface
{
    /*
     *
     */
    public function itemsArray($taxonomy_id);
    /*
     *
     */
    public function getAll($taxonomy_id, $parent_id, $params);
    /*
     *
     */
    public function tree($taxonomy_id);
    /*
     *
     */
    public function treeAll($taxonomy_id);
    /*
     *
     */
    public function make($request);
    /*
     *
     */
    public function change($request, $id);
    /*
     *
     */
    public function destroy($id);
    /*
     *
     */
    public function findBySlug($slug);
    /*
     *
     */
    public function get2($taxonomy_id, $parent_id);
    /*
     *
     */
    public function getTreeLocale($taxonomy_id, $parent_id, $options);
    //
    public function getOptGroup($taxonomy_id, $parent_id, $options);
    //
    public function getOpt($taxonomy_id, $parent_id, $options);
}
