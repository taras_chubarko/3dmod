<?php

namespace Modules\Taxonomy\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Taxonomy\Repositories\TaxonomyRepository;
use Modules\Taxonomy\Entities\TaxonomyTerm;

class TaxonomyController extends Controller
{
	/*
	 *
	 */
	protected $repository;
	protected $term;
	/* public function __construct
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function __construct(TaxonomyRepository $repository, TaxonomyTerm $term)
	{
		$this->repository = $repository;
		$this->term = $term;
	}
	/*
	 *
	 */
	public function index()
	{
		//$taxonomy = $this->taxonomy->paginate(20);
		//return view('admin::taxonomy.index', compact('taxonomy'));
        return view('admin::layouts.master');
	}

    /* public function items
    * @par $a
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function items()
    {
        $taxonomy = $this->repository->orderBy('name', 'ASC')->paginate(20);
        return response()->json($taxonomy, 200);
	}
	
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function create() {
		return view('admin::taxonomy.create');
	}
	
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function store(\Modules\Taxonomy\Http\Requests\CreateReq $request) {
		$this->repository->create($request->all());
        return response()->json('ok', 200);
	}
	
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function edit($id) {
		
		$taxonomy = $this->repository->find($id);
		
		return view('admin::taxonomy.edit', compact('taxonomy'));
	}
	
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function update(\Modules\Taxonomy\Http\Requests\CreateReq $request, $id) {
		$this->repository->update($request->all(), $id);
        return response()->json('ok', 200);
	}
	
	/* public function destroy
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function destroy($id) {
		
		$taxonomy = $this->repository->find($id);
		$items = \TaxonomyTerm::findWhere(['taxonomy_id' => $id]);
		foreach($items as $item)
		{
		    $item->delete();
		}
		$taxonomy->delete();
	}
	
	/* public function show
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function show($id) {
        return view('admin::layouts.master');
	}

    /* public function import
    * @par $a
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function import()
    {
//        $terms = [
//            "Audio (.AA)",
//            "Audio (.AAC)",
//            "Audio (.AC3(DolbyDigital))",
//            "Audio (.ADX)",
//            "Audio (.AHX)",
//            "Audio (.AIFF)",
//            "Audio (.APE)",
//            "Audio (.ASF)",
//            "Audio (.AUD)",
//            "Audio (.DMF)",
//            "Audio (.DTS)",
//            "Audio (.DXD)",
//            "Audio (.FLAC)",
//            "Audio (.MIDI)",
//            "Audio (.MMF(YamahaSMAF))",
//            "Audio (.MOD)",
//            "Audio (.MP1)",
//            "Audio (.MP2)",
//            "Audio (.MP3)",
//            "Audio (.MP4)",
//            "Audio (.MPC)",
//            "Audio (.OggVorbis)",
//            "Audio (.Opus)",
//            "Audio (.RA)",
//            "Audio (.TTA)",
//            "Audio (.VOC)",
//            "Audio (.VOX)",
//            "Audio (.VQF)",
//            "Audio (.WAV)",
//            "Audio (.WMA)",
//            "Audio (.XM)",
//            "Graphics (.ABR)",
//            "Adobeillustrator(.AI)",
//            "CorelDraw (.CDR)",
//            "Graphics (.EPS)",
//            "Immage (.GIFF)",
//            "Immage (.JPG)",
//            "Immage (.PNG)",
//            "Photoshop (.PSD)",
//            "Immage (.SVG)",
//            "Immage (.TIFF)",
//            "Video (.3G2)",
//            "Video (.3GP)",
//            "Video (.ASF)",
//            "Video (.AVI)",
//            "Video (.FLV)",
//            "Video (.M4V)",
//            "Video (.MOV)",
//            "Video (.MP4)",
//            "Video (.MPG)",
//            "Video (.RM)",
//            "Video (.SRT)",
//            "Video (.SWF)",
//            "Video (.VOB)",
//            "Video (.WMV)",
//            "3D Studio Max (.max)",
//            "3D Coat (.3b)",
//            "3D Coat Scene (.3b/.3b)",
//            "3D Points File (.pts)",
//            "3D Studio (.3ds)",
//            "AC3D  (.ac)",
//            "Additive Manufacturing (.amf)",
//            "Agisoft Photoscan (.ply)",
//            "Alembic (.abc)",
//            "Alias/WaveFront Material (.mtl)",
//            "Alibre/Geomagic Assm (.ad_asm)",
//            "Alibre/Geomagic Design Package (.ad_pkg)",
//            "Alibre/Geomagic Drawing (.ad_dwg)",
//            "Alibre/Geomagic Part (.ad_prt)",
//            "Alibre/Geomagic Sheetmetal (.ad_smp)",
//            "ArchiCAD (.gsm)",
//            "Artlantis (.atl/.atla/.atlo)",
//            "AutoCAD (.dwg)",
//            "Autodesk FBX (.fbx)",
//            "Autodesk Inventor (.ipt)",
//            "Beyond3D (.b3d)",
//            "Blender (.blend)",
//            "Bryce (.br5/.obp)",
//            "Catia V5 (.CATProduct/.CATMaterial/.CATAnalysis)",
//            "Cheetah3D (.jas)",
//            "Cinema 4D (.c4d)",
//            "Collada (.dae)",
//            "Crytek Character (.chr)",
//            "Crytek Geometry Animation (.cga)",
//            "Crytek Geometry Format (.cgf)",
//            "Crytek Skinned Render mesh (.skin)",
//            "DarkBASIC Object (.dbo)",
//            "DAZ Studio (.duf)",
//            "DirectX (.X)",
//            "DXF (.dxf)",
//            "Fusion 360 (.f3d)",
//            "Houdini (.hda/.hip/.bgeo/.geo/.bclip/.clip/.hipnc)",
//            "IGES (.ige/.igs/.iges)",
//            "Inventor Assembly (.iam)",
//            "Jewelry Cad (.jcad/.jcd)",
//            "Leaderwerks (.gmf)",
//            "Lightwave (.lwo/.lw/.lws)",
//            "Luxology Modo (.lxo/.lxl)",
//            "macroScript 3dsmax (.ms)",
//            "Marmoset Toolbag material (.tbmat)",
//            "Marmoset Toolbag Scene (.tbscene)",
//            "Marvelous Designer Avatar (.avt)",
//            "Marvelous Designer Garment  (.zpac)",
//            "Marvelous Designer Pose (.pos)",
//            "Marvelous Designer Project (*.ZPrj/.ZPrj)",
//            "Material Libraries (.mat)",
//            "Maxwell Scene (.mxs)",
//            "Maya (.ma/.mb)",
//            "MDL Material (.mdl)",
//            "MilkShape 3D  (.ms3d)",
//            "OBJ (.obj)",
//            "Octane Standalone Scene (.orbx)",
//            "OpenFlight (.flt)",
//            "PDF (.pdf)",
//            "Poser (.pz3/.pp2)",
//            "Reallusion iClone (.iprop,/.iAcc,/iCloth,/iAvatar,/iEffect)",
//            "Renderman (.rib/.slc/.sl/.slo)",
//            "Revit Family (.rfa)",
//            "Revit Models (.rvt)",
//            "Rhino (.3dm)",
//            "Shockwave 3D (.w3d)",
//            "Silo (.sia)",
//            "Sketchup (.skp)",
//            "Softimage (.hrc/.xsi)",
//            "Solid Edge (.asm)",
//            "SolidWorks (.sldprt/.sldasm/.slddrw)",
//            "Step (.stp)",
//            "Stereolithography (.stl)",
//            "Substance Painter project (.spp)",
//            "Terragen (.tgo)",
//            "Torque (.dts)",
//            "Truevision (.tga)",
//            "Unity 3D (.unitypackage)",
//            "Unity Prefab Format (.prefab)",
//            "Universal 3D (.u3d)",
//            "UnrealEngine (.uasset)",
//            "VRML (.wrl/.wrz)",
//            "Vue (.vue)",
//            "X file (.x)",
//            "X3D (.x3d)",
//        ];
//
//        foreach ($terms as $k => $term)
//        {
//            $item = $this->term->create([
//                'taxonomy_id' 	=> 8,
//                'name' 		    => $term,
//                'sort' 		    => $k,
//                'ico'           => null,
//                'status'        => 1,
//            ]);
//            //$item->makeChildOf(166);
//
//
//            $item->nameLocale()->attach($item->id, [
//                'name' => $term,
//                'locale' => 'en',
//            ]);
//
//            $item->nameLocale()->attach($item->id, [
//                'name' => $term,
//                'locale' => 'ru',
//            ]);
//
//            $item->nameLocale()->attach($item->id, [
//                'name' => $term,
//                'locale' => 'uk',
//            ]);
//        }
//
//        dd($terms);
	}
}
