<?php

namespace Modules\Taxonomy\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Taxonomy\Repositories\TaxonomyTermRepository;
use Modules\Taxonomy\Entities\TaxonomyTerm;
use Modules\Taxonomy\Entities\Taxonomy;


class TaxonomyTermController extends Controller
{
    /*
     *
     */
    protected $term;
    protected $model;
    protected $taxonomy;
    /* public function __construct
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct(TaxonomyTermRepository $term, TaxonomyTerm $model, Taxonomy $taxonomy)
    {
        $this->term = $term;
        $this->model = $model;
        $this->taxonomy = $taxonomy;
    }
    /* public function create
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */

    public function create() {
        return view('admin::taxonomy.terms.create');
    }

    /* public function store
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */

    public function store(Request $request) {
        $this->term->make($request);
        return response()->json('ok', 200);
    }

    /* public function edit
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */

    public function edit($id) {
        $term = $this->term->find($id);
        return view('admin::taxonomy.terms.edit', compact('term'));
    }

    /* public function update
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */

    public function update(Request $request, $id) {
        $this->term->change($request, $id);
        return response()->json('ok', 200);
    }

    /* public function destroy
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */

    public function destroy($id) {
        $this->term->destroy($id);
        return response()->json('ok', 200);
    }


    /* public function sort
     * @param 
     *-----------------------------------
     *|
     *-----------------------------------
     */

    public function sort(Request $request) {

        $items = $this->parseJsonArray($request->sort);

        foreach($items as $key => $item)
        {
            $menu = $this->term->find($item['id']);

            if($item['parent_id'] == 0)
            {
                $menu->makeRoot();
                $menu->sort = $key;
                $menu->save();
            }
            else
            {
                $menu->sort = $key;
                $menu->parent_id = $item['parent_id'];
                $menu->save();
            }

        }

        return 'OK'; // All the Input fields

    }


    public function parseJsonArray($jsonArray, $parentID = 0)
    {
        $return = array();
        foreach ($jsonArray as $subArray) {
            $returnSubSubArray = array();
            if (isset($subArray['children'])) {
                $returnSubSubArray = $this->parseJsonArray($subArray['children'], $subArray['id']);
            }
            $return[] = array('id' => $subArray['id'], 'parent_id' => $parentID);
            $return = array_merge($return, $returnSubSubArray);
        }
        return $return;
    }

    /* public function items
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function items($taxonomy_id)
    {
        $taxonomy = $this->taxonomy->find($taxonomy_id);
        $data = $this->term->getTreeLocale($taxonomy_id);

        $response = [
            'category' =>$taxonomy,
            'items' => $data,
        ];

        return response()->json($response, 200);
    }

}
