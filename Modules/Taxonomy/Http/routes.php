<?php

Route::group(['middleware' => 'web', 'prefix' => 'taxonomy', 'namespace' => 'Modules\Taxonomy\Http\Controllers'], function()
{
	Route::get('/', 'TaxonomyController@index');
    Route::get('/import', 'TaxonomyController@import');
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'Modules\Admin\Http\Middleware\AdminMDW'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Taxonomy\Http\Controllers'], function()
{
	Route::resource('taxonomy', 'TaxonomyController');
    Route::get('taxonomy/items/all', 'TaxonomyController@items');

	Route::resource('taxonomy/term', 'TaxonomyTermController');
	Route::post('taxonomy/term/sort', 'TaxonomyTermController@sort');
    Route::get('taxonomy/term/items/{taxonomy_id}', 'TaxonomyTermController@items');
});
