<?php

Route::group(['middleware' => 'web', 'prefix' => 'data', 'namespace' => 'Modules\Data\Http\Controllers'], function()
{
    Route::get('/', 'DataController@index');
    
    Route::get('fail', [
        'as' => 'fail',
        'uses' => 'DataController@fail'
    ]);
    
    Route::get('success', [
        'as' => 'success',
        'uses' => 'DataController@success'
    ]);
    
    Route::post('check', [
        'as' => 'check',
        'uses' => 'DataController@check'
    ]);
    
    Route::post('aviso', [
        'as' => 'aviso',
        'uses' => 'DataController@aviso'
    ]);

    Route::get('trans/{txt}', [
        'uses' => 'DataController@trans'
    ]);

    Route::get('slug/{txt}', [
        'uses' => 'DataController@slug'
    ]);

    
    
    
});
