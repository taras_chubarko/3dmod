<?php

namespace Modules\AllCgStorage\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AllCgStorage\Entities\AllCgStorage;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class AllCgStorageController extends Controller
{
    protected $model;
    //
    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(AllCgStorage $model)
    {
        $this->model = $model;
    }

    /* public function upload
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function upload(Request $request)
    {
        if($request->has('folder')){

            $user = $request->has('user_id') ? \User::find($request->user_id) : auth()->user();

            $file = Storage::disk('allcgstorage')->putFile($request->folder, new File($request->file));
            $meta = Storage::disk('allcgstorage')->getMetaData($file);
            $mime = Storage::disk('allcgstorage')->mimeType($file);
            $meta['mime'] = $mime;
            $meta['ext'] = $request->file->getClientOriginalExtension();
            $meta['name'] = $request->file->getClientOriginalName();
            $meta['folder'] = $request->folder;
            $meta['user_id'] = ($user) ? $user->id : null;
            $data = $this->model->create($meta);
            $data->id = $data->_id;
            unset($data->_id);

            $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
            return response()->json($data, 200, $headers, JSON_UNESCAPED_UNICODE);
        }else{
            $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
            return response()->json('error', 404, $headers, JSON_UNESCAPED_UNICODE);
        }
    }

    /* public function destroy
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function destroy($id)
    {
        $file = $this->model->find($id);
        if($file){
            Storage::disk('allcgstorage')->delete($file->path);
            $file->delete();
        }
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json('ok', 200, $headers, JSON_UNESCAPED_UNICODE);
    }
}
