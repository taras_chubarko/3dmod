<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 19.06.2018
 * Time: 12:50
 */

namespace Modules\AllCgStorage\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AllCgStorage\Entities\AllCgStorage;
use Illuminate\Support\Facades\Storage;

class AllCgStorageImagePreviewController extends Controller
{
    protected $model;

    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(AllCgStorage $model)
    {
        $this->model = $model;
    }

    /* public function preview
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function preview($id, $size)
    {
        $file = $this->model->find($id);
        //
        if($file){
            $exists = Storage::disk('allcgstorage')->exists($file->path);
            if($exists){
                if($size == 'original'){
                    $cache_image = \Image::cache(function($image) use($file){
                        $contents = Storage::disk('allcgstorage')->get($file->path);
                        return $image->make($contents);
                    }, 1440); // cache for 10 minutes
                    return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
                }else{
                    $size = explode('x', $size);
                    $cache_image = \Image::cache(function($image) use($file, $size){
                        $contents = Storage::disk('allcgstorage')->get($file->path);
                        return $image->make($contents)->fit($size[0], $size[1]);
                    }, 1440); // cache for 10 minutes
                    return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
                }
            }else{
                $size = explode('x', $size);
                $cache_image = \Image::cache(function($image) use($size){
                    return $image->make('http://via.placeholder.com/'.$size[0].'x'.$size[1])->fit($size[0], $size[1]);
                }, 1440); // cache for 10 minutes
                return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
            }
        }else{
            $size = explode('x', $size);
            $cache_image = \Image::cache(function($image) use($size){
                return $image->make('http://via.placeholder.com/'.$size[0].'x'.$size[1])->fit($size[0], $size[1]);
            }, 1440); // cache for 10 minutes
            return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
        }
    }
}