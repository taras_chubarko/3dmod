<?php

Route::group(['middleware' => 'web', 'prefix' => 'allcgstorage', 'namespace' => 'Modules\AllCgStorage\Http\Controllers'], function()
{
    Route::get('/', 'AllCgStorageController@index');
});
//
Route::group(['middleware' => 'web', 'prefix' => 'image', 'namespace' => 'Modules\AllCgStorage\Http\Controllers'], function()
{
    Route::get('/preview/{id}/{size}', [
            'as' 	=> 'image.preview',
            'uses' 	=> 'AllCgStorageImagePreviewController@preview'
    ]);
});
//
Route::group(['middleware' => 'auth.apikey', 'prefix' => 'api/v1', 'namespace' => 'Modules\AllCgStorage\Http\Controllers'], function()
{
    Route::post('/allcgstorage/upload', 'AllCgStorageController@upload');
    Route::delete('/allcgstorage/delete/{id}', 'AllCgStorageController@destroy');
});
