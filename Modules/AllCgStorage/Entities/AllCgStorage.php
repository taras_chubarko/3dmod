<?php

namespace Modules\AllCgStorage\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

/**
 * Class AllCgStorage.
 *
 * @package namespace App\Entities;
 */
class AllCgStorage extends Eloquent implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $connection = 'mongodb';
    protected $collection = 'all_cg_storage';
    //
    protected $guarded = ['_id', '_token'];

}
