<?php

namespace Modules\AllCgStorage\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\AllCgStorage\Repositories\AllCgStorageRepository;
use Modules\AllCgStorage\Entities\AllCgStorage;

/**
 * Class AllCgStorageRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class AllCgStorageRepositoryEloquent extends BaseRepository implements AllCgStorageRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return AllCgStorage::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
