<?php

namespace Modules\AllCgStorage\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AllCgStorageRepository.
 *
 * @package namespace App\Repositories;
 */
interface AllCgStorageRepository extends RepositoryInterface
{
    //
}
