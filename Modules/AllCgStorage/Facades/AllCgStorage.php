<?php
/**
 * Created by PhpStorm.
 * User: wvl-ab019u
 * Date: 04.06.2018
 * Time: 19:26
 */

namespace Modules\AllCgStorage\Facades;

use Illuminate\Support\Facades\Facade;

class AllCgStorage extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Modules\AllCgStorage\Repositories\AllCgStorageRepository';
    }
}