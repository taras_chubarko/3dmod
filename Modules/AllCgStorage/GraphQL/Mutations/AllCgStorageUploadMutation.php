<?php
/**
 * Created by PhpStorm.
 * User: wvl-ab019u
 * Date: 05.06.2018
 * Time: 8:44
 */

namespace Modules\AllCgStorage\GraphQL\Mutations;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Upload\UploadType;
use M1naret\GraphQL\Support\Facades\GraphQL;
use M1naret\GraphQL\Support\Mutation;
use M1naret\GraphQL\Support\SelectFields;
use Modules\AllCgStorage\GraphQL\Types\AllCgUploadType;

class AllCgStorageUploadMutation extends Mutation
{
    //
    const NAME = 'AllCgStorageUpload';
    //
    protected $attributes = [
        'name' => self::NAME,
    ];
    /* public function args
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function args() : array
    {
        return [
            [
                'name' => 'file',
                'type' =>  new UploadType(['name' => 'file_upload']),//Type::string(),
            ],
        ];
    }

    /* public function authorize
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function authorize(array $args) : bool
    {
        return !empty(request()->user());
    }

    /* public function rules
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function rules(array $args = []) : array
    {
        return [
//            'name'        => 'required|string|min:5',
//            'description' => 'sometimes|required|string|min:20',
//            'type'        => 'sometimes|required|integer',
//            'invite_type' => 'sometimes|required|integer',
//            'is_forum'    => 'sometimes|required|boolean',
        ];
    }

    /* public function type
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function type()
    {
        //return GraphQL::type(AllCgUploadType::NAME);
        return Type::string();
    }

//    /* public function prepareRequest
//    * @param
//    *-----------------------------------
//    *|
//    *-----------------------------------
//    */
//    public function prepareRequest(array $args) : array
//    {
//
//    }

    /* public function resolve
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function resolve($root, $args, SelectFields $fields)
    {
        //$file = $args['file'];
        //\Storage::putFile('photos', new File($file));
        //dd(\Request::all());
        dd($args);
    }

}