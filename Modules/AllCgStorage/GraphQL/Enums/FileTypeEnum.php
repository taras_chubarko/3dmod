<?php
/**
 * Created by PhpStorm.
 * User: wvl-ab019u
 * Date: 05.06.2018
 * Time: 9:48
 */

namespace Modules\AllCgStorage\GraphQL\Enums;

use M1naret\GraphQL\Support\Type as GraphQLType;

class FileTypeEnum extends GraphQLType
{
    //
    const NAME = 'file_types_enum';

    protected $enumObject = true;

    protected $attributes = [
        'name'        => self::NAME,
        'description' => 'Типи файлів',
        'values'      => [
            'images' => [
                'value'       => 'images',
                'description' => 'Картинки',
            ],
            'video' => [
                'value'       => 'video',
                'description' => 'Відео',
            ],
            'audio' => [
                'value'       => 'audio',
                'description' => 'Аудіо',
            ],
            'archives' => [
                'value'       => 'archives',
                'description' => 'Архіви',
            ],
        ],
    ];
}