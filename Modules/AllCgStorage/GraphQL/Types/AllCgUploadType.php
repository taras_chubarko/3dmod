<?php
/**
 * Created by PhpStorm.
 * User: wvl-ab019u
 * Date: 05.06.2018
 * Time: 9:07
 */

namespace Modules\AllCgStorage\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use M1naret\GraphQL\Support\Facades\GraphQL;
use M1naret\GraphQL\Support\Type as GraphQLType;
use Modules\AllCgStorage\Entities\AllCgStorage;
use Modules\AllCgStorage\GraphQL\Enums\FileTypeEnum;

class AllCgUploadType  extends GraphQLType
{
    //
    const NAME = 'AllCgUploadType';
    //
    protected $attributes = [
        'name'        => self::NAME,
        'description' => 'Тип данных файл',
        'model'       => AllCgStorage::class,
    ];

    /* public function fields
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function fields() : array
    {
        return[
            'id' => [
                'type'          => Type::nonNull(Type::string()),
                'description'   => 'ID файлу',
            ],
            'filename' => [
                'type'        => Type::string(),
                'description' => 'Назва файлу',
            ],
            'filename_original' => [
                'type'        => Type::string(),
                'description' => 'Оригінальна назва файлу',
            ],
            'type' => [
                'type'        => Type::string(),//GraphQL::type(FileTypeEnum::NAME),
                'description' => 'Тип файлу',
            ],
            'uri' => [
                'type'        => Type::string(),
                'description' => 'Короткий шлях файлу',
            ],
            'ext' => [
                'type'        => Type::string(),
                'description' => 'Розширення файлу',
            ],
            'mime' => [
                'type'        => Type::string(),
                'description' => 'MIME - тип файлу',
            ],
            'size' => [
                'type'        => Type::string(),
                'description' => 'Розмір файлу',
            ],
            'user_id' => [
                'type'        => Type::string(),
                'description' => 'Автор файлу',
            ],
        ];
    }
}