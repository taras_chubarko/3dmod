<?php

namespace Modules\Catalog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Catalog\Entities\CatalogItems;
use Modules\Catalog\Entities\Catalog;

class CatalogItemsController extends Controller
{
    protected $model;
    protected $catalog;

    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(CatalogItems $model, Catalog $catalog)
    {
        $this->model = $model;
        $this->catalog = $catalog;
    }

    /* public function index
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function index()
    {

    }

    /* public function get_child_items
    * @param
    *-----------------------------------
    *| $slug - каталог
    *| $slug2 - елемент каталогу
    *-----------------------------------
    */
    public function get_child_items($slug, $slug2)
    {
        $catalog = $this->catalog->where('slug', $slug)->first();
        $item = $this->model->where('catalog_id', $catalog->_id)->where('slug', $slug2)->with('children.children')->first();
        $item->catalog = $catalog;

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($item, 200, $headers, JSON_UNESCAPED_UNICODE);
    }
    /* public function store
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function store(Request $request)
    {
        $request->offsetUnset('_id');
        $item = $this->model->create($request->all());
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($item, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function update
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function update(Request $request, $id)
    {
        $item = $this->model->find($id);
        $request->offsetUnset('created_at');
        $request->offsetUnset('updated_at');
        $request->offsetUnset('_id');
        $request->offsetUnset('children');

        foreach ($request->all() as $k=>$val)
        {
            $item->{$k} = $val;
        }
        $item->save();

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($item, 200, $headers, JSON_UNESCAPED_UNICODE);
    }
    /* public function destroy
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function destroy($id)
    {
        $items = $this->model->with('children')->where('_id', $id)->get();
        $this->removeAllChildren($items);
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json('ok', 200, $headers, JSON_UNESCAPED_UNICODE);
    }
    /* public function removeAllChildren
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function removeAllChildren($items)
    {
        foreach ($items as $item) {
            if($item->children->count())
            {
                $this->removeAllChildren($item->children);
            }
            $this->model->destroy($item->_id);
        }
    }

    /* public function tree
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function tree(Request $request)
    {
        $slug = $this->catalog->where('slug', $request->slug)->first();

        $items = $this->model->where('catalog_id', $slug->id)
            ->where('parent_id', $request->has('parent_id') ? $request->parent_id : null)
            ->get();

        $items->transform(function ($value) {
            $data = [
                'value' => $value->_id,
                'text'  => $value->name['uk'],
            ];
            return $data;
        });
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($items, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function upload_xlsx
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function upload_xlsx(Request $request)
    {
        $path = $request->file('xls')->store('public/xls');
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($path, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function upload_add
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function upload_add(Request $request)
    {
        \Excel::load(storage_path('app').'/'.$request->path, function($reader) use ($request) {
            $results = $reader->all();
            $items = collect($results)->toArray();
            $this->import_add($this->array_tree($items), $request->catalog_id, $request->parent_id);
        });
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json('ok', 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function import_add
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function import_add($items, $catalog_id, $parent_id = null)
    {
        foreach ($items as $item)
        {
            $var = $this->model->create([
                'catalog_id' => $catalog_id,
                'name' => [
                    'en' => $item['name_en'],
                    'ru' => $item['name_ru'],
                    'uk' => $item['name_uk'],
                ],
                'parent_id' => $parent_id,
                'sort'  => !empty($item['sort']) ? $item['sort'] : 1,
                'ico'   => !empty($item['ico']) ? $item['ico'] : null,
                'status' => true,
            ]);
            if(isset($item['children'])){
                $this->import_add($item['children'], $catalog_id, $var->_id);
            }
        }
    }

    public function array_tree(&$array) {
        $tree = array();
        // Create an associative array with each key being the ID of the item
        foreach($array as $k => &$v) $tree[$v['id']] = &$v;
        // Loop over the array and add each child to their parent
        foreach($tree as $k => &$v) {
            if(!$v['parent_id']) continue;
            $tree[$v['parent_id']]['children'][] = &$v;
        }
        // Loop over the array again and remove any items that don't have a parent of 0;
        foreach($tree as $k => &$v) {
            if(!$v['parent_id']) continue;
            unset($tree[$k]);
        }
        return $tree;
    }

    /* public function filterdata
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function filterdata($id)
    {
        $items = $this->model->where('parent_id', $id)->get();
        $items->transform(function ($value) {
            return [
                'id'    => $value->_id,
                'name'  => $value->name,
            ];
        });

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($items, 200, $headers, JSON_UNESCAPED_UNICODE);
    }


}
