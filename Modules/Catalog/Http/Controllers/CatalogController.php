<?php

namespace Modules\Catalog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Catalog\Entities\Catalog;

class CatalogController extends Controller
{
    protected $model;

    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(Catalog $model)
    {
        $this->model = $model;
    }

    /* public function index
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function index()
    {
        $items = $this->model->orderBy('created_at', 'DESC')->paginate();
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($items, 200, $headers, JSON_UNESCAPED_UNICODE);
    }
    /* public function store
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function store(Request $request)
    {
        $request->offsetUnset('_id');
        $item = $this->model->create($request->all());
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($item, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function update
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function update(Request $request, $id)
    {
        $item = $this->model->find($id);
        $request->offsetUnset('created_at');
        $request->offsetUnset('updated_at');
        $request->offsetUnset('_id');

        foreach ($request->all() as $k=>$val)
        {
            $item->{$k} = $val;
        }
        $item->save();

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($item, 200, $headers, JSON_UNESCAPED_UNICODE); 
    }

    /* public function show
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function show($id)
    {
        $item = $this->model->where('slug', $id)->with('parentItems.children')->first();
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($item, 200, $headers, JSON_UNESCAPED_UNICODE);
    }
}
