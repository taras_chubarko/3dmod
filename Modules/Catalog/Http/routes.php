<?php

//Route::group(['middleware' => 'web', 'prefix' => 'catalog', 'namespace' => 'Modules\Catalog\Http\Controllers'], function()
//{
//    Route::get('/', 'CatalogController@index');
//});

Route::group(['middleware' => ['web', 'Modules\Admin\Http\Middleware\AdminMDW'], 'prefix' => 'api/admin', 'namespace' => 'Modules\Catalog\Http\Controllers'], function()
{
    Route::resource('catalog', 'CatalogController');
    Route::resource('catalog/data/items', 'CatalogItemsController');


    Route::group(['prefix' => 'catalog'], function()
    {
        Route::post('tree/items', 'CatalogItemsController@tree');
        Route::post('upload/xlsx', 'CatalogItemsController@upload_xlsx');
        Route::post('upload/add', 'CatalogItemsController@upload_add');
        Route::get('items/{slug}/{slug2}', 'CatalogItemsController@get_child_items');
    });
});

Route::group(['middleware' => 'auth.apikey', 'prefix' => 'api/v1', 'namespace' => 'Modules\Catalog\Http\Controllers'], function()
{
    Route::post('/catalog/filterdata/{id}', 'CatalogItemsController@filterdata');
});
