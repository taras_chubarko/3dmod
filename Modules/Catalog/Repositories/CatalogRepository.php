<?php

namespace Modules\Catalog\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CatalogRepository.
 *
 * @package namespace App\Repositories;
 */
interface CatalogRepository extends RepositoryInterface
{
    //
    public function treeMenu($catalog_id, $parent_id, $options);
}
