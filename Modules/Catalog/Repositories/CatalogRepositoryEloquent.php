<?php

namespace Modules\Catalog\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Catalog\Repositories\CatalogRepository;
use Modules\Catalog\Entities\Catalog;
use Modules\Catalog\Entities\CatalogItems;

/**
 * Class CatalogRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CatalogRepositoryEloquent extends BaseRepository implements CatalogRepository
{
    protected $catalogItems;

    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(CatalogItems $catalogItems)
    {
        $this->catalogItems = $catalogItems;
    }
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Catalog::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /* public function treeMenu($catalog_id, $parent_id, $options)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function treeMenu($catalog_id, $parent_id = null, $options = array())
    {
        $status = !empty($options['status']) ? $options['status'] : null;
        //
        $query = $this->catalogItems->where('catalog_id', $catalog_id)->where('parent_id', $parent_id)->orderBy('sort', 'ASC');
        if($status)
        {
            $query->where('status', $status);
        }
        $query->with(['children.children' => function($q){
            $q->select('_id', 'name', 'parent_id', 'sort', 'ico', 'status', 'slug');
        }]);
        //
        $query->select('_id', 'name', 'parent_id', 'sort', 'ico', 'status', 'slug');
        $data = $query->get();
        return $data;
    }
    
}
