<?php
/**
 * Created by PhpStorm.
 * User: wvl-ab019u
 * Date: 23.05.2018
 * Time: 10:56
 */

namespace Modules\Catalog\Facades;

use Illuminate\Support\Facades\Facade;


class Catalog extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Modules\Catalog\Repositories\CatalogRepository';
    }
}