<?php

namespace Modules\Catalog\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Cviebrock\EloquentSluggable\Sluggable;


/**
 * Class Catalog.
 *
 * @package namespace App\Entities;
 */
class Catalog extends Eloquent implements Transformable
{
    use TransformableTrait;
    use Sluggable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //
    protected $connection = 'mongodb';
    protected $collection = 'catalog';
    //
    protected $guarded = ['_token'];
    /* public function sluggable
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }

    /* public function parentItems
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function parentItems()
    {
        return $this->hasMany(\Modules\Catalog\Entities\CatalogItems::class, 'catalog_id', '_id')
            ->where('parent_id', null);
    }

}
