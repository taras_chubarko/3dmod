<?php

namespace Modules\Catalog\Entities;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Cviebrock\EloquentSluggable\Sluggable;

class CatalogItems extends Eloquent
{
    use Sluggable;
    //
    protected $connection = 'mongodb';
    protected $collection = 'catalog_items';
    //
    protected $guarded = ['_token'];
    /* public function sluggable
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name.en'
            ]
        ];
    }

    /* public function setSortAttribute
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setSortAttribute($value)
    {
        $this->attributes['sort'] = (int) $value;
    }
    /* public function setStatusAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = (boolean) $value;
    }

    /* public function countChildren
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function children()
    {
        return $this->hasMany(\Modules\Catalog\Entities\CatalogItems::class, 'parent_id', '_id');
    }

}
