<?php

namespace Modules\Message\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class MessageCategory extends Model
{
    use Sluggable;

    protected $connection = 'mysql';

    protected $fillable = [];

    protected $guarded = ['_token'];

    protected $table = 'message_category';

    //
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
                'onUpdate' => true,
            ]
        ];
    }
}
