<?php

namespace Modules\Message\Entities;

use Illuminate\Database\Eloquent\Model;

class MessageReply extends Model
{

    protected $connection = 'mysql';

    protected $fillable = [];

    protected $guarded = ['_token'];

    protected $table = 'message_reply';

    /* public function toUser
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function user()
    {
        return $this->hasOne(\Modules\User\Entities\User::class, 'id', 'user_id');
    }

    /* public function toUser
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function tema()
    {
        return $this->hasOne(\Modules\Message\Entities\Message::class, 'id', 'message_id');
    }
    /* public function file
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function file()
    {
        return $this->hasOne(\Modules\Filem\Entities\Filem::class, 'id', 'fid');
    }
}
