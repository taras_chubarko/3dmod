<?php

namespace Modules\Message\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Message extends Model implements Transformable
{
    use TransformableTrait;

    protected $connection = 'mysql';

    protected $fillable = [];

    protected $guarded = ['_token'];

    protected $table = 'message';

    /* public function toUser
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function addressee()
    {
        return $this->hasOne(\Modules\User\Entities\User::class, 'id', 'addressee_id');
    }

    /* public function fromUser
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function sender()
    {
        return $this->hasOne(\Modules\User\Entities\User::class, 'id', 'sender_id');
    }
    /* public function getAddresatAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getAddressatAttribute()
    {
        $user = auth()->user();
        if($user->id !== $this->sesender_id)
        {
            return $this->sender;
        }
        else if($user->id !== $this->addressee_id)
        {
            return $this->addressee;
        }
    }

}
