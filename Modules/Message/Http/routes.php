<?php

Route::group(['middleware' => 'web', 'prefix' => 'api/v1/message', 'namespace' => 'Modules\Message\Http\Controllers'], function()
{
    Route::post('/category/create', 'MessageCategoryController@create');
    Route::get('/category/my', 'MessageCategoryController@my_category');
    Route::put('/category/{id}', 'MessageCategoryController@category_update');
    Route::delete('/category/{id}', 'MessageCategoryController@category_delete');


    Route::post('/komu', 'MessageController@komu');
    Route::post('/create', 'MessageController@create');
    Route::post('/reply', 'MessageController@reply');
    Route::post('/send/file', 'MessageController@send_file');
    Route::post('/block', 'MessageController@block');

    Route::post('/read/{id}', 'MessageController@read');
    Route::delete('/delete/{id}/block', 'MessageController@delete_from_block');
    Route::post('/get/messages', 'MessageController@messages');

    Route::post('/get/reply/{message_id}', 'MessageController@get_reply');

});
