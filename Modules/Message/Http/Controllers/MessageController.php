<?php

namespace Modules\Message\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Message\Entities\Message;
use Modules\User\Entities\User;
use Modules\Message\Repositories\MessageRepository;

class MessageController extends Controller
{
    protected $model;
    protected $users;
    protected $repository;
    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(Message $model, User $users, MessageRepository $repository)
    {
        $this->model = $model;
        $this->users = $users;
        $this->repository = $repository;
    }
    /* public function komu
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function komu(Request $request)
    {
        $user = auth()->user();
        $data = [];

        if($request->has('q') && $request->q != '')
        {
            $result = $this->users->where('id', '<>', $user->id)
                ->where('name', 'like', '%'.$request->q.'%')
                ->orWhere('sname', 'like', '%'.$request->q.'%')
                ->take(5)->get();
            //
            if($result)
            {
                foreach ($result as $item) {
                    $data[] = [
                        'id' => $item->id,
                        'name' => $item->full_name,
                    ];
                }
            }
        }

        return response()->json($data, 200);
    }

    /* public function create
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function create(Request $request)
    {
        $this->repository->make($request);
        return response()->json('ok', 200);
    }

    /* public function listCollection
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function listCollection($messages = null)
    {
        if($messages)
        {
            $messages->getCollection()->transform(function ($value) {
                $user = auth()->user();

                if($user->id !== $value->sender->id)
                {
                    $user = $value->sender;
                }
                else
                {
                    $user = $value->addressee;
                }

                $data = [
                    'id' => $value->id,
                    'user' => [
                        'id' => $user->id,
                        'name' => $user->full_name,
                        'link' => '/user/'.$user->slug,
                    ],
                    'subject' => $value->subject,
                    'message' => $value->message,
                    'created_at' => $value->created_at->format('d.m.Y - H:i'),
                ];
                return $data;
            });
        }
        return $messages;
    }

    /* public function messages
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function messages()
    {
        $user = auth()->user();
        $messages = $this->repository->getMessages($user,20);
        //
        return response()->json($messages, 200);
    }

    /* public function block
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function block()
    {
        $user = auth()->user();
        $messages = $this->repository->getMessages($user,5);
        return response()->json($messages, 200);
    }

    /* public function read
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function read($id)
    {
        $this->repository->setRead($id);
        return response()->json('ok', 200);
    }

    /* public function delete_from_block
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function delete_from_block($id)
    {
        $this->model->where('id', $id)->update(['addressee_status' => 1, 'addressee_category' => 3]);
        $messages = $this->repository->block();
        return response()->json($messages, 200);
    }

    /* public function reply
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function reply(Request $request)
    {
        $user = auth()->user();

       $this->repository->reply([
            'user_id'       => $user->id,
            'message_id'    => $request->message_id,
            'message'       => $request->message,
            'status'        => $request->status,
        ]);
        //
        return response()->json('ok', 200);
    }

    /* public function get_reply
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_reply($message_id)
    {
        $user = auth()->user();

        $messages = $this->repository->getReply($message_id);

        event(new \Modules\Message\Events\UpdateMessageBlockEvent($this->repository->getMessages($user, 5), $user));
        event(new \Modules\Message\Events\UpdateMessageListEvent($this->repository->getMessages($user, 20), $user));

        return response()->json($messages, 200);
    }

    /* public function send_file
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function send_file(Request $request)
    {
        $user = auth()->user();

        $file = \Filem::make($request->image, $request->folder, $user->id, $status = 1);

        $this->repository->reply([
            'user_id'       => $user->id,
            'message_id'    => $request->message_id,
            'message'       => $file->original,
            'fid'           => $file->id,
            'status'        => 0,
        ]);
        //
        return response()->json('ok', 200);
    }
}
