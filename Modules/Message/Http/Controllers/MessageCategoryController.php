<?php

namespace Modules\Message\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Message\Entities\MessageCategory;
use Modules\Message\Entities\Message;

class MessageCategoryController extends Controller
{
    protected $model;
    protected $message;

    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(MessageCategory $model, Message $message)
    {
        $this->model = $model;
        $this->message = $message;
    }
    /* public function create
    * @parama
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function create(Request $request)
    {
        $user = auth()->user();
        $item = $this->model->create([
            'user_id' => $user->id,
            'name' => $request->name,
        ]);

        $data = [
            'id'    => $item->id,
            'name'  => str_limit($item->name, 12),
            'name_long'  => $item->name,
            'slug'  => $item->slug,
            'count' => 0,
            'rm' => true,
            'translate' => false,
        ];

        return response()->json($data, 200);
    }

    /* public function my_category
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function my_category()
    {
        $user = auth()->user();

        $items[0] = [
            'id'    => null,
            'name'  => 'inputs',//'Вхідні',
            'slug'  => null,
            'count' => \Message::countNew(),
            'translate' => true,
        ];

        $items[1] = [
            'id'    => null,
            'name'  => 'sent',//'Надіслані',
            'slug'  => 'out',
            'count' => null,
            'translate' => true,
        ];

        $items[2] = [
            'id'    => null,
            'name'  => 'basket',//'Кошик',
            'slug'  => 'cart',
            'count' => $this->message->where('addressee_id', $user->id)->where('addressee_category', 3)->count(),
            'translate' => true,
        ];

        $cats = $this->model->where('user_id', $user->id)->get();
        $i = 3;
        foreach ($cats as $cat) {
            $items[$i] = [
                'id'    => $cat->id,
                'name'  => str_limit($cat->name, 12),
                'name_long'  => $cat->name,
                'slug'  => $cat->slug,
                'count' => 0,
                'rm' => true,
                'translate' => false,
            ];
            $i++;
        }
        //
        return response()->json($items, 200);
    }

    /* public function category_update
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function category_update(Request $request, $id)
    {
        $cat = $this->model->find($id);
        $cat->name = $request->name;
        $cat->save();

        $item = [
            'id'    => $cat->id,
            'name'  => str_limit($cat->name, 12),
            'name_long'  => $cat->name,
            'slug'  => $cat->slug,
            'count' => 0,
            'rm' => true,
            'translate' => false,
        ];

        return response()->json($item, 200);
    }

    /* public function category_delete
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function category_delete($id)
    {
        $this->model->find($id)->delete();
        return response()->json('ok', 200);
    }
}
