<?php

namespace Modules\Message\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MessageRepository
 * @package namespace App\Repositories;
 */
interface MessageRepository extends RepositoryInterface
{
    //
    public function countNew($user);
    //
    public function make($request);
    //
    public function getReply($message_id);
    //
    public function reply($data);
    //
    public function setRead($id);
    //
    public function getMessages($user, $paginate);
    //
    public function getChat($addressee);
    //
    public function findOrNew($addressee);
    //
    public function remove($addressee);
    //
    public function countNewReply($message);
    //
    public function getLastReply($message);
    //
    public function formatedReply($reply);
}
