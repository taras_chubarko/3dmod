<?php

namespace Modules\Message\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Message\Repositories\MessageRepository;
use Modules\Message\Entities\Message;
use Modules\Message\Entities\MessageReply;

/**
 * Class MessageRepositoryEloquent
 * @package namespace App\Repositories;
 */
class MessageRepositoryEloquent extends BaseRepository implements MessageRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Message::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /* public function getChat($sender_id, $addressee_id)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getChat($addressee)
    {

        $sender = auth()->user();
        $chat = $this->model->where('sender_id', $sender->id)->where('addressee_id', $addressee->id)->orWhere(function ($query) use ($sender, $addressee){
            $query->where('sender_id', $addressee->id)->where('addressee_id', $sender->id);
        })->first();
        //
        if(!$chat)
        {
            $chat = $this->model->create([
                'sender_id'     => $sender->id,
                'addressee_id'  => $addressee->id,
            ]);
        }
        //
        return $chat;
    }
    /* public function make

    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function make($request)
    {
        $sender = auth()->user();
        $addressee = \User::findOrFail($request->addressee_id);
        $reply = null;
        //
        if($addressee)
        {
            $chat = $this->getChat($addressee);
            //
            $reply = $this->reply([
                'user_id'       => $sender->id,
                'message_id'    => $chat->id,
                'message'       => $request->has('message') ? $request->message : null,
            ]);
        }
        return $reply;
    }
    //
    public function countNew($user = null)
    {
        $user = ($user) ? $user: auth()->user();
        $messages = $this->model->where('sender_id', $user->id)->orWhere('addressee_id', $user->id)->select('id')->get()->pluck('id');
        $reply = MessageReply::whereIn('message_id', $messages)->where('user_id', '<>', $user->id)->where('status', 0)->count();
        return $reply;
    }
    /* public function getReply($message_id)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getReply($message_id)
    {
        $user = auth()->user();
        //
        $replys = MessageReply::where('message_id', $message_id)->where('message', '<>', 'start_msg')->orderBy('created_at', 'DESC')->paginate(10);

        $replys->getCollection()->transform(function ($value) use ($user) {
            //
            if($user->id !== $value->user->id)
            {
                $mss = MessageReply::find($value->id);
                $mss->status = 1;
                $mss->save();
            }
            //
            $data = [
                'id'        => $value->id,
                'message_id'=> $value->message_id,
                'user' => [
                    'id'    => $value->user->id,
                    'name'  => $value->user->full_name,
                    'link'  => '/user/'.$value->user->slug,
                ],
                'message'       => $value->message,
                'created_at'    => $value->created_at->format('d.m.Y - H:i'),
                'status'        => $value->status,
                'sort'          => $value->created_at,
                'file'          => $this->formatedFile($value->file),
            ];
            return $data;
        });

        $var = [
            'data' => $replys->getCollection()->reverse()->values(),
            'next_page_url' => $replys->nextPageUrl(),
        ];


        return $var;
    }

    /* public function reply($request)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function reply($data)
    {
        $reply = MessageReply::create([
            'user_id'       => $data['user_id'],
            'message_id'    => $data['message_id'],
            'message'       => $data['message'],
            'fid'           => !empty($data['fid']) ? $data['fid'] : null,
            'status'        => !empty($data['status']) ? $data['status'] : 0,
        ]);
        //
        $messages = $this->model->find($data['message_id']);
        $messages->updated_at = \Carbon::now();
        $messages->save();

        $addressee = $messages->addressee;
        $sender = $messages->sender;
        //
        event(new \Modules\Message\Events\NewReplyEvent($this->formatedReply($reply)));
        event(new \Modules\Message\Events\UpdateMessageBlockEvent($this->getMessages($addressee, 5), $addressee));
        event(new \Modules\Message\Events\UpdateMessageBlockEvent($this->getMessages($sender, 5), $sender));
        event(new \Modules\Message\Events\UpdateMessageListEvent($this->getMessages($addressee, 20), $addressee));
        event(new \Modules\Message\Events\UpdateMessageListEvent($this->getMessages($sender, 20), $sender));
        //
        return $reply;
    }

    /* public function setRead($id)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setRead($id)
    {
        $user = auth()->user();
        //
        MessageReply::where('message_id', $id)->where('user_id', '<>', $user->id)->update(['status' => 1]);
        //
        $block = $this->block($user->id);
        $inbox = $this->messageIn($user->id);
        event(new \Modules\Message\Events\NewMessageEvent($block, $inbox, $user->id));
    }

    /* public function messageIn
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getMessages($user = null, $paginate = 20)
    {
        $user = (is_numeric($user)) ? \User::findOrFail($user) : $user;
        // Вибираємо всі чати, та сортуємо по останньому повідомленню
        $query = $this->model->where('message.sender_id', $user->id)->orWhere('message.addressee_id', $user->id);
        $query->orderBy('message.updated_at', 'DESC');
        //$query->leftJoin('message_reply', 'message.id', '=', 'message_reply.message_id');
       // $query->where('message_reply.user_id', '<>', $user->id);
        //$query->orderBy('message_reply.created_at', 'DESC');
        //$query->groupby('message.id');
        $items = $query->paginate($paginate);

        $items->getCollection()->transform(function ($value) use ($user) {
            $userMsg = ($user->id == $value->sender_id) ? $value->addressee : $value->sender;
            $countNew = MessageReply::where('message_id', $value->id)->where('user_id', $userMsg->id)->where('status', 0)->count();
            $last = MessageReply::where('message_id', $value->id)->where('user_id', '<>', $user->id)->orderBy('created_at', 'DESC')->first();
            $data = [
                'id'   => $value->id,
                'user' => [
                    'id'    => $userMsg->id,
                    'name'  => $userMsg->full_name,
                    'link'  => '/user/'.$userMsg->slug,
                ],
                'message'       => ($last) ? $last->message : 'send_msg',
                'created_at'    => $value->updated_at->format('d.m.Y - H:i'),
                'countNew'      => $countNew,//$this->countNewReply($value->message_id),
            ];
            return $data;
        });

        $custom = collect([
            'countNew' => $this->countNew($user),
        ]);

        $data = $custom->merge($items);

        return $data;
    }

    /* public function findOrNew
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function findOrNew($addressee)
    {
        $user = auth()->user();

        $item = $this->model->where('sender_id', $user->id)->where('addressee_id', $addressee->id)->orWhere(function ($q) use ($user, $addressee){
            $q->where('sender_id', $addressee->id)->where('addressee_id', $user->id);
        })->first();

        if(!$item)
        {
            $item = $this->model->create([
                'sender_id'     => $user->id,
                'addressee_id'  => $addressee->id,
            ]);
            //
            $this->reply([
                'user_id'       => $user->id,
                'message_id'    => $item->id,
                'message'       => 'start_msg',
                'status'        => 0,
            ]);
            //
            $this->reply([
                'user_id'       => $addressee->id,
                'message_id'    => $item->id,
                'message'       => 'start_msg',
                'status'        => 0,
            ]);
        }
        //
        return $item;
    }

    /* public function remove($addressee)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function remove($addressee)
    {
        $user = auth()->user();
        //
        $item = $this->model->where('sender_id', $user->id)->where('addressee_id', $addressee->id)->orWhere(function ($q) use ($user, $addressee){
            $q->where('sender_id', $addressee->id)->where('addressee_id', $user->id);
        })->first();
        //
        $item->delete();
        //
        return true;
    }

    /* public function countNewReply($chat)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function countNewReply($message)
    {
        $message = (is_numeric($message)) ? $this->model->find($message) : $message;
        $user = auth()->user();
        $replies = MessageReply::where('message_id', $message->id)->where('user_id', '<>', $user->id)->where('status', 0)->count();
        return $replies;
    }

    /* public function getLastReply($message)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getLastReply($message)
    {
        $message = (is_numeric($message)) ? $this->model->find($message) : $message;
        $user = auth()->user();
        $reply = MessageReply::where('message_id', $message->id)->where('user_id', '<>', $user->id)->orderBy('created_at', 'DESC')->first();
        return $reply;
    }

    /* public function formatedReply($reply)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function formatedReply($reply)
    {
        $data = (object)[
            'id'        => $reply->id,
            'message_id'=> $reply->message_id,
            'user' => (object)[
                'id'    => $reply->user->id,
                'name'  => $reply->user->full_name,
                'link'  => '/user/'.$reply->user->slug,
            ],
            'message'       => $reply->message,
            'created_at'    => $reply->created_at->format('d.m.Y - H:i'),
            'status'        => $reply->status,
            'sort'          => $reply->created_at,
            'file'          => $this->formatedFile($reply->file),
        ];
        return $data;
    }

    /* public function formatedFile
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function formatedFile($file)
    {
        $data = null;
        if($file)
        {
            if(str_contains($file->filemime, 'image'))
            {
                $data = [
                    'type' => 'image',
                    'name' => $file->original,
                    'original' => '/uploads/'.$file->uri.'/'.$file->filename,
                    'thum' => route('filem.filem', [$file->uri, '100x100', $file->filename]),
                ];
            }
            else
            {
                $data = [
                    'type' => 'file',
                    'name' => $file->original,
                    'original' => '/uploads/'.$file->uri.'/'.$file->filename,
                    'ico'   => $this->mimeIco($file->filemime),
                ];
            }
        }
        return $data;
    }

    /* public function mimeIco
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function mimeIco($mime_type)
    {
        $icon_classes = array(
            // Media
            'image' => 'fa-file-image-o',
            'audio' => 'fa-file-audio-o',
            'video' => 'fa-file-video-o',
            // Documents
            'application/pdf' => 'fa-file-pdf-o',
            'application/msword' => 'fa-file-word-o',
            'application/vnd.ms-word' => 'fa-file-word-o',
            'application/vnd.oasis.opendocument.text' => 'fa-file-word-o',
            'application/vnd.openxmlformats-officedocument.wordprocessingml' => 'fa-file-word-o',
            'application/vnd.ms-excel' => 'fa-file-excel-o',
            'application/vnd.openxmlformats-officedocument.spreadsheetml' => 'fa-file-excel-o',
            'application/vnd.oasis.opendocument.spreadsheet' => 'fa-file-excel-o',
            'application/vnd.ms-powerpoint' => 'fa-file-powerpoint-o',
            'application/vnd.openxmlformats-officedocument.presentationml' => 'fa-file-powerpoint-o',
            'application/vnd.oasis.opendocument.presentation' => 'fa-file-powerpoint-o',
            'text/plain' => 'fa-file-text-o',
            'text/html' => 'fa-file-code-o',
            'application/json' => 'fa-file-code-o',
            // Archives
            'application/gzip' => 'fa-file-archive-o',
            'application/zip' => 'fa-file-archive-o',
            'application/x-7z-compressed' => 'fa-file-archive-o',
            'application/x-rar' => 'fa-file-archive-o',
        );
        foreach ($icon_classes as $text => $icon) {
            if (strpos($mime_type, $text) === 0) {
                return $icon;
            }
        }
        return 'fa-file-o';
    }

}
