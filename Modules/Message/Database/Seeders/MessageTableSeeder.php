<?php

namespace Modules\Message\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('uk_UA');

        $usersId = \Modules\User\Entities\User::where('id', '<>', 0)->select('id')->get();
        $usersId = collect($usersId)->pluck('id')->toArray();
        foreach(range(1, 100) as $i)
        {
            $message = \Modules\Message\Entities\Message::create([
                'from_user_id' => $faker->randomElement($usersId),
                'to_user_id' => $faker->randomElement($usersId),
                'parent_id' => 0,
                'subject'  => $faker->sentence(6),
                'message' => $faker->text(500),
                'category_id_from' => 2,
                'category_id_to' => 1,
                'status' => 0,
            ]);

            foreach(range(1, 10) as $y)
            {
                 \Modules\Message\Entities\Message::create([
                    'from_user_id' => $faker->randomElement([$message->from_user_id, $message->to_user_id]),
                    'to_user_id' => $faker->randomElement([$message->from_user_id, $message->to_user_id]),
                    'parent_id' => $message->id,
                    'subject'  => null,
                    'message' => $faker->text(500),
                    'category_id_from' => 2,
                    'category_id_to' => 1,
                    'status' => 0,
                ]);
            }

        }
    }
}
