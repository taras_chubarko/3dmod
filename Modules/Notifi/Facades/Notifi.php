<?php namespace Modules\Notifi\Facades;

use Illuminate\Support\Facades\Facade;

class Notifi extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Modules\Notifi\Repositories\NotifiRepository';
    }
}