<?php

namespace Modules\Notifi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Notifi\Repositories\NotifiRepository;

class NotifiController extends Controller
{
    public $repo;

    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(NotifiRepository $repo)
    {
        $this->repo = $repo;
    }
    /* public function get_notifi
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_notifi()
    {
        $user = auth()->user();
        $notifis = $this->repo->getMyNotifi($user);
        //event(new \Modules\Notifi\Events\NewNotifiEvent($notifis, $user));
        return response()->json($notifis, 200);
    }

    /* public function read
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function read($id)
    {
        $item = $this->repo->find($id);
        $item->status = 1;
        $item->save();

        return response()->json($this->repo->getMyNotifiCountNew($item->to_user_id), 200);
    }

    /* public function read_all
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function read_all()
    {
        $user = auth()->user();
        $this->repo->setReadAll($user);
    }
}
