<?php

Route::group(['middleware' => 'web', 'prefix' => 'api/v1/notifi', 'namespace' => 'Modules\Notifi\Http\Controllers'], function()
{
    Route::post('block/notifi', [
        'uses' 		=> 'NotifiController@get_notifi'
    ]);

    Route::post('{id}/read', [
        'uses' 		=> 'NotifiController@read'
    ]);

    Route::post('/read/all', [
        'uses' 		=> 'NotifiController@read_all'
    ]);

});

