<?php

namespace Modules\Notifi\Entities;

use Illuminate\Database\Eloquent\Model;
use Modules\User\Entities\User;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Notifi
 *
 * @property string  type
 * @property string  message
 * @property integer status
 * @property integer from_user_id
 * @property integer to_user_id
 * @property integer request_id
 *
 * @package Modules\Notifi\Entities
 */
class Notifi extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [];

    protected $connection = 'mysql';

    protected $guarded = ['_token'];

    protected $table = 'notifi';

    /* public function user
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function fromUser()
    {
        return $this->hasOne(\Modules\User\Entities\User::class, 'id', 'from_user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'from_user_id', 'id');
    }

    /* public function user
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function toUser()
    {
        return $this->hasOne(\Modules\User\Entities\User::class, 'id', 'to_user_id');
    }

    /* public function friendRequest
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function friendRequest()
    {
        return $this->hasOne(\Modules\User\Entities\FriendReq::class, 'id', 'request_id');
    }

}
