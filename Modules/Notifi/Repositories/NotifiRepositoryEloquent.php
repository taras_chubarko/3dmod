<?php

namespace Modules\Notifi\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Notifi\Repositories\NotifiRepository;
use Modules\Notifi\Entities\Notifi;
use Modules\Notifi\Events\NewNotifiEvent;

/**
 * Class NotofiRepositoryEloquent
 * @package namespace App\Repositories;
 */
class NotifiRepositoryEloquent extends BaseRepository implements NotifiRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Notifi::class;
    }

    /**
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /* public function add($data)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function add($data)
    {
        $this->model->create([
            'from_user_id'  => $data['from_user_id'],
            'to_user_id'    => $data['to_user_id'],
            'type'          => !empty($data['type']) ? $data['type'] : null,
            'request_id'    => !empty($data['request_id']) ? $data['request_id'] : null,
            'message'       => $data['message'],
            'status'        => 0,
        ]);

        $user = \User::findOrFail($data['to_user_id']);

        event(new \Modules\Notifi\Events\UpdateBlockEvent($this->getMyNotifi($user), $user));
        event(new \Modules\Notifi\Events\NewNotifiEvent($this->getMyNotifi($user, 20), $user));
    }

    /* public function getMyNotifi
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getMyNotifi($user, $paginate = 5)
    {
        $user = (is_numeric($user)) ? \User::findOrFail($user) : $user;

        if($user && is_object($user))
        {
            $notifi =  $this->model->where('to_user_id', $user->id)->orderBy('created_at', 'DESC')->paginate($paginate);

            $notifi->getCollection()->transform(function ($value) {

                $data = [
                    'id'    => $value->id,
                    'type'  => $value->type,
                    'user'  => [
                        'id'        => $value->fromUser->id,
                        'link' 	    => '/user/'.$value->fromUser->slug,
                        'avatar'    => route('filem.avatar', ['200x200', $value->fromUser->id]),
                        'name' 	    => $value->fromUser->full_name,
                    ],
                    'request' => [
                        'id'        => $value->request_id,
                    ],
                    'message'       => $value->message,
                    'created_at'    => $value->created_at->format('d.m.Y - H:i'),
                    'status'        => $value->status,
                    'animation'     => true,
                    'transition'    => 'fade',
                ];
                return $data;
            });

            $custom = collect([
                'countNew'          => $this->getMyNotifiCountNew($user->id),
                'countFriendReq'    => $this->getMyNotifiCountFriendReq($user->id),
            ]);
            $data = $custom->merge($notifi);
        }
        else
        {
            $data = ['error' => true, 'message' => 'no user or data'];
        }
        return $data;
    }

    /* public function getMyNotifiCountNew($to_user_id)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getMyNotifiCountNew($to_user_id)
    {
        return $this->model->where('to_user_id', $to_user_id)->where('status', 0)->count();
    }
    //
    public function getMyNotifiCountFriendReq($to_user_id)
    {
        return $this->model->where('to_user_id', $to_user_id)->where('status', 0)->where('type', 'friend_request')->count();
    }

    /* public function setReadAll($user)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setReadAll($user)
    {
        $user = (is_numeric($user)) ? \User::findOrFail($user) : $user;
        //
        if($user && is_object($user))
        {
            $this->model->where('to_user_id', $user->id)->update(['status' => 1]);
            event(new \Modules\Notifi\Events\UpdateBlockEvent($this->getMyNotifi($user), $user));
        }
    }

    /* public function load($notify)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function load($notify)
    {
        $data = [
            'id'    => $notify->id,
            'type'  => $notify->type,
            'user'  => [
                'id'        => $notify->fromUser->id,
                'link' 	    => '/user/'.$notify->fromUser->slug,
                'avatar'    => route('filem.avatar', ['200x200', $notify->fromUser->id]),
                'name' 	    => $notify->fromUser->full_name,
            ],
            'request' => [
                'id'        => $notify->request_id,
            ],
            'message'       => $notify->message,
            'created_at'    => $notify->created_at->format('d.m.Y - H:i'),
            'status'        => $notify->status,
        ];
        return $data;
    }


}
