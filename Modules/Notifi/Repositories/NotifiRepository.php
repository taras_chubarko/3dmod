<?php

namespace Modules\Notifi\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NotofiRepository
 * @package namespace App\Repositories;
 */
interface NotifiRepository extends RepositoryInterface
{
    public function add($data);
    //
    public function getMyNotifi($user, $paginate);
    //
    public function getMyNotifiCountNew($to_user_id);
    //
    public function getMyNotifiCountFriendReq($to_user_id);
    //
    public function setReadAll($user);
    //
    public function load($notify);
}
