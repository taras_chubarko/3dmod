<?php namespace Modules\Notifi\Listeners;

use Illuminate\Http\Request;
use Illuminate\Contracts\Mail\Mailer;

class NotifiEventListener
{
    public $request;
    protected $mailer;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request, Mailer $mailer)
    {
        $this->request = $request;
        $this->mailer = $mailer;
    }
    /* public function onSocialRegister
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
//    public function notifi($event)
//    {
//        $notifi = $event->data;
//        \Redis::publish('notifi', json_encode($notifi));
//    }
    /**
     * Регистрация слушателей для подписки.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
//        $events->listen(
//            'Modules\Notifi\Events\NewNotifiEvent',
//            'Modules\Notifi\Listeners\NotifiEventListener@notifi'
//        );

    }

}