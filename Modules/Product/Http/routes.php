<?php

Route::group(['middleware' => 'web', 'prefix' => 'api/v1/product', 'namespace' => 'Modules\Product\Http\Controllers'], function()
{
    Route::get('/find', 'ProductController@find');
    // Завантаження файлів
    Route::post('/upload/images', 'ProductImagesController@upload_images');
    Route::post('/upload/archives', 'ProductImagesController@upload_archives');
    Route::post('/upload/video', 'ProductImagesController@upload_video');
    Route::post('/upload/audio', 'ProductImagesController@upload_audio');
    //Видалення файлів
    Route::delete('/images/{id}', 'ProductImagesController@delete_images');
    Route::delete('/archives/{id}', 'ProductImagesController@delete_archives');
    Route::delete('/video/{id}', 'ProductImagesController@delete_video');
    Route::delete('/audio/{id}', 'ProductImagesController@delete_audio');
    // Формати файлів
    Route::get('/formats', 'ProductController@formats');
    // створення товару
    Route::post('/store', 'ProductController@store');
    Route::post('/store/compete/{id}', 'ProductController@store_compete');
    Route::delete('/{id}', 'ProductController@destroy');

    // Товари користувача
    Route::get('/lists', 'ProductController@lists');
    Route::get('/tags/{id}', 'ProductController@get_tags');
    // Ціни товару по тарифу
    Route::post('/prices', 'ProductController@prices');
    // Реклама товару
    Route::post('/reklama', 'ProductController@reklama');
    // товар
    Route::get('/load/{slug}', 'ProductController@slug');

});

Route::group(['middleware' => ['web', 'Modules\Admin\Http\Middleware\AdminMDW'], 'prefix' => 'api/admin/product', 'namespace' => 'Modules\Product\Http\Controllers'], function()
{
    Route::resource('tariff', 'AdminProductTariffController');
    Route::post('/tariff/activate/{id}', 'AdminProductTariffController@activate');
    Route::resource('reklama', 'AdminProductReklamaController');
    Route::post('/reklama/activate/{id}', 'AdminProductReklamaController@activate');

});
