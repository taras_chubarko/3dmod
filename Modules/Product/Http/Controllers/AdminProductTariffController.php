<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\ProductTariff;

class AdminProductTariffController extends Controller
{
    protected $model;

    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(ProductTariff $model)
    {
        $this->model = $model;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $data['world'] = $this->model->where('vid', 'world')->orderBy('sort', 'ASC')->get(['status','sort', 'vid', 'category', 'amount', 'persent', 'pix', 'rate']);
        $data['sng'] = $this->model->where('vid', 'sng')->orderBy('sort', 'ASC')->get(['status','sort', 'vid', 'category', 'amount', 'amount_sng', 'persent', 'pix', 'rate']);

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($data, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $item = $this->model->create($request->all());

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($item, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('product::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('product::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->model->where('_id', $id)->update($request->all());

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json('ok', 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    /* public function activate
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function activate(Request $request, $id)
    {
        $item = $this->model->find($id);
        $item->status = $request->status;
        $item->save();
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json('ok', 200, $headers, JSON_UNESCAPED_UNICODE);
    }
}
