<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\AllCgStorage\Entities\AllCgStorage;
use Modules\Product\Repositories\ProductRepository;
use Google\Cloud\Vision\VisionClient;
use Modules\Product\Entities\ProductTariff;
use Modules\Product\Entities\ProductReklama;
use Modules\Catalog\Entities\CatalogItems;
use Modules\Product\Entities\ProductMongo;


class ProductController extends Controller
{
    protected $repository;
    protected $tariff;
    protected $reklama;
    protected $model;
    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(ProductRepository $repository, ProductTariff $tariff, ProductReklama $reklama, ProductMongo $model)
    {
        $this->repository = $repository;
        $this->tariff = $tariff;
        $this->reklama = $reklama;
        $this->model = $model;
    }
    /* public function find
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function find()
    {
        $products = $this->repository->getFakeFind();
        return response()->json($products, 200);
    }

    /* public function formats
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function formats()
    {
        $items =  CatalogItems::where('catalog_id', '5b053206bd302525f43523c1')->get();
        if($items) {
            $items->transform(function ($value) {
                $data = [
                    'id' => $value->_id,
                    'name' => $value->name,
                ];
                return $data;
            });
        }
        return response()->json($items, 200);
    }

    /* public function store
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function store(Request $request)
    {
        $product = $this->repository->createProduct($request);
        return response()->json($product, 200);
    }

    /* public function store_compete
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function store_compete($id)
    {
        $product = $this->repository->find($id);
        //$product = $this->repository->loadForEdit($product);
        return response()->json($product, 200);
    }
    /* public function lists
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function lists()
    {
        $products = $this->repository->userProducts();
        return response()->json($products, 200);
    }

    /* public function get_tags
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_tags($id)
    {
        $file = \Filem:: find($id);

        $image_url = config('app.url').'/uploads/'.$file->uri.'/'.$file->filename;

        $vision = new VisionClient([
            'keyFilePath' => public_path().'/uploads/keyfile.json',
            'projectId' => 'vertical-datum-200814',
        ]);

        //$image_url = config('app.url').'/uploads/products/preview/sHCAjMB2Bk.jpeg';

        $image = $vision->image(file_get_contents($image_url), ['LABEL_DETECTION']);
        $result = $vision->annotate($image);
        $items = [];
        foreach ($result->labels() as $label)
        {
            $items[] = $label->description();
        }

        return response()->json($items, 200);
    }

    /* public function prices
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function prices()
    {
        $world = $this->tariff->where('vid', 'world')->where('status', 1)->get();
        if($world) {
            $world->transform(function ($value) {
                $data = [
                    'vid' => $value->vid,
                    'amount' => $value->amount,
                    'amount_sng' => $value->amount_sng,
                    'category' => $value->category,
                    'persent' => $value->persent,
                    'pix' => $value->pix,
                    'rate' => $value->rate,
                ];
                return $data;
            });
        }
        //
        $sng = $this->tariff->where('vid', 'sng')->where('status', 1)->get();
        if($sng){
            $sng->transform(function ($value) {
                $data = [
                    'vid' => $value->vid,
                    'amount' => $value->amount,
                    'amount_sng' => $value->amount_sng,
                    'category' => $value->category,
                    'persent' => $value->persent,
                    'pix' => $value->pix,
                    'rate' => $value->rate,
                ];
                return $data;
            });
        }

        $items['world'] = $world;
        $items['sng'] = $sng;
        return response()->json($items, 200);
    }

    /* public function reklama
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function reklama()
    {
        $items['blocks'] = $this->reklama->where('type', 'block')->where('status', 1)->get();
        $items['rotations'] = $this->reklama->where('type', 'rotation')->where('status', 1)->get();
        $items['tops'] = $this->reklama->where('type', 'top')->where('status', 1)->get();

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($items, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function destroy
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function destroy($id)
    {
        $product = $this->repository->find($id);

        if(!empty($product->files))
        {
            foreach ($product->files as $fileItems){
                foreach ($fileItems as $item){
                    if(isset($item['fid'])){
                        \Filem::destroy($item['fid']);
                    }
                }
            }
        }
        $product->delete();
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json('ok', 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function slug
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function slug($slug)
    {
        $item = $this->model->where('slug', $slug)->first();
        $data['id'] = $item->id;
        $data['name'] = $item->name;

        $breadcrumbs = CatalogItems::whereIn('_id', $item->category)->select('name', 'ico')->get();
        $breadcrumbs->transform(function ($value) {
            return[
                'id' => $value->id,
                'name' => $value->name,
                'ico' => $value->ico,
            ];
        });

        $data['breadcrumbs'] = $breadcrumbs;

        $data['price'] = $item->price;

        $data['images'] = array();

        if($item->files && isset($item->files['images'])){
            $data['images'] = collect($item->files['images'])->pluck('fid');
        }






        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($data, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

}
