<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\ProductReklama;

class AdminProductReklamaController extends Controller
{
    protected $model;

    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(ProductReklama $model)
    {
        $this->model = $model;
    }

    /* public function index
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function index()
    {
        $items['blocks'] = $this->model->where('type', 'block')->get();
        $items['rotations'] = $this->model->where('type', 'rotation')->get();
        $items['tops'] = $this->model->where('type', 'top')->get();

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($items, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function store
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function store(Request $request)
    {
        $item = $this->model->create($request->all());
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($item, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function update
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function update(Request $request, $id)
    {
        $item = $this->model->find($id);
        $request->offsetUnset('created_at');
        $request->offsetUnset('updated_at');
        $request->offsetUnset('_id');

        foreach ($request->all() as $k=>$val)
        {
            $item->{$k} = $val;
        }
        $item->save();

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($item, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function activate
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function activate(Request $request, $id)
    {
        $item = $this->model->find($id);
        $item->status = $request->status;
        $item->save();
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json('ok', 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function destroy
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function destroy($id)
    {
        $this->model->destroy($id);
        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json('ok', 200, $headers, JSON_UNESCAPED_UNICODE);
    }
}
