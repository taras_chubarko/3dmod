<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ProductImagesController extends Controller
{
    /* public function upload_images
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function upload_images(Request $request)
    {
        $user = auth()->user();
        $file = \Filem::make($request->image, 'products/preview', $user->id, $status = 0, $request->file_id);

        $response = [
            'key' => (int) $request->key,
            'fid' => $file->id,
            'id'  => $request->file_id,
            'img' => route('filem.filem', [$file->uri, '68x68', $file->filename]),
            'loading' => false,
            'load' => 100,
            'name' => $file->original,
        ];
        return response()->json($response, 200);
    }

    /* public function delete_images
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function delete_images($id)
    {
        $file = \Filem::findByField('file_id', $id)->first();
        \Filem::destroy($file->id);
        return response()->json('ok', 200);
    }

    /* public function upload_archives
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function upload_archives(Request $request)
    {
        $user = auth()->user();
        $file = \Filem::make($request->file, 'products/archives', $user->id, $status = 0, $request->file_id);

        $response = [
            'key' => (int) $request->key,
            'fid' => $file->id,
            'id'  => $request->file_id,
            'name' => $file->original,
            'path' => '/uploads/products/archives/'.$file->filename,
            'soft' => null,
            'loading' => false,
            'load' => 100,
        ];

        return response()->json($response, 200);
    }

    /* public function delete_archives
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function delete_archives($id)
    {
        $file = \Filem::findByField('file_id', $id)->first();
        \Filem::destroy($file->id);
        return response()->json('ok', 200);
    }

    /* public function upload_video
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function upload_video(Request $request)
    {
        $user = auth()->user();
        $file = \Filem::make($request->file, 'products/video', $user->id, $status = 0, $request->file_id);

        $response = [
            'key' => (int) $request->key,
            'fid' => $file->id,
            'id'  => $request->file_id,
            'name' => $file->original,
            'path' => '/uploads/products/video/'.$file->filename,
            'soft' => null,
            'loading' => false,
            'load' => 100,
        ];

        return response()->json($response, 200);
    }

    /* public function delete_video
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function delete_video($id)
    {
        $file = \Filem::findByField('file_id', $id)->first();
        \Filem::destroy($file->id);
        return response()->json('ok', 200);
    }

    /* public function upload_audio
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function upload_audio(Request $request)
    {
        $user = auth()->user();
        $file = \Filem::make($request->file, 'products/audio', $user->id, $status = 0, $request->file_id);

        $response = [
            'key' => (int) $request->key,
            'fid' => $file->id,
            'id'  => $request->file_id,
            'name' => $file->original,
            'path' => '/uploads/products/audio/'.$file->filename,
            'soft' => null,
            'loading' => false,
            'load' => 100,
        ];

        return response()->json($response, 200);
    }

    /* public function delete_audio
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function delete_audio($id)
    {
        $file = \Filem::findByField('file_id', $id)->first();
        \Filem::destroy($file->id);
        return response()->json('ok', 200);
    }

}
