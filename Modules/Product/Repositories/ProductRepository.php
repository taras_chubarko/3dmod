<?php

namespace Modules\Product\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProductRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProductRepository extends RepositoryInterface
{
    public function getFakeFind();
    //
    public function createProduct($request);
    //
    public function loadForEdit($product);
    //
    public function userProducts();
}
