<?php

namespace Modules\Product\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Product\Repositories\ProductRepository;
use Modules\Product\Entities\ProductMongo;
use Illuminate\Pagination\LengthAwarePaginator;
use Faker\Factory as Faker;

/**
 * Class ProductRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProductRepositoryEloquent extends BaseRepository implements ProductRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProductMongo::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /* public function getFakeFind
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getFakeFind()
    {
        $faker = Faker::create('uk_UA');

        \Carbon::setLocale('uk');
        foreach(range(1, 100) as $i)
        {
            $name = $faker->lastName.' '.$faker->firstName;
            $tpl[] = [
                'tpl' => 'product',
                'img' => route('fake.image', ['217x160', $faker->numberBetween(1, 151)]),
                'division' => $faker->randomElement(['midi', 'maxi', 'mini', 'ultra', 'free']),
                'name' => $faker->sentence(3),
                'category' => $faker->word,
                'downloads' => $faker->numberBetween(0, 500),
                'comments' => $faker->numberBetween(0, 500),
                'views' => $faker->numberBetween(0, 500),
                'likes' => $faker->numberBetween(0, 500),
                'price' => $faker->numberBetween(0, 500),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
            ];
        }
        //
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $col = collect($tpl);
        $perPage = 12;
        $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $products = new LengthAwarePaginator($currentPageSearchResults, count($col), $perPage);
        return $products;
    }

    /* public function createProduct($request)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function createProduct($request)
    {
        $user = auth()->user();
        //
        $data = $request->all();
        unset($data['_id']);

        if(is_null($request->_id))
        {
            $data['user_id'] = (int) $user->id;
            $data['status'] = (int) 0;

            $product = $this->model->create($data);
        }
        else
        {
            $this->model->where('_id', $request->_id)->update($data);
            $product = $this->model->find($request->_id);
        }
//
//        event( new \Modules\Product\Events\ProductCreateEvent($this->loadForEdit($product)));
//
        return $product;//$this->loadForEdit($product);
    }

    /* public function load($product)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function loadForEdit($product)
    {
        $product = (object)[
            '_id'        => $product->_id,
            'name'      => $product->name,
            'files'     => $product->files,
            'category'  => $product->category,
            'step'      => $product->step,
            'description' => $product->description,
            'tags'      => $product->tags,
        ];
        //
        return $product;
    }

    /* public function userProducts
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function userProducts()
    {
        $user = auth()->user();
        //
        $products = $this->model->where('user_id', $user->id)
            ->orderBy('created_at', 'DESC')->paginate();
        //
        if($products)
        {
            $products->getCollection()->transform(function ($value) {

                $data = [
                    'id'        => $value->id,
                    'name'      => $value->name,
                    'step'      => $value->step,
                    'status'    => $value->status,
                    'status'    => $value->status,
                    'price'     => !empty($value->price['amount']) ? $value->price['amount'] : 0,
                    'division'  => !empty($value->price['category']) ? $value->price['category'] : 'free',
                    'downloads' => 0,
                    'comments'  => 0,
                    'views'     => 0,
                    'likes'     => 0,
                    'category'  => null,
                    'img'       => $value->image('150x150'),
                    'rating'    => 0,
                    'tags'      => $value->formats(),
                    'slug'      => $value->slug,
                ];
                return $data;
            });
        }
        return $products;
    }
    
}
