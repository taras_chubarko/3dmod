<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Modules\Catalog\Entities\CatalogItems;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class ProductMongo extends Eloquent implements Transformable
{
    use TransformableTrait;
    use Sluggable;
    //
    protected $connection = 'mongodb';
    protected $collection = 'products';
    //
    protected $guarded = ['_id', '_token'];
    //
    /* public function sluggable
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source'    => 'name',
                'onUpdate'  => true,
            ]
        ];
    }

    /* public function getImageAttribute
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function image($size = '350x350')
    {
        if($this->files){
            if(isset($this->files['images'])){
                return route('image.preview', [$this->files['images'][0]['fid'], $size]);
            }
            else{
                return route('image.preview', [0, $size]);
            }
        }else{
            return route('image.preview', [0, $size]);
        }
    }

    /* public function tags
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function tagsList($limit = 3)
    {
        $tags = null;
        if(!empty($this->tags))
        {
            if($limit > 0)
            {
                $tags = collect($this->tags)->take($limit)->values()->implode(', ');
            }
        }
        return $tags;
    }

    /* public function formats
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function formats($limit = 3)
    {
        if($this->files){
            if(isset($this->files['formats'])){
                $items = CatalogItems::whereIn('_id', $this->files['formats'][0]['value'])->take($limit)->get();
                $items = $items->pluck('name.en')->implode(', ');
                return $items;
            }else{
                return null;
            }
        }else{
            return null;
        }
    }

}
