<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ProductCategoryMongo extends Eloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'products_category';
    //
    protected $fillable = [];
    protected $guarded = ['_token'];
}
