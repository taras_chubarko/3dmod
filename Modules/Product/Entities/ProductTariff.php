<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ProductTariff extends Eloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'products_tariff';
    //
    protected $fillable = [];
    protected $guarded = ['_token'];

    protected $attributes = [
        'status'        => 0,
        'sort'          => 0,
        'amount_sng'    => 0,
    ];

    /* public function setStatusAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = (int) $value;
    }

    /* public function setStatusAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setSortAttribute($value)
    {
        $this->attributes['sort'] = (int) $value;
    }

    /* public function setAmountAttribute
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setAmountAttribute($value)
    {
        $this->attributes['amount'] = (int) $value;
    }

    /* public function setAmountAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setAmountSngAttribute($value)
    {
        $this->attributes['amount_sng'] = (int) $value;
    }


    /* public function setPersent
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setPersentAttribute($value)
    {
        $this->attributes['persent'] = (int) $value;
    }
    /* public function setPersent
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setPixAttribute($value)
    {
        $this->attributes['pix'] = (int) $value;
    }
    /* public function setPersent
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setRateAttribute($value)
    {
        $this->attributes['rate'] = (int) $value;
    }

}
