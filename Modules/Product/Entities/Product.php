<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class Product.
 *
 * @package namespace App\Entities;
 */
class Product extends Model implements Transformable
{
    use TransformableTrait;
    use Sluggable;

    protected $connection = 'mysql';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];
    protected $guarded = ['id', '_token'];
    protected $table = 'product';
    protected $primaryKey = 'id';

    /* public function sluggable
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    /* public function product_category_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function product_category_table()
    {
        return $this->belongsToMany(\Modules\Taxonomy\Entities\TaxonomyTerm::class, 'product_category', 'product_id', 'category_id')->withPivot('category_id', 'sort');
    }

    /* public function setCategoryAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setCategoryAttribute($value)
    {
        $this->product_category_table()->detach();
        if($value)
        {
            foreach ($value as $k => $item)
            {
                $this->product_category_table()->attach($this->id, [
                    'category_id' => $item,
                    'sort'  => $k,
                ]);
            }
        }
    }

    /* public function getCategoryAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getCategoryAttribute()
    {
        $field = new \stdClass;
        $field->title = 'Категорія товару';
        $field->values = $this->product_category_table->pluck('pivot.category_id', 'pivot.sort');

        unset($this->product_category_table);
        return $field;
    }

    /* public function getSesAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getSesAttribute($value)
    {
        return (int) $value;
    }

    /* public function product_images_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function product_images_table()
    {
        return $this->belongsToMany(\Modules\Filem\Entities\Filem::class, 'product_images', 'product_id', 'fid')->withPivot('fid', 'sort');
    }

    /* public function product_archives_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function product_archives_table()
    {
        return $this->belongsToMany(\Modules\Filem\Entities\Filem::class, 'product_archives', 'product_id', 'fid')->withPivot('fid', 'sort');
    }

    /* public function product_video_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function product_video_table()
    {
        return $this->belongsToMany(\Modules\Filem\Entities\Filem::class, 'product_video', 'product_id', 'fid')->withPivot('fid', 'sort');
    }

    /* public function product_audio_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function product_audio_table()
    {
        return $this->belongsToMany(\Modules\Filem\Entities\Filem::class, 'product_audio', 'product_id', 'fid')->withPivot('fid', 'sort');
    }

    /* public function product_formats_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function product_formats_table()
    {
        return $this->belongsToMany(\Modules\Taxonomy\Entities\TaxonomyTerm::class, 'product_formats', 'product_id', 'format_id')->withPivot('format_id', 'key', 'sort');
    }

    /* public function setFilesAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setFilesAttribute($values)
    {
        $this->product_archives_table()->detach();
        $this->product_audio_table()->detach();
        $this->product_formats_table()->detach();
        $this->product_images_table()->detach();
        $this->product_video_table()->detach();

        //
        if($values['archives'])
        {
            foreach ($values['archives'] as $k => $item)
            {
                $this->product_archives_table()->attach($this->id, [
                    'fid'   => $item['fid'],
                    'sort'  => $k,
                ]);
            }
        }
        //
        if($values['audio'])
        {
            foreach ($values['audio'] as $k => $item)
            {
                $this->product_audio_table()->attach($this->id, [
                    'fid'   => $item['fid'],
                    'sort'  => $k,
                ]);
            }
        }
        //
        if($values['formats'])
        {
            foreach ($values['formats'] as $k => $items)
            {
                if($items['value'])
                {
                    foreach ($items['value'] as $sort => $item)
                    {
                        $this->product_formats_table()->attach($this->id, [
                            'format_id'     => $item,
                            'key'           => $k,
                            'sort'          => $sort,
                        ]);
                    }
                }
            }
        }
        //
        if($values['images'])
        {
            foreach ($values['images'] as $k => $item)
            {
                $this->product_images_table()->attach($this->id, [
                    'fid'   => $item['fid'],
                    'sort'  => $k,
                ]);
            }
        }
        //
        if($values['video'])
        {
            foreach ($values['video'] as $k => $item)
            {
                $this->product_video_table()->attach($this->id, [
                    'fid'   => $item['fid'],
                    'sort'  => $k,
                ]);
            }
        }
    }

    /* public function getFilesAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getFilesAttribute()
    {
        $field = new \stdClass;
        $field->title = 'Файли товару';

        $archives = [];
        $formats = [];
        if($this->product_archives_table->count())
        {
            foreach ($this->product_archives_table as $item)
            {
                $archives[$item->pivot->sort] = [
                    'key'   => (int) $item->pivot->sort,
                    'fid'   => (int) $item->id,
                    'id'    => (int) $item->file_id,
                    'name'  => data_get($item, 'original'),
                    'path'  => '/uploads/products/archives/'.$item->filename,
                    'soft'  => null,
                    'loading' => false,
                    'load'  => 100,
                ];
                $formats[$item->pivot->sort]['value'] = $this->product_formats_table->where('pivot.key', $item->pivot->sort)->pluck('pivot.format_id');
            }
        }
        //
        $images = [];
        if($this->product_images_table->count())
        {
            foreach ($this->product_images_table as $item)
            {
                $images[$item->pivot->sort] = [
                    'key' => (int) $item->pivot->sort,
                    'fid' => (int) $item->id,
                    'id'  => (int) $item->file_id,
                    'img' => route('filem.filem', [$item->uri, '68x68', $item->filename]),
                    'loading' => false,
                    'load' => 100,
                    'name' =>data_get($item, 'original'),
                ];
            }
        }
        //
        $audio = [];
        if($this->product_audio_table->count())
        {
            foreach ($this->product_audio_table as $item)
            {
                $audio[$item->pivot->sort] = [
                    'key' => (int) $item->pivot->sort,
                    'fid' => (int) $item->id,
                    'id'  => (int) $item->file_id,
                    'name' =>data_get($item, 'original'),
                    'path' => '/uploads/products/audio/'.$item->filename,
                    'soft' => null,
                    'loading' => false,
                    'load' => 100,
                ];
            }
        }
        //
        $video = [];
        if($this->product_video_table->count())
        {
            foreach ($this->product_video_table as $item)
            {
                $video[$item->pivot->sort] = [
                    'key' => (int) $item->pivot->sort,
                    'fid' => (int) $item->id,
                    'id'  => (int) $item->file_id,
                    'name' =>data_get($item, 'original'),
                    'path' => '/uploads/products/video/'.$item->filename,
                    'soft' => null,
                    'loading' => false,
                    'load' => 100,
                ];
            }
        }

        $field->values = [
            'archives'  => $archives,
            'audio'     => $audio,
            'formats'   => $formats,
            'images'    => $images,
            'video'     => $video,
        ];

        unset($this->product_video_table);
        unset($this->product_audio_table);
        unset($this->product_images_table);
        unset($this->product_formats_table);
        unset($this->product_archives_table);
        return $field;
    }



}
