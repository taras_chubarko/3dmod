<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ProductReklama extends Eloquent
{
    //
    protected $connection = 'mongodb';
    protected $collection = 'products_reklama';
    //
    protected $fillable = [];
    protected $guarded = ['_token'];

    protected $attributes = [
        'status'        => 0,
        'sort'          => 0,
    ];

    /* public function setStatusAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setStatusAttribute($value)
    {
        $this->attributes['status'] = (int) $value;
    }

    /* public function setStatusAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setSortAttribute($value)
    {
        $this->attributes['sort'] = (int) $value;
    }

    /* public function setTopAttribute
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setTopAttribute($value)
    {
        $this->attributes['top'] = (int) $value;
    }

    /* public function setRotationAttribute
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setRotationAttribute($value)
    {
        $this->attributes['rotation'] = (int) $value;
    }

    /* public function setPixAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function setPixAttribute($value)
    {
        $this->attributes['pix'] = (int) $value;
    }

}
