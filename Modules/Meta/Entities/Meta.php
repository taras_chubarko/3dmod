<?php

namespace Modules\Meta\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Meta extends Model implements Transformable
{
    use TransformableTrait;

    protected $connection = 'mysql';

    protected $table = 'meta';

    protected $fillable = [];
    
    protected $guarded = ['_token'];

}
