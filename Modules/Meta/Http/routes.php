<?php

Route::group(['middleware' => 'web', 'prefix' => 'meta', 'namespace' => 'Modules\Meta\Http\Controllers'], function()
{
	Route::get('/', 'MetaController@index');
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'Modules\Cms\Http\Middleware\Admin'], 'as' => 'admin.', 'prefix' => 'admin', 'namespace' => 'Modules\Meta\Http\Controllers'], function()
{
	Route::resource('meta', 'MetaController');
});