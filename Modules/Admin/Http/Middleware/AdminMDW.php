<?php

namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminMDW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        if(auth()->check())
//        {
//            if(auth()->user()->hasRole('admin'))
//            {
//                return $next($request);
//            }
//        }
//        else
//        {
//            return redirect()->guest('/');
//        }
        return $next($request);
    }
}
