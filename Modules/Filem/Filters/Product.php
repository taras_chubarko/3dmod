<?php

namespace Modules\Filem\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Product implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(200, 200);
    }
}