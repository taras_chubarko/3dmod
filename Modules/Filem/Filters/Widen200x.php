<?php

namespace Modules\Filem\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Widen200x implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->widen(200)->encode('jpg', 85);
    }
}