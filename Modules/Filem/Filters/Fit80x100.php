<?php

namespace Modules\Filem\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Fit80x100 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(80, 100);
    }
}