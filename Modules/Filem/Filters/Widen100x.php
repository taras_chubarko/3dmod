<?php

namespace Modules\Filem\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Widen100x implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->widen(100)->encode('jpg', 85);
    }
}