<?php

namespace Modules\Filem\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Fit127x127 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(127, 127);
    }
}