<?php

namespace Modules\Filem\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface FilemRepository
 * @package namespace App\Repositories;
 */
interface FilemRepository extends RepositoryInterface
{
    /*
     *
     */
    public function make($file, $uri, $user_id, $status);
    /*
     *
     */
    public function setStatus($fid, $status);
    /*
     *
     */
    public function destroy($fid);
    /*
     *
     */
    public function tables();
}
