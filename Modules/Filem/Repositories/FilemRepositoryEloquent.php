<?php

namespace Modules\Filem\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Filem\Repositories\FilemRepository;
use Modules\Filem\Entities\Filem;

/**
 * Class FilemRepositoryEloquent
 * @package namespace App\Repositories;
 */
class FilemRepositoryEloquent extends BaseRepository implements FilemRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Filem::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /* public function make($file, $uri, $status)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function make($file, $uri, $user_id = null, $status = 1, $file_id = null)
    {
        $user = ($user_id) ? \User::find($user_id) : auth()->user();
       
        $destinationPath = public_path('uploads/'.$uri.'/');
        
        $filename = str_random(10). '.' . $file->getClientOriginalExtension();
        
        $uploadSuccess = $file->move($destinationPath, $filename);
        
        $fid            = new $this->model;
        $fid->user_id 	= ($user) ? $user->id : 0;
        $fid->filename 	= $filename;
        $fid->uri 	= $uri;
        $fid->filemime 	= $uploadSuccess->getMimeType();
        $fid->ext 	= $file->getClientOriginalExtension();
        $fid->original 	= $file->getClientOriginalName();
        $fid->filesize 	= filesize($uploadSuccess);
        $fid->file_id 	= $file_id;
        $fid->status 	= $status;
        $fid->save();
        
        return $fid;
    }
    /* public function setStatus($fid, $status);
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setStatus($fid, $status = 1)
    {
        $file = $this->model->find($fid);
        $file->status = 1;
        $file->save();
        return $fid;
    }
    /* public function destroy($fid);
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function destroy($fid)
    {
        $file = $this->model->find($fid);
        // Ищем файл по папкам
        if($file){
            $manuals = \File::allFiles('uploads');
            foreach($manuals as $manual)
            {
                $dirs[] = pathinfo($manual);
            }

            foreach($dirs as $dir)
            {
                if($dir['basename'] == $file->filename)
                {
                    \File::delete($dir['dirname'].'/'.$dir['basename']);
                }
            }
            $tables = $this->tables();
            if($tables)
            {
                foreach($tables as $table)
                {
                    \DB::table($table)->where('fid', $fid)->delete();
                }
            }
            $file->delete();
        }
    }
    /* public function tables();
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function tables()
    {
        //$tables = \DB::select('SHOW TABLES');
        //$tb = 'Tables_in_'.config('database.connections.mysql.database');
        //$items = array();
        //foreach($tables as $key => $table)
        //{
        //    $table = (array) $table;
        //    if (\Schema::hasColumn($table[$tb], 'fid'))
        //    {
        //        $items[] = $table[$tb];
        //    }
        //}
        $items = config('filem.images_table');
        return $items;
    }
}
