<?php

namespace Modules\Filem\Entities;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Filem
 *
 * @property string uri
 * @property string filename
 *
 * @package Modules\Filem\Entities
 */
class Filem extends Model implements Transformable
{
    use TransformableTrait, HybridRelations;

    /** @var string  */
    protected $connection = 'mysql';

    /** @var string  */
    protected $table = 'filemanager';

    /** @var array  */
    protected $fillable = [];

    /** @var array  */
    protected $guarded = ['_token'];

}
