<?php

namespace Modules\Filem\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Filem\Repositories\FilemRepository;
use Faker\Factory as Faker;

class FilemController extends Controller
{
    /*
    *
    */
    protected $filem;
    /* public function __construct
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct(FilemRepository $filem)
    {
        $this->filem = $filem;
    }
    /* public function upload_image
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function upload_image(Request $request)
    {
        $user_id = (auth()->user()) ? auth()->user()->id : 2;
        if($request->has('file_id'))
        {
            $file = \Filem::make($request->image, $request->folder, $user_id, $status = 1, $request->file_id);
        }
        else
        {
            $file = \Filem::make($request->image, $request->folder, $user_id, $status = 1);
        }
        return response()->json($file, 200);
    }

    /* public function upload_analize
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function upload_analize(Request $request)
    {
        $user_id = (auth()->user()) ? auth()->user()->id : 2;

        $file = \Filem::make($request->image, $request->folder, $user_id, $status = 1, $request->file_id);

        $image_url = config('app.url').'/uploads/'.$file->uri.'/'.$file->filename;
        $api_credentials = array(
            'key'       => 'acc_3ec8c498abf66cb',
            'secret'    => 'a5f79cc54bca9bceb1524d8cfc177e02'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.imagga.com/v1/tagging?url='.urlencode($image_url).'&language='.$request->lang);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_USERPWD, $api_credentials['key'].':'.$api_credentials['secret']);
        $response = curl_exec($ch);
        curl_close($ch);
        $json_response = json_decode($response);
        //var_dump($json_response);
        $file->tags = $json_response->results[0]->tags;


        return response()->json($file, 200);
    }
    /* public function upload_images
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function upload_images(Request $request)
    {
        if($request->hasFile('file_data'))
        {
            $uri = $request->uri;
            $actions = $request->actions;
            $name = $request->name;
            $file_id = $request->file_id;
            $image = $this->filem->make($request->file_data, $uri, null, 0);
            $view = view('admin::filem.im', compact('name', 'image', 'actions', 'uri', 'file_id'))->render();
            return response()->json(['result' => $view], 200);
        }
    }
    /* public function get_image
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_image($uri = null, $size = null, $name = null)
    {
        //dd($name);
        if(!is_null($size) && !is_null($name)){
            $size = explode('x', $size);
            $cache_image = \Image::cache(function($image) use($uri, $size, $name){
                return $image->make(url('/uploads/'.$uri.'/'.$name))->fit($size[0], $size[1]);
            }, 1440); // cache for 10 minutes
            return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
        } else {
            abort(403);
        }
    }
    /* public function get_filem
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_filem($uri = null, $size = null, $name = null)
    {
        if(!is_null($size) && !is_null($name)){
            $sizeArr = explode('x', $size);
            $patentFolder = public_path().'/uploads/'.$uri;

            try{
                $list = \File::directories($patentFolder);
                $newFolder = $patentFolder.'/'.$size;

                if(!in_array($newFolder, $list))
                {
                    \File::makeDirectory($newFolder, $mode = 0777, true, true);
                }

                $file = $patentFolder.'/'.$size.'/'.$name;

                if(\File::exists($file) == false)
                {
                    $img = \Image::make($patentFolder.'/'.$name)->fit($sizeArr[0], $sizeArr[1]);
                    $img->save($file, 80);
                }

                $cache_image = \Image::cache(function($image) use($file){
                    $im = $image->make($file);
                    return $im;
                }, 1440); // cache for 10 minutes
                return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
            }
            catch (\Exception $e)
            {
                $img = \Image::canvas($sizeArr[0], $sizeArr[1], '#CCCCCC');
                return $img->response();
            }
        }
        else {
            $sizeArr = explode('x', $size);
            $img = \Image::canvas($sizeArr[0], $sizeArr[1], '#CCCCCC');
            return $img->response();
        }

    }

    /* public function get_filem_subfolder
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_filem_subfolder($folder = null, $subfolder =null, $size = null, $name = null)
    {
        //dd($folder);
        if(!is_null($size) && !is_null($name)){
            $sizeArr = explode('x', $size);
            $patentFolder = public_path().'/uploads/'.$folder.'/'.$subfolder;

            try{
                $list = \File::directories($patentFolder);
                $newFolder = $patentFolder.'/'.$size;

                if(!in_array($newFolder, $list))
                {
                    \File::makeDirectory($newFolder, $mode = 0777, true, true);
                }

                $file = $patentFolder.'/'.$size.'/'.$name;

                if(\File::exists($file) == false)
                {
                    $img = \Image::make($patentFolder.'/'.$name)->fit($sizeArr[0], $sizeArr[1]);
                    $img->save($file, 80);
                }

                $cache_image = \Image::cache(function($image) use($file){
                    $im = $image->make($file);
                    return $im;
                }, 1440); // cache for 10 minutes
                return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
            }
            catch (\Exception $e)
            {
                $img = \Image::canvas($sizeArr[0], $sizeArr[1], '#CCCCCC');
                return $img->response();
            }
        }
        else {
            $sizeArr = explode('x', $size);
            $img = \Image::canvas($sizeArr[0], $sizeArr[1], '#CCCCCC');
            return $img->response();
        }
    }
    /* public function get_avatar
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_avatar($size = null, $user_id = null)
    {
        $defaultImage = public_path().'/uploads/avatar/nouser.png';
        if(!is_null($size) && !is_null($user_id)){

            $sizeArr = explode('x', $size);
            $patentFolder = public_path().'/uploads/avatar/';
            $user = \User::find($user_id);
            //dd($user->avatar);
            if($user && $user->users_avatar_table->count())
            {
                $name = data_get($user, 'users_avatar_table.0.filename');
                //
                $currentImage = $patentFolder.$name;
                //
                $list = \File::directories($patentFolder);
                $newFolder = $patentFolder.'/'.$size;
                // Создаем папку превю
                if(!in_array($newFolder, $list))
                {
                    \File::makeDirectory($newFolder, $mode = 0777, true, true);
                }
                //
                $file = $patentFolder.'/'.$size.'/'.$name;
                //
                if(\File::exists($file) == false)
                {
                    $img = \Image::make($currentImage)->fit($sizeArr[0], $sizeArr[1]);
                    $img->save($file, 80);
                }
                //
                $cache_image = \Image::cache(function($image) use($file){
                    $im = $image->make($file);
                    return $im;
                }, 1440); // cache for 10 minutes
                //
                return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
            }
            else
            {
                if($user)
                {
                    $name = str_random(10).'.jpg';

                    if($user->social->count())
                    {
                        $socFoto = $user->social->first();
                        $img = \Image::make($socFoto->photo_big);
                        $img->save(public_path('uploads/avatar/'.$name));
                    }
                    else
                    {
                        $ava = \Avatar::create($user->name)->save(public_path('/uploads/avatar/'.$name), 100);
                    }

                    $fid = \Filem::create([
                        'user_id'   => $user->id,
                        'original'  => $name,
                        'filename'  => $name,
                        'uri'       => 'avatar',
                        'ext'       => 'jpg',
                        'filemime'  => 'image/jpeg',
                        'filesize'  => 1,
                        'status'    => 1,
                    ]);

                    $user->users_avatar_table()->attach($user->id,['fid' => $fid->id]);

                    //$name = data_get($user, 'avatar.0.filename');
                    //
                    $currentImage = $patentFolder.$name;
                    //
                    $list = \File::directories($patentFolder);
                    $newFolder = $patentFolder.'/'.$size;
                    // Создаем папку превю
                    if(!in_array($newFolder, $list))
                    {
                        \File::makeDirectory($newFolder, $mode = 0777, true, true);
                    }
                    //
                    $file = $patentFolder.'/'.$size.'/'.$name;
                    //
                    if(\File::exists($file) == false)
                    {
                        $img = \Image::make($currentImage)->fit($sizeArr[0], $sizeArr[1]);
                        $img->save($file, 80);
                    }
                    //
                    $cache_image = \Image::cache(function($image) use($file){
                        $im = $image->make($file);
                        return $im;
                    }, 1440); // cache for 10 minutes
                    //
                    return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
                }
                else
                {
                    $cache_image = \Image::cache(function($image) use($defaultImage){
                        $im = $image->make($defaultImage);
                        return $im;
                    }, 1440); // cache for 10 minutes
                    return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
                }

            }
        }
        else
        {
            $cache_image = \Image::cache(function($image) use($defaultImage){
                $im = $image->make($defaultImage);
                return $im;
            }, 1440); // cache for 10 minutes
            return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
        }
    }
    /* public function destroy
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function destroy($id)
    {
        $this->filem->destroy($id);
        //return $filem;
    }
    /* public function fake_mage
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function fake_image($size, $num)
    {
        $cache_image = \Image::cache(function($image) use($size, $num){
            $size = explode('x', $size);
            //return $image->make('http://css.progim.net/img/any/'.$num.'.jpg')->fit($size[0], $size[1]);
            $st = \Storage::disk('ftp')->get('images/any/'.$num.'.jpg');
            return $image->make($st)->fit($size[0], $size[1]);

        }, 1440); // cache for 10 minutes
        return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
    }

    /* public function fake_video
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function fake_video($num)
    {
        $fileContents = \Storage::disk('ftp')->get('video/any/'.$num.'.mp4');
        return \Response::make($fileContents, 200, ['Content-Type' => 'video/mp4']);
    }

    /* public function fake_video_thumbnail
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function fake_video_thumbnail($num)
    {
        $client = new \Spatie\Dropbox\Client(config('app.dropbox_token'));
        $thumbnail = $client->getThumbnail('video/any/'.$num.'.mp4', 'jpeg', 'w640h480');
        return \Response::make($thumbnail, 200, ['Content-Type' => 'image']);
    }

    /* public function fake_audio
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function fake_audio($num)
    {
        $fileContents = \Storage::disk('ftp')->get('audio/any/'.$num.'.mp3');
        return \Response::make($fileContents, 200, ['Content-Type' => 'audio/mpeg']);
    }

    /* public function get_bg
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_bg($user_id)
    {
        $user = \User::find($user_id);
        $name = data_get($user, 'users_bg_table.0.filename');

        if($name)
        {
            $cache_image = \Image::cache(function($image) use ($name){
                return $image->make(public_path('uploads/bg/'.$name));
            }, 1440); // cache for 10 minutes
        }
        else
        {
            $cache_image = \Image::cache(function($image){
                return $image->make(public_path('uploads/bg/fon2.jpg'));
            }, 1440); // cache for 10 minutes
        }

        return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
    }

    /* public function get_bg_blur
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_bg_blur($user_id)
    {
        $user = \User::find($user_id);
        $name = data_get($user, 'bg.0.filename');

        if($name)
        {
            $cache_image = \Image::cache(function($image) use ($name){
                return $image->make(public_path('uploads/bg/'.$name))->blur(100)->greyscale();
            }, 1440); // cache for 10 minutes
        }
        else
        {
            $cache_image = \Image::cache(function($image){
                return $image->make(public_path('uploads/bg/fon2.jpg'))->blur(100)->greyscale();
            }, 1440); // cache for 10 minutes
        }

        return \Response::make($cache_image, 200, ['Content-Type' => 'image']);
    }

    /* public function video_thumbs
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function video_thumbs($size, $filename)
    {
        $directories = \Storage::disk('ftp')->allDirectories('video');
        $directories = collect($directories);
        $directory = 'video/thumbs/'.$size;

        if($directories->contains($directory))
        {
            $directoryTrue = true;
        }
        else{
            \Storage::disk('ftp')->makeDirectory('video/thumbs/'.$size);
            $directoryTrue = true;
        }

        $fn = explode('.', $filename);
        $fname = $fn[0].'.jpg';

        if($directoryTrue)
        {
            $fileContents = \Storage::disk('ftp')->has('video/thumbs/'.$size.'/'.$fname);
            // якщо є файл на диску
            if($fileContents)
            {

            }
            // якщо немає то створюєм
            else
            {
                // перевіряємо чи існує файл
                $fileVideo = \Storage::disk('ftp')->has('video/'.$filename);
                if($fileVideo)
                {
                    $resource = \Storage::disk('ftp')->get('video/'.$filename);
                    \Storage::put('tmp/'.$filename, $resource);

                    $thumbnail = \Thumbnail::getThumbnail(
                        storage_path().'/app/tmp/'.$filename,
                        storage_path().'/app/tmp/',
                        $fname,
                        2
                    );

                    $resource = \Storage::get('tmp/'.$fname);
                    \Storage::disk('ftp')->put('video/thumbs/'.$fname, $resource);
                    \Storage::delete('tmp/'.$filename);
                    \Storage::delete('tmp/'.$fn[0].'.jpg');

                    dd($thumbnail);
                }
                else
                {
                    // картинка по замовчувані
                }
            }
        }


    }
}
