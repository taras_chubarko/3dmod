<?php

Route::group(['middleware' => 'web', 'prefix' => 'filem', 'namespace' => 'Modules\Filem\Http\Controllers'], function()
{
    Route::post('upload/image', 'FilemController@upload_image');
    Route::post('upload/images', 'FilemController@upload_images');
    Route::post('upload/analize', 'FilemController@upload_analize');

    Route::get('image/{uri}/{size}/{name}', [
        'as' 	=> 'filem.image',
        'uses' 	=> 'FilemController@get_image'
    ]);

    Route::post('destroy/{id}', [
        'as' 	=> 'filem.destroy.id',
        'uses' 	=> 'FilemController@destroy'
    ]);

    Route::get('{uri}/{size}/{name}', [
        'as' 	=> 'filem.filem',
        'uses' 	=> 'FilemController@get_filem'
    ]);

    Route::get('{folder}/{subfolder}/{size}/{name}', [
        'as' 	=> 'filem.subfolder',
        'uses' 	=> 'FilemController@get_filem_subfolder'
    ]);

});


Route::get('avatar/{size?}/{user_id?}', [
    'as' 	=> 'filem.avatar',
    'uses' 	=> 'Modules\Filem\Http\Controllers\FilemController@get_avatar'
]);

Route::get('profile/bg/{user_id}', [
    'as' 	=> 'filem.bg',
    'uses' 	=> 'Modules\Filem\Http\Controllers\FilemController@get_bg'
]);

Route::get('profile/bg/blur/{user_id}', [
    'as' 	=> 'filem.bg.blur',
    'uses' 	=> 'Modules\Filem\Http\Controllers\FilemController@get_bg_blur'
]);

Route::get('fake/image/{size?}/{num?}', [
    'as' 	=> 'fake.image',
    'uses' 	=> 'Modules\Filem\Http\Controllers\FilemController@fake_image'
]);

Route::get('fake/video/{num?}', [
    'as' 	=> 'fake.video',
    'uses' 	=> 'Modules\Filem\Http\Controllers\FilemController@fake_video'
]);

Route::get('fake/thumbnail/video/{num?}', [
    'as' 	=> 'fake.video.thumbnail',
    'uses' 	=> 'Modules\Filem\Http\Controllers\FilemController@fake_video_thumbnail'
]);

Route::get('fake/audio/{num?}', [
    'as' 	=> 'fake.audio',
    'uses' 	=> 'Modules\Filem\Http\Controllers\FilemController@fake_audio'
]);


Route::get('video/thumbs/{size}/{filename}', [
    'as' 	=> 'video.thumbs',
    'uses' 	=> 'Modules\Filem\Http\Controllers\FilemController@video_thumbs'
]);


/*
 * Админка
 */
//Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Filem\Http\Controllers'], function()
//{
//	Route::resource('filem', 'FilemController');
//	Route::post('upload/images', 'FilemController@uploadImages');
//	Route::post('delete/images', 'FilemController@deleteImages');
//	Route::post('upload/filem', 'FilemController@uploadFilem');
//});