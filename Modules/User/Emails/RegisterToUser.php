<?php

namespace Modules\User\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterToUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request, $user)
    {
        $this->request = $request;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $request = $this->request;
        $user = $this->user;
        
        $subject['ru'] = 'Спасибо за регистрацию!';
        $subject['en'] = 'Thank you for registering!';
        $subject['uk'] = 'Дякую за реєстрацію!';
        
        
        return $this->view('site::mail.mail-register-to-user-'.$request->lang, compact('request', 'user'))
        ->to($user->email)
        ->subject($subject[$request->lang]);
    }
}
