<?php

namespace Modules\User\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LoginMDW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (\Sentinel::guest())
        {
            return redirect()->guest('/');
        }
        else
        {
            return $next($request);
        }
    }
}
