<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Entities\User as UserModel;
use Modules\User\Repositories\UserRepository;
use Faker\Factory as Faker;
use Modules\Notifi\Entities\Notifi;


class FriendController extends Controller
{
    protected $model;
    protected $friendReq;
    protected $myFriends;
    protected $repository;
    protected $notifi;
    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(UserModel $model, UserRepository $repository, Notifi $notifi)
    {
        $this->model = $model;
        $this->repository = $repository;
        $this->notifi = $notifi;
    }

    /* public function add_request
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function add_request($friend_id)
    {
        $user = auth()->user();
        $recipient = $this->model->find($friend_id);
        $add = $user->befriend($recipient);
        $this->counts($user);
        $this->counts($recipient);

        \Notifi::add([
            'from_user_id'  => $user->id,
            'to_user_id'    => $recipient->id,
            'type'          => 'friend_request',
            'request_id'    => $add->id,
            'message'       => 'notifi_1', //notifi_1 - Користувач хоче добавити вас у друзі.
        ]);
        return response()->json('ok', 200);
    }

    /* public function refuse
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function refuse($user_id)
    {
        $user = auth()->user();
        $sender = $this->model->find($user_id);
        $user->unfriend($sender);
        $this->counts($user);
        $this->counts($sender);

        \Notifi::add([
            'from_user_id'  => $user->id,
            'to_user_id'    => $sender->id,
            'type'          => 'friend_request',
            'request_id'    => null,
            'message'       => 'notifi_2', //Користувач відмовив вам у дружбі
        ]);
        return response()->json('ok', 200);
    }

    /* public function remove
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function remove($user_id)
    {
        $user = auth()->user();
        $sender = $this->model->find($user_id);
        $refuse = $user->unfriend($sender);
        $this->counts($user);
        $this->counts($sender);

        \Notifi::add([
            'from_user_id'  => $user->id,
            'to_user_id'    => $sender->id,
            'type'          => 'friend_request',
            'request_id'    => null,
            'message'       => 'notifi_6', //Користувач видалився з ваших друзів
        ]);
        //
        return response()->json('ok', 200);
    }

    /* public function removereq
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function removereq($user_id)
    {
        $user = auth()->user();
        $sender = $this->model->find($user_id);
        $user->unfriend($sender);
        $this->counts($user);
        $this->counts($sender);

        \Notifi::add([
            'from_user_id'  => $user->id,
            'to_user_id'    => $sender->id,
            'type'          => 'friend_request',
            'request_id'    => null,
            'message'       => 'notifi_7', //Користувач відмінив запит дружби
        ]);
        return response()->json('ok', 200);
    }

    /* public function accept
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function accept($user_id)
    {

        $user = auth()->user();
        $sender = $this->model->find($user_id);
        $user->acceptFriendRequest($sender);
        $this->counts($user);
        $this->counts($sender);

        \Notifi::add([
            'from_user_id'  => $user->id,
            'to_user_id'    => $sender->id,
            'type'          => 'friend_request',
            'request_id'    => null,
            'message'       => 'notifi_4', //Користувач добавив вас у друзі
        ]);
        // Створення чату
        \Message::findOrNew($sender);
        //
        return response()->json('ok', 200);
    }

    /* public function counts
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function counts($user = null)
    {
        $user = ($user) ? $user : auth()->user();
        $countAll = $user->getFriendsCount();
        $countOnline = 0;
        $countReq = $user->getPendingBothFriendships()->count();

        event(new \Modules\User\Events\CountAllFriendEvent($countAll, $user->id));
        event(new \Modules\User\Events\CountFriendOnlineEvent($countOnline, $user->id));
        event(new \Modules\User\Events\CountFriendReqEvent($countReq, $user->id));

    }
    /* public function rekomendovani_druzi
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function rekomendovani_druzi(Request $request)
    {
        $user = auth()->user();
        $req = $user->getFriendRequests()->pluck('sender_id');
        $notInID = $user->getFriends()->pluck('id')->merge($req);
        $notInID = $notInID->push($user->id);

        $users = $this->model->inRandomOrder()->whereNotIn('id', $notInID)->take(5)->get();
        $users->transform(function ($value) use ($user) {
            $data = [
                'id'        => $value->id,
                'fullname'  => $value->full_name,
                'address'   => $value->address,
                'vid_diyalnosti' => $value->po->locale,
                'slug'      => $value->slug,
                'is_friend' => $value->hasFriendRequestFrom($user) ? 'requestFromMe' : false,
            ];
            return $data;
        });
        return response()->json($users, 200);
    }
    /* public function myfriends
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function myfriends(Request $request)
    {
        $user = auth()->user();
        //broadcast(new \Modules\User\Events\CountAllFriendEvent(2, $user->id));

        $this->repository->pushCriteria(\Modules\User\Criteria\MyFriendCriteria::class);
        $users = $this->repository->paginate();

        $users->getCollection()->transform(function ($value) use ($user) {
            $faker = Faker::create('uk_UA');

            $data = [
                'id'        => $value->id,
                'fullname'  => $value->full_name,
                'address'   => $value->address,
                'vid_diyalnosti' => $value->po->locale,
                'slug'      => $value->slug,
                'views'     => $faker->numberBetween(0, 1000),
                'likes'     => $faker->numberBetween(0, 1000),
                'follows'   => $faker->numberBetween(0, 1000),
                'last_works' => $value->last_works,
                'is_friend' => $value->isFriendWith($user),
                'show'      => true,
            ];
            return $data;
        });



        return response()->json($users, 200);
    }


    /* public function myfriends_request_data
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function myfriends_request_data()
    {
        $user = auth()->user();

        $users = $user->friendsReq()->with('sender', 'recipient')->paginate();

        $users->getCollection()->transform(function ($value) use ($user) {
            $faker = Faker::create('uk_UA');
            if($value->sender_id == $user->id)
            {
                $value = $value->recipient;
            }
            else
            {
                $value = $value->sender;
            }

            $data = [
                'id'        => $value->id,
                'fullname'  => $value->full_name,
                'address'   => $value->address,
                'vid_diyalnosti' => $value->po->locale,
                'slug'      => $value->slug,
                'views'     => $faker->numberBetween(0, 1000),
                'likes'     => $faker->numberBetween(0, 1000),
                'follows'   => $faker->numberBetween(0, 1000),
                'last_works'=> $value->last_works,
                'show'      => true,
                'is_my_req' => $value->hasFriendRequestFrom($user),
            ];
            return $data;
        });
        return $users;
    }
    /* public function myfriends_request
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function myfriends_request()
    {
        $users = $this->myfriends_request_data();
        return response()->json($users, 200);
    }

    /* public function friends_add
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function friends_add()
    {
        $user = auth()->user();
        $this->repository->pushCriteria(\Modules\User\Criteria\AddFriendCriteria::class);
        $users = $this->repository->paginate();

        $users->getCollection()->transform(function ($value) use ($user) {
            $faker = Faker::create('uk_UA');
            $data = [
                'id'        => $value->id,
                'fullname'  => $value->full_name,
                'address'   => $value->address,
                'vid_diyalnosti' => $value->po->locale,
                'slug'      => $value->slug,
                'views'     => $faker->numberBetween(0, 1000),
                'likes'     => $faker->numberBetween(0, 1000),
                'follows'   => $faker->numberBetween(0, 1000),
                'last_works' => $value->last_works,
                'is_friend' => $value->hasFriendRequestFrom($user) ? 'requestFromMe' : false,
            ];
            return $data;
        });
        return response()->json($users, 200);
    }
}
