<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Notifi\Repositories\NotifiRepository;

class NotifiController extends Controller
{
    protected $repository;

    /* public function __construct
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function __construct(NotifiRepository $repository)
    {
        $this->repository = $repository;
    }

    /* public function all_notifications
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function all_notifications(Request $request)
    {
        $user = auth()->user();
        $notifi = $this->repository->getMyNotifi($user->id, 20);
        return response()->json($notifi, 200);
    }
}
