<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Modules\User\Repositories\UserRepository;
use Faker\Factory as Faker;
use Modules\User\Entities\UserPaymentMethods;
use Modules\User\Entities\User;
use Modules\User\Entities\Role;
use Modules\User\Entities\UserSocial;


class UserController extends Controller
{
    /*
     *
     */
    protected $repository;
    protected $model;
    protected $userSocial;
    /*
     *
     */
    public function __construct(UserRepository $repository, User $model, UserSocial $userSocial)
    {
        $this->repository = $repository;
        $this->model = $model;
        $this->userSocial = $userSocial;
    }

    /* public function login
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function login(Request $request)
    {
        $user = $this->repository->findByField('email', $request->email)->first();
        if(!$user)
        {
            return response()->json(['error' => 'msg.0'], 442);//Користувач з такою поштою не зареєстрований
        }
        else
        {
            if (\Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
                $data['token'] = $request->session()->get('_token');
                $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
                return response()->json($data, 200);
            }
            else
            {
                return response()->json(['error' => 'msg.1'], 442); //Не правильний логін або пароль
            }
        }
    }

    /* public function logout
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function logout()
    {
        auth()->logout();
        \Session::flush();
        return response()->json('ok', 200);
    }

    /* public function social
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function social(Request $request)
    {
        $user = $this->repository->socialLogin($request);
        if($user)
        {
            $data['token'] = $request->session()->get('_token');
            $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
            return response()->json($data, 200);
        }
        else
        {
            return response()->json(['error' => 'msg.2'], 442); //Ошибка авторизации
        }
    }

    /* public function register
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function register(Request $request)
    {
        $user = $this->repository->findByField('email', $request->email)->first();
        if(!$user)
        {
            $user = $this->model->create([
                'name'      => $request->email,
                'email'     => $request->email,
                'password'  => bcrypt($request->password),
            ]);
            //
            $role = Role::findOrFail(3); //Покупець
            $user->attachRole($role);
            \Auth::loginUsingId($user->id);
            //
            $data['token'] = $request->session()->get('_token');
            $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
            return response()->json($data, 200);
        }
        else
        {
            return response()->json(['error' => 'msg.3'], 442); //Користувач з такою поштою уже був зареєстрований
        }
    }

    /* public function forgot
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function forgot(Request $request)
    {
        $user = $this->repository->findByField('email', $request->email)->first();
        if(!$user)
        {
            return response()->json(['error' => 'msg.0'], 442);//Користувач з такою поштою не зареєстрований
        }
        else
        {
            $pass = str_random(6);
            $user->password = bcrypt($pass);
            $user->save();
            return response()->json('ok', 200);
        }
    }

    /* public function profile_settings
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function profile_settings($id)
    {
        $user = $this->model->find($id);
        //
        $item = [
            'id'        => $user->id,
            'login'     => $user->login,
            'email'     => $user->email,
            'name'      => $user->name,
            'sname'     => $user->sname,
            'slug'      => $user->slug,
            'role'      => $user->role->arr,
            'vid_diyalnosti' => $user->vid_diyalnosti->arr,
            'name_shop' => $user->name_shop->value,
            'dosvid'    => $user->dosvid->value,
            'gender'    => $user->gender->value,
            'birthday'  => $user->birthday->data,
            'address'   => $user->address->value,
            'web'       => $user->socialLinks->values,
            'po'        => $user->po->arr,
        ];
        //
        return response()->json($item, 200);
    }

    /* public function profile_settings_save
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function profile_settings_save(Request $request, $id)
    {
        $user = $this->model->find($id);
        $user->login = $request->login;
        $user->name = $request->name;
        $user->sname = $request->sname;
        $user->save();

        $data = $request->all();

        foreach ($data as $key => $datum) {
            if($request->has($key))
            {
                $user->$key = $datum;
            }
        }
        $item = $this->repository->block($user->slug);

        return response()->json($item, 200);
    }

    /* public function profile_social_save
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function profile_social_save(Request $request, $id)
    {
        $soc = $this->userSocial->where('user_id', $id)->where('network', $request->network)->first();
        //
        if(!$soc)
        {
            $soc = $this->userSocial->create([
                'user_id'           => $id,
                'manual'            => $request->manual,
                'email'             => $request->email,
                'phone'             => $request->phone,
                'city'              => $request->city,
                'network'           => $request->network,
                'identity'          => $request->identity,
                'photo_big'         => $request->photo_big,
                'profile'           => $request->profile,
                'last_name'         => $request->last_name,
                'uid'               => $request->uid,
                'photo'             => $request->photo,
                'verified_email'    => $request->verified_email,
                'first_name'        => $request->first_name,
            ]);
        }
        //
        $data = [
            'id'        => $soc->id,
            'network'   => $soc->network,
            'profile'   => $soc->profile,
        ];
        //
        return response()->json($data, 200);
    }

    /* public function profile_social_delete
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function profile_social_delete($id, $network)
    {
        $this->userSocial->where('user_id', $id)->where('network', $network)->delete();
        return response()->json('success', 200);
    }

    /* public function upload_avatar
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function upload_avatar(Request $request)
    {
        $user = auth()->user();
        $fid = $user->avatar->fid;
        if($fid)
        {
            \Filem::destroy($fid);
        }
        $file = \Filem::make($request->image, 'avatar', $user->id, $status = 1);
        $user->avatar = $file->id;
        $result['user'] = $user;
        return response()->json($result, 200);
    }

    /* public function upload_bg
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function upload_bg(Request $request)
    {
        $user = auth()->user();
        $fid = $user->bg->fid;
        if($fid)
        {
            \Filem::destroy($fid);
        }
        $file = \Filem::make($request->image, 'bg', $user->id, $status = 1);
        $user->bg = $file->id;
        $result['user'] = $user;
        return response()->json($result, 200);
    }

    /* public function remove_bg
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function remove_bg($id)
    {
        $user = $this->model->find($id);
        $fid = $user->bg->fid;
        if($fid)
        {
            \Filem::destroy($fid);
        }
        $result['user'] = $user;
        return response()->json($result, 200);
    }

    /* public function refresh_bg
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function refresh_bg($id)
    {
        $faker = Faker::create('uk_UA');
        $user = $this->model->find($id);
        $fid = $user->bg->fid;
        if($fid)
        {
            \Filem::destroy($fid);
        }
        $name = str_random(10).'.jpg';
        $img = \Image::make('http://css.progim.net/img/any/'.$faker->numberBetween(1, 151).'.jpg');
        $img->save(public_path('uploads/bg/'.$name));
        //
        $fid = \Filem::create([
            'user_id'   => $user->id,
            'original'  => $name,
            'filename'  => $name,
            'uri'       => 'avatar',
            'ext'       => 'jpg',
            'filemime'  => 'image/jpeg',
            'filesize'  => 1,
            'status'    => 1,
        ]);
        //
        $user->bg = $fid->id;
        //
        $result['user'] = $user;
        return response()->json($result, 200);
    }
    /* public function save_web
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function save_web(Request $request, $id)
    {
        $user = $this->model->find($id);
        $user->web = $request->soc;
        return response()->json('Saved!', 200);
    }
    /* public function delete_web
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function delete_web(Request $request, $id)
    {
        \DB::table('users_web')->where('user_id', $id)->where('soc', $request->soc)->delete();
        return response()->json('Removed!', 200);
    }


    /* public function profile
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function profile(Request $request)
    {
        $user = auth()->user();

        $faker = Faker::create('uk_UA');

        $notify = \Notifi::getMyNotifi($user->id, 3);
        $blocks = [];

        \Carbon::setLocale('uk');
//        foreach(range(1, 3) as $i)
//        {
//            $name = $faker->lastName.' '.$faker->firstName;
//            $notifyType = $faker->randomElement(['work', 'gallery', 'info']);
//            $notify[] = [
//                'user' => [
//                    'link' 	=> '/profile',
//                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
//                    'name' 	=> $name,
//                ],
//                'text' => $faker->text(200),
//                'type' => [
//                    'name' 	=> $notifyType,
//                    'src' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
//                    'link' 	=> '/profile',
//                ],
//                'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->diffForHumans(),
//            ];
//        }

        //
        foreach(range(1, 14) as $i)
        {
            $name = $faker->lastName.' '.$faker->firstName;
            $tpl['product'] = [
                'tpl' => 'product',
                'img' => route('fake.image', ['217x160', $faker->numberBetween(1, 151)]),
                'division' => $faker->randomElement(['midi', 'maxi', 'mini', 'ultra', 'free']),
                'name' => $faker->sentence(3),
                'category' => $faker->word,
                'downloads' => $faker->numberBetween(0, 500),
                'comments' => $faker->numberBetween(0, 500),
                'views' => $faker->numberBetween(0, 500),
                'likes' => $faker->numberBetween(0, 500),
                'price' => $faker->numberBetween(0, 500),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
            ];

            $tpl['gallery'] = [
                'tpl' => 'gallery',
                'img' => route('fake.image', ['217x160', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'category' => $faker->word,
                'scale' => '1:'.$faker->numberBetween(1, 100),
                'downloads' => $faker->numberBetween(0, 500),
                'comments' => $faker->numberBetween(0, 500),
                'views' => $faker->numberBetween(0, 500),
                'likes' => $faker->numberBetween(0, 500),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
            ];

            $tpl['tema'] = [
                'tpl' => 'tema',
                'img' => route('fake.image', ['260x160', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'text' => $faker->text($maxNbChars = 200),
                'category' => $faker->words($nb = 3, $asText = false),
                'comments' => $faker->numberBetween(0, 500),
                'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->format('d.m.Y'),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
            ];

            $tpl['work'] = [
                'tpl' => 'work',
                'img' => route('fake.image', ['260x160', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'text' => $faker->text($maxNbChars = 100),
                'category' => $faker->words($nb = 3, $asText = false),
                'price' => $faker->numberBetween(0, 500),
                'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->format('d.m.Y'),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'deadline' => $faker->numberBetween(0, 500),
                'bids' => $faker->numberBetween(0, 500),
                'tema' => $faker->randomElement(['3d модели', 'web', 'audio', 'video', 'Полиграфия']),
                'ico' => $faker->randomElement(['topcat-uniF109', 'topcat-uniF110', 'topcat-uniF10F', 'topcat-uniF101', 'topcat-uniF10E']),
            ];


            $blocks[] = $tpl[$faker->randomElement(['product', 'gallery', 'tema', 'work'])];
        }


        $response['notify'] = $notify;
        $response['blocks'] = $blocks;

        return response()->json($response, 200);
    }
    /* public function activity_friends
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function activity_friends(Request $request)
    {
        $faker = Faker::create('uk_UA');

        $blocks = [];

        \Carbon::setLocale('uk');
        //
        foreach(range(1, 20) as $i)
        {
            $name = $faker->lastName.' '.$faker->firstName;

            $notifyType = $faker->randomElement(['work', 'gallery', 'info']);

            $created = \Carbon::parse($faker->date($format = 'd.m.Y', $max = 'now'));

            $tpl['notify'] = [
                'tpl' => 'notify',
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'text' => $faker->text(200),
                'type' => [
                    'name' 	=> $notifyType,
                    'src' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'link' 	=> '/profile',
                ],
                'created' => $created->diffForHumans(),
                'date' => $created->format('d.m.Y'),
            ];

            $tpl['product'] = [
                'tpl' => 'product',
                'img' => route('fake.image', ['217x160', $faker->numberBetween(1, 151)]),
                'division' => $faker->randomElement(['midi', 'maxi', 'mini', 'ultra', 'free']),
                'name' => $faker->sentence(3),
                'category' => $faker->word,
                'downloads' => $faker->numberBetween(0, 500),
                'comments' => $faker->numberBetween(0, 500),
                'views' => $faker->numberBetween(0, 500),
                'likes' => $faker->numberBetween(0, 500),
                'price' => $faker->numberBetween(0, 500),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'date' => \Carbon::parse($faker->date($format = 'd.m.Y', $max = 'now'))->format('d.m.Y'),
            ];

            $tpl['gallery'] = [
                'tpl' => 'gallery',
                'img' => route('fake.image', ['217x160', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'category' => $faker->word,
                'scale' => '1:'.$faker->numberBetween(1, 100),
                'downloads' => $faker->numberBetween(0, 500),
                'comments' => $faker->numberBetween(0, 500),
                'views' => $faker->numberBetween(0, 500),
                'likes' => $faker->numberBetween(0, 500),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'date' => \Carbon::parse($faker->date($format = 'd.m.Y', $max = 'now'))->format('d.m.Y'),
            ];

            $tpl['tema'] = [
                'tpl' => 'tema',
                'img' => route('fake.image', ['260x160', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'text' => $faker->text($maxNbChars = 200),
                'category' => $faker->words($nb = 3, $asText = false),
                'comments' => $faker->numberBetween(0, 500),
                'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->format('d.m.Y'),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'date' => \Carbon::parse($faker->date($format = 'd.m.Y', $max = 'now'))->format('d.m.Y'),
            ];

            $tpl['work'] = [
                'tpl' => 'work',
                'img' => route('fake.image', ['260x160', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'text' => $faker->text($maxNbChars = 100),
                'category' => $faker->words($nb = 3, $asText = false),
                'price' => $faker->numberBetween(0, 500),
                'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->format('d.m.Y'),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'deadline' => $faker->numberBetween(0, 500),
                'bids' => $faker->numberBetween(0, 500),
                'tema' => $faker->randomElement(['3d модели', 'web', 'audio', 'video', 'Полиграфия']),
                'ico' => $faker->randomElement(['topcat-uniF109', 'topcat-uniF110', 'topcat-uniF10F', 'topcat-uniF101', 'topcat-uniF10E']),
                'date' => \Carbon::parse($faker->date($format = 'd.m.Y', $max = 'now'))->format('d.m.Y'),
            ];


            $blocks[] = $tpl[$faker->randomElement(['product', 'gallery', 'tema', 'work', 'notify'])];
        }

        $response['blocks'] = $blocks;

        return response()->json($response, 200);
    }
    /* public function activity_groups
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function activity_groups(Request $request)
    {
        $faker = Faker::create('uk_UA');
        foreach(range(1, 20) as $i)
        {
            $name = $faker->lastName.' '.$faker->firstName;

            $tpl[] = [
                'tpl' => 'tema',
                'img' => route('fake.image', ['260x160', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'text' => $faker->text($maxNbChars = 200),
                'category' => $faker->words($nb = 3, $asText = false),
                'comments' => $faker->numberBetween(0, 500),
                'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->format('d.m.Y'),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'date' => \Carbon::parse($faker->date($format = 'd.m.Y', $max = 'now'))->format('d.m.Y'),
            ];
        }
        return response()->json($tpl, 200);
    }
    /* public function activity_goods
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function activity_goods(Request $request)
    {
        $faker = Faker::create('uk_UA');
        foreach(range(1, 20) as $i)
        {
            $name = $faker->lastName.' '.$faker->firstName;

            $tpl[] = [
                'tpl' => 'product',
                'img' => route('fake.image', ['217x160', $faker->numberBetween(1, 151)]),
                'division' => $faker->randomElement(['midi', 'maxi', 'mini', 'ultra', 'free']),
                'name' => $faker->sentence(3),
                'category' => $faker->word,
                'downloads' => $faker->numberBetween(0, 500),
                'comments' => $faker->numberBetween(0, 500),
                'views' => $faker->numberBetween(0, 500),
                'likes' => $faker->numberBetween(0, 500),
                'price' => $faker->numberBetween(0, 500),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'date' => \Carbon::parse($faker->date($format = 'd.m.Y', $max = 'now'))->format('d.m.Y'),
            ];
        }
        return response()->json($tpl, 200);
    }
    /* public function activity_gallery
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function activity_gallery(Request $request)
    {
        $faker = Faker::create('uk_UA');
        foreach(range(1, 20) as $i)
        {
            $name = $faker->lastName.' '.$faker->firstName;

            $tpl[] = [
                'tpl' => 'gallery',
                'img' => route('fake.image', ['217x160', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'category' => $faker->word,
                'scale' => '1:'.$faker->numberBetween(1, 100),
                'downloads' => $faker->numberBetween(0, 500),
                'comments' => $faker->numberBetween(0, 500),
                'views' => $faker->numberBetween(0, 500),
                'likes' => $faker->numberBetween(0, 500),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'date' => \Carbon::parse($faker->date($format = 'd.m.Y', $max = 'now'))->format('d.m.Y'),
            ];
        }
        return response()->json($tpl, 200);
    }
    /* public function activity_work
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function activity_work(Request $request)
    {
        $faker = Faker::create('uk_UA');
        foreach(range(1, 20) as $i)
        {
            $name = $faker->lastName.' '.$faker->firstName;

            $tpl[] = [
                'tpl' => 'work',
                'img' => route('fake.image', ['260x160', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'text' => $faker->text($maxNbChars = 100),
                'category' => $faker->words($nb = 3, $asText = false),
                'price' => $faker->numberBetween(0, 500),
                'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->format('d.m.Y'),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'deadline' => $faker->numberBetween(0, 500),
                'bids' => $faker->numberBetween(0, 500),
                'tema' => $faker->randomElement(['3d модели', 'web', 'audio', 'video', 'Полиграфия']),
                'ico' => $faker->randomElement(['topcat-uniF109', 'topcat-uniF110', 'topcat-uniF10F', 'topcat-uniF101', 'topcat-uniF10E']),
                'date' => \Carbon::parse($faker->date($format = 'd.m.Y', $max = 'now'))->format('d.m.Y'),
            ];
        }
        return response()->json($tpl, 200);
    }
    /* public function activity_lessons
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function activity_lessons(Request $request)
    {
        $faker = Faker::create('uk_UA');
        foreach(range(1, 20) as $i)
        {
            $name = $faker->lastName.' '.$faker->firstName;

            $tpl[] = [
                'tpl' => 'tema',
                'img' => route('fake.image', ['260x160', $faker->numberBetween(1, 151)]),
                'name' => $faker->sentence(3),
                'text' => $faker->text($maxNbChars = 200),
                'category' => $faker->words($nb = 3, $asText = false),
                'comments' => $faker->numberBetween(0, 500),
                'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->format('d.m.Y'),
                'user' => [
                    'link' 	=> '/profile',
                    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                    'name' 	=> $name,
                ],
                'date' => \Carbon::parse($faker->date($format = 'd.m.Y', $max = 'now'))->format('d.m.Y'),
            ];
        }
        return response()->json($tpl, 200);
    }

    /* public function block_profile
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function block_profile($slug)
    {
        $item = $this->repository->block($slug);
        return response()->json($item, 200);
    }
    /* public function get_profile_notification
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_profile_notification()
    {
        $response['items'] = \TaxonomyTerm::getTreeLocale(3);
        $response['notification'] = auth()->user()->notification->arr;

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($response, 200, $headers, JSON_UNESCAPED_UNICODE);
    }
    /* public function save_profile_notification
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function save_profile_notification(Request $request)
    {
        $user = auth()->user();
        $user->notification = $request->notification;

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json('OK', 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function get_profile_access
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function get_profile_access()
    {
        $user = auth()->user();
        $item = [
            'id' => $user->id,
            'email' => $user->email,
        ];

        $headers = [ 'Content-Type' => 'application/json; charset=utf-8' ];
        return response()->json($item, 200, $headers, JSON_UNESCAPED_UNICODE);
    }

    /* public function save_profile_access
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function save_profile_access(Request $request)
    {
        $user = auth()->user();

        if($request->has('password_repeat') && $request->password_repeat)
        {
            $user->password = bcrypt($request->password_repeat);
            $user->save();
        }
        //
        if($request->has('email') && $request->email)
        {
            $userMail = $this->repository->findByField('email', $request->email)->first();
            if($userMail && $userMail->id != $user->id)
            {
                $msg['uk'] = [0 => "Почта вам не належить. Будь-ласка введіть іншу."];
                $msg['ru'] = [0 => 'Почта вам не принадлежит. Пожалуйста введите другую.'];
                $msg['en'] = [0 => 'You do not own the mail. Please enter another one.'];
                return response()->json($msg, 442);
            }
            else
            {
                $user->email = $request->email;
                $user->save();
            }
        }
        return response()->json('OK', 200);
    }

    /* public function sw
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function sw(Request $request, $id)
    {
        auth()->logout();
        \Session::flush();
        \Auth::loginUsingId($id);
        return redirect('/');

//        $data['token'] = $request->session()->get('_token');
//        $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
//        return response()->json($data, 200);
    }

    /* public function profile_slug
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function profile_slug($slug)
    {
        $user = $this->model->where('slug', $slug)->first();
        return response()->json($user, 200);
    }















}
