<?php


Route::get('sw/{id}', [
    'middleware'=> 'web',
    'uses'      => 'Modules\User\Http\Controllers\UserController@sw'
]);

Route::group(['middleware' => 'web', 'prefix' => 'api/v1/user', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
    Route::post('login', 'UserController@login');
    Route::post('logout', 'UserController@logout');
    Route::post('login/social', 'UserController@social');
    Route::post('register', 'UserController@register');
    Route::post('forgot', 'UserController@forgot');
    Route::post('block/profile/{slug}', 'UserController@block_profile');
    Route::post('profile/{slug}', 'UserController@profile_slug');
    //
    Route::get('profile/{id}', 'UserController@profile_settings');
    Route::put('profile/{id}', 'UserController@profile_settings_save');
    Route::put('profile/{id}/social', 'UserController@profile_social_save');
    Route::delete('profile/{id}/social/{network}', 'UserController@profile_social_delete');
    //
    Route::post('profile/upload/avatar', 'UserController@upload_avatar');
    Route::post('profile/upload/bg', 'UserController@upload_bg');
    Route::post('profile/remove/bg/{id}', 'UserController@remove_bg');
    Route::post('profile/refresh/bg/{id}','UserController@refresh_bg');
    //
    Route::post('profile/save/web/{id}', 'UserController@save_web');
    Route::post('profile/delete/web/{id}', 'UserController@delete_web');
    //
    Route::get('profile/settings/notification', 'UserController@get_profile_notification');
    Route::post('profile/settings/notification', 'UserController@save_profile_notification');
    //
    Route::get('profile/settings/access', 'UserController@get_profile_access');
    Route::post('profile/settings/access', 'UserController@save_profile_access');

    Route::post('profile/activity/all-notifications', 'NotifiController@all_notifications');
    //
    Route::get('profile', 'UserController@profile');
});

Route::group(['middleware' => 'auth.apikey', 'prefix' => 'api/v1', 'namespace' => 'Modules\User\Http\Controllers'], function()
{

    Route::post('profile/activity/friends', [
        'as' 		=> 'api.v1.profile.activity.friends',
        'uses' 		=> 'UserController@activity_friends'
    ]);

    Route::post('profile/activity/groups', [
        'as' 		=> 'api.v1.profile.activity.groups',
        'uses' 		=> 'UserController@activity_groups'
    ]);

    Route::post('profile/activity/goods', [
        'as' 		=> 'api.v1.profile.activity.goods',
        'uses' 		=> 'UserController@activity_goods'
    ]);

    Route::post('profile/activity/gallery', [
        'as' 		=> 'api.v1.profile.activity.gallery',
        'uses' 		=> 'UserController@activity_gallery'
    ]);

    Route::post('profile/activity/work', [
        'as' 		=> 'api.v1.profile.activity.work',
        'uses' 		=> 'UserController@activity_work'
    ]);

    Route::post('profile/activity/lessons', [
        'as' 		=> 'api.v1.profile.activity.lessons',
        'uses' 		=> 'UserController@activity_lessons'
    ]);


    Route::post('profile/settings/access', [
        'uses' 	=> 'UserController@save_profile_access'
    ]);

    Route::get('finances/osoba/{user_id}', [
        'uses' 	=> 'UserController@profile_finances_osoba'
    ]);

    Route::get('finances/shop/{user_id}', [
        'uses' 	=> 'UserController@profile_finances_shop'
    ]);

//    Route::put('profile/{user_id}', [
//        'uses' 		=> 'UserController@profile_settings_save'
//    ]);

    Route::post('profile/upload/passport', [
        'uses' 		=> 'UserController@upload_passport'
    ]);

    Route::post('profile/add/payments', [
        'uses' 		=> 'UserController@add_payments'
    ]);

    Route::post('profile/upload/scan', [
        'uses' 		=> 'UserController@upload_scan'
    ]);


});

Route::group(['middleware' => 'web', 'prefix' => 'api/v1/friends', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
    Route::get('/', 'FriendController@myfriends');
    Route::get('rekomendovani', 'FriendController@rekomendovani_druzi');
    Route::post('request/{friend_id}','FriendController@add_request');
    Route::post('/refuse/{user_id}', 'FriendController@refuse');
    Route::post('/remove/{user_id}', 'FriendController@remove');
    Route::post('/removereq/{user_id}', 'FriendController@removereq');
    Route::post('/accept/{user_id}','FriendController@accept');
    Route::get('/add', 'FriendController@friends_add');
    Route::post('/menu', 'FriendController@counts');

    Route::get('request', [
        'uses' 		=> 'FriendController@myfriends_request'
    ]);

});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'Modules\Admin\Http\Middleware\AdminMDW'], 'prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
    Route::resource('users', 'AdminUserController');

    Route::post('users/{id}/activate', [
        'as' => 'users.activate',
        'uses' => 'AdminUserController@activate'
    ]);

    Route::post('users/{id}/ban', [
        'as' => 'users.ban',
        'uses' => 'AdminUserController@ban'
    ]);

    Route::post('users/{id}/unban', [
        'as' => 'users.unban',
        'uses' => 'AdminUserController@unban'
    ]);

});
