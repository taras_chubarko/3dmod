<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaymentMethodsRable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_payment_methods', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('type')->nullable();
            $table->string('wm_id')->nullable();
            $table->string('wm_wallet')->nullable();
            $table->string('inn')->nullable();
            $table->string('name')->nullable();
            $table->string('sname')->nullable();
            $table->string('fname')->nullable();
            $table->string('company')->nullable();
            $table->string('vat')->nullable();
            $table->string('zip')->nullable();
            $table->string('address')->nullable();
            $table->string('card_num')->nullable();
            $table->string('card_exp')->nullable();
            $table->integer('doc1')->nullable();
            $table->integer('doc2')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_payment_methods');
    }
}
