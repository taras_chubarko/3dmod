<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

//        $admin = new \Modules\User\Entities\Role();
//        $admin->name         = 'admin';
//        $admin->display_name = 'Адмін'; // optional
//        $admin->description  = ''; // optional
//        $admin->save();
//
//        $moderator = new \Modules\User\Entities\Role();
//        $moderator->name         = 'moderator';
//        $moderator->display_name = 'Модератор'; // optional
//        $moderator->description  = ''; // optional
//        $moderator->save();
//
//        $buyer = new \Modules\User\Entities\Role();
//        $buyer->name         = 'buyer';
//        $buyer->display_name = 'Покупець'; // optional
//        $buyer->description  = ''; // optional
//        $buyer->save();
//
//        $seller = new \Modules\User\Entities\Role();
//        $seller->name         = 'seller';
//        $seller->display_name = 'Продавець'; // optional
//        $seller->description  = ''; // optional
//        $seller->save();

        $admin = \Modules\User\Entities\Role::findOrFail(1);
        $user1 = \Modules\User\Entities\User::find(1);
//        $user1 = \Modules\User\Entities\User::create([
//            'name'=> 'admin',
//            'email'     => 'admin@admin.ua',
//            'password'  => bcrypt('123456'),
//        ]);
        $user1->attachRole($admin);

        $moderator = \Modules\User\Entities\Role::findOrFail(2);
        $user2 = \Modules\User\Entities\User::create([
            'name'=> 'moderator',
            'email'     => 'moderator@moderator.ua',
            'password'  => bcrypt('123456'),
        ]);
        $user2->attachRole($moderator);

        $buyer = \Modules\User\Entities\Role::findOrFail(3);
        $user3 = \Modules\User\Entities\User::create([
            'name'=> 'buyer',
            'email'     => 'buyer@buyer.ua',
            'password'  => bcrypt('123456'),
        ]);
        $user3->attachRole($buyer);

        $seller = \Modules\User\Entities\Role::findOrFail(4);
        $user4 = \Modules\User\Entities\User::create([
            'name'=> 'seller',
            'email'     => 'seller@seller.ua',
            'password'  => bcrypt('123456'),
        ]);
        $user4->attachRole($seller);


    }
}
