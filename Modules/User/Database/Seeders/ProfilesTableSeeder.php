<?php

namespace Modules\User\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Faker\Factory as Faker;
use Modules\Taxonomy\Facades\TaxonomyTerm;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('uk_UA');
//
//        foreach(range(1, 100) as $i)
//        {
//            $role = \Modules\User\Entities\Role::findOrFail($faker->numberBetween(3, 4));
//            $user = \Modules\User\Entities\User::create([
//                'name'      => $faker->firstName(),
//                'sname'     => $faker->lastName,
//                'email'     => $faker->email,
//                'password'  => bcrypt('123456'),
//            ]);
//            $user->attachRole($role);
//            //
//            $country = 'Україна';//$faker->country;
//            $city  = $faker->city;
//            $user->address = [
//                'address' => $country.', '.$city,
//                'country' => $country,
//                'city' => $city,
//                'lat' => $faker->latitude,
//                'lng' => $faker->longitude,
//            ];
//            $user->vid_diyalnosti = $faker->randomElements($array = array (103,104,105,106,107,108,109,110), $count = 3);
//
//        }

        $users = \User::all();
        foreach ($users as $user)
        {
            $po = \Modules\Taxonomy\Entities\TaxonomyTerm::where('taxonomy_id', 9)->whereNull('parent_id')->get()->pluck('id')->toArray();
            $po = $faker->randomElements($po, 3);
            $po2 = \Modules\Taxonomy\Entities\TaxonomyTerm::where('taxonomy_id', 9)->whereIn('parent_id', $po)->get()->pluck('id')->toArray();
            $po2 = $faker->randomElements($po2, 6);

            $user->po = $po;
            $user->vid_diyalnosti = $po2;

        }
        dd($po2);

    }
}
