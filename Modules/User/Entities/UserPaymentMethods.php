<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;

class UserPaymentMethods extends Model
{
    protected $fillable = [];

    protected $guarded = ['_token'];

    protected $table = 'users_payment_methods';

    protected $connection = 'mysql';
}
