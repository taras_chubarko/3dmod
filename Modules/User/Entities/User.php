<?php

namespace Modules\User\Entities;

use Chrisbjr\ApiGuard\Models\Mixins\Apikeyable;
use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Modules\Filem\Entities\Filem;
use Modules\Groups\Entities\GroupMongo;
use Modules\Groups\Enums\NotifyTypeEnum;
use Modules\Notifi\Entities\Notifi;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Faker\Factory as Faker;
use Hootlex\Friendships\Traits\Friendable;

/**
 * Class User
 *
 * @package Modules\User\Entities
 *
 * @property integer id
 * @property Notifi  group_invites
 * @property Notifi  group_requests
 */
class User extends Authenticatable implements Transformable
{
    use Sluggable,
        TransformableTrait,
        Notifiable,
        EntrustUserTrait,
        Friendable,
        Apikeyable,
        HybridRelations;
    // use SoftDeletes;

    protected $connection = 'mysql';

    protected $fillable = ['email', 'password', 'name', 'sname', 'login'];

    protected $guarded = ['_token'];

    protected $table = 'users';

    protected $dates = ['deleted_at'];

    protected $hidden = ['password', 'remember_token'];

    protected $appends = [];

    protected $myAttributes = [
        'address',
        'name_full',
        'vid_diyalnosti',
        'views',
        'likes',
        'follows',
        'last_works',
        'new_notifi',
        'rating',
        'activity',
        'reviews',
        'registred',
        'po',
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'emailName',
            ],
        ];
    }

    /* public function getFullNameAttribute
     * @param $id
     *-----------------------------------
     *| полное имя
     *-----------------------------------
     */
    public function getEmailNameAttribute()
    {
        $name = explode('@', $this->email);

        return $name[0];
    }

    public function getFullNameAttribute()
    {
        $name[] = $this->name;
        $name[] = $this->sname;
        $name = implode(' ', $name);

        return $name;
    }

    /*
     * Привязка атрибутов
     */
    public function getMyAttributes($attributes)
    {
        if (is_array($attributes)) {
            $this->appends = $attributes;
        } else {
            if ($attributes == 'all') {
                $this->appends = $this->myAttributes;
            }
        }
    }

    /*
     * Аватарка
     */

    public function social()
    {
        return $this->hasMany(\Modules\User\Entities\UserSocial::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->hasMany(GroupMongo::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function group_invites()
    {
        return $this->hasMany(Notifi::class, 'to_user_id')->where('type', NotifyTypeEnum::INVITE_TYPE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function group_requests()
    {
        return $this->hasMany(Notifi::class, 'to_user_id')->where('type', NotifyTypeEnum::REQUEST);
    }

    /* public function social
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */

    public function setAddressAttribute($value)
    {
        $this->users_address_table()->detach();
        if ($value) {
            $this->users_address_table()->attach($this->id, [
                'address' => $value['address'],
                'country' => $value['country'],
                'city'    => $value['city'],
                'lat'     => $value['lat'],
                'lng'     => $value['lng'],
            ]);
        }
    }

    /* public function users_address_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_address_table()
    {
        return $this->belongsToMany(\Modules\User\Entities\User::class, 'users_address')
                    ->withPivot('address', 'country', 'city', 'lat', 'lng');
    }

    /* public function users_bg_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getAddressAttribute()
    {
        $field = new \stdClass;
        $field->title = 'address';

        $field->value = [
            'address' => data_get($this, 'users_address_table.0.pivot.address'),
            'country' => data_get($this, 'users_address_table.0.pivot.country'),
            'city'    => data_get($this, 'users_address_table.0.pivot.city'),
            'lat'     => (float)data_get($this, 'users_address_table.0.pivot.lat'),
            'lng'     => (float)data_get($this, 'users_address_table.0.pivot.lng'),
        ];

        $address[] = $field->value['city'];
        $address[] = $field->value['country'];
        $field->address = ($field->value['city']) ? implode(', ', $address) : null;

        unset($this->users_address_table);

        return $field;
    }

    /* public function users_birthday_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setAvatarAttribute($value)
    {
        $this->users_avatar_table()->detach();
        if ($value) {
            $this->users_avatar_table()->attach($this->id, [
                'fid' => $value,
            ]);
        }
    }

    /* public function users_dosvid_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_avatar_table()
    {
        return $this->belongsToMany(\Modules\Filem\Entities\Filem::class, 'users_avatar', 'user_id', 'fid')
                    ->withPivot('fid');
    }

    /* public function users_gender_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getAvatarAttribute()
    {
        $field = new \stdClass;
        $field->title = 'avatar';
        $field->fid = data_get($this, 'users_avatar_table.0.pivot.fid');

        unset($this->users_avatar_table);

        return $field;
    }

    /* public function users_name_shop_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setBgAttribute($value)
    {
        $this->users_bg_table()->detach();
        if ($value) {
            $this->users_bg_table()->attach($this->id, [
                'fid' => $value,
            ]);
        }
    }

    /* public function users_payment_methods_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_bg_table()
    {
        return $this->belongsToMany(\Modules\Filem\Entities\Filem::class, 'users_bg', 'user_id', 'fid')
                    ->withPivot('fid');
    }

    /* public function users_vid_diyalnosti_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getBgAttribute()
    {
        $field = new \stdClass;
        $field->title = 'bg';
        $field->fid = data_get($this, 'users_bg_table.0.pivot.fid');

        unset($this->users_bg_table);

        return $field;
    }

    /* public function users_web_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setBirthdayAttribute($value)
    {
        $this->users_birthday_table()->detach();
        if ($value) {
            $this->users_birthday_table()->attach($this->id, [
                'birthday' => \Carbon::parse($value['day'] . '-' . $value['month'] . '-' . $value['year']),
            ]);
        }
    }

    /* public function users_notification_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_birthday_table()
    {
        return $this->belongsToMany(\Modules\User\Entities\User::class, 'users_birthday')->withPivot('birthday');
    }

    /* public function users_po_table
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getBirthdayAttribute()
    {
        $field = new \stdClass;
        $field->title = 'birthday';
        $field->value = data_get($this, 'users_birthday_table.0.pivot.birthday');
        $field->data = [
            'day'   => ($field->value) ? \Carbon::parse($field->value)->format('j') : null,
            'month' => ($field->value) ? \Carbon::parse($field->value)->format('n') : null,
            'year'  => ($field->value) ? \Carbon::parse($field->value)->format('Y') : null,
        ];
        unset($this->users_birthday_table);

        return $field;
    }

    /* public function setAddressAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setDosvidAttribute($value)
    {
        $this->users_dosvid_table()->detach();
        if ($value) {
            $this->users_dosvid_table()->attach($this->id, [
                'dosvid' => $value,
            ]);
        }
    }

    /* public function getAddressAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_dosvid_table()
    {
        return $this->belongsToMany(\Modules\User\Entities\User::class, 'users_dosvid')->withPivot('dosvid');
    }

    /* public function setAvatarAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getDosvidAttribute()
    {
        $field = new \stdClass;
        $field->title = 'dosvid';
        $field->value = data_get($this, 'users_dosvid_table.0.pivot.dosvid');
        unset($this->users_dosvid_table);

        return $field;
    }

    /* public function getAvatarAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setGenderAttribute($value)
    {
        $this->users_gender_table()->detach();
        if ($value) {
            $this->users_gender_table()->attach($this->id, [
                'gender' => $value,
            ]);
        }
    }

    /* public function setBgAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_gender_table()
    {
        return $this->belongsToMany(\Modules\User\Entities\User::class, 'users_gender')->withPivot('gender');
    }

    /* public function getBgAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getGenderAttribute()
    {
        $field = new \stdClass;
        $field->title = 'gender';
        $field->value = data_get($this, 'users_gender_table.0.pivot.gender');
        unset($this->users_gender_table);

        return $field;
    }

    /* public function setBirthdayAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setNameShopAttribute($value)
    {
        $this->users_name_shop_table()->detach();
        if ($value) {
            $this->users_name_shop_table()->attach($this->id, [
                'name_shop' => $value,
            ]);
        }
    }

    /* public function getBirthdayAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_name_shop_table()
    {
        return $this->belongsToMany(\Modules\User\Entities\User::class, 'users_name_shop')->withPivot('name_shop');
    }

    /* public function setDosvidAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getNameShopAttribute()
    {
        $field = new \stdClass;
        $field->title = 'name_shop';
        $field->value = data_get($this, 'users_name_shop_table.0.pivot.name_shop');
        unset($this->users_name_shop_table);

        return $field;
    }

    /* public function getDosvidAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setPaymentMethodsAttribute($value)
    {
        $this->users_payment_methods_table()->detach();
        if ($value) {
            $this->users_payment_methods_table()->attach($this->id, [
                'payment_methods' => $value,
            ]);
        }
    }

    /* public function setGenderAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_payment_methods_table()
    {
        return $this->belongsToMany(\Modules\User\Entities\User::class, 'users_payment_methods')
                    ->withPivot('payment_methods');
    }

    /* public function getGenderAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getPaymentMethodsAttribute()
    {
        $field = new \stdClass;
        $field->title = 'payment_methods';
        $field->value = data_get($this, 'users_payment_methods_table.0.pivot.payment_methods');
        unset($this->users_payment_methods_table);

        return $field;
    }

    /* public function setNameShopAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setVidDiyalnostiAttribute($value)
    {
        $this->users_vid_diyalnosti_table()->detach();
        if ($value) {
            foreach ($value as $k => $item) {
                $this->users_vid_diyalnosti_table()->attach($this->id, [
                    'vid_diyalnosti' => $item,
                    'sort'           => $k,
                ]);
            }
        }
    }

    /* public function getNameShopAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_vid_diyalnosti_table()
    {
        return $this->belongsToMany(\Modules\Taxonomy\Entities\TaxonomyTerm::class, 'users_vid_diyalnosti', 'user_id', 'vid_diyalnosti')
                    ->withPivot('vid_diyalnosti', 'sort');
    }

    /* public function setPaymentMethodsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getVidDiyalnostiAttribute()
    {
        $field = new \stdClass;
        $field->title = 'vid_diyalnosti';
        $field->values = [];
        $field->arr = [];
        $field->locale = [];
        if ($this->users_vid_diyalnosti_table->count()) {
            $ru = [];
            $uk = [];
            $name_en = [];
            $name_ru = [];
            $name_uk = [];
            foreach ($this->users_vid_diyalnosti_table as $item) {
                $field->values[$item->pivot->sort] = [
                    'id'   => $item->id,
                    'name' => $item->name,
                ];
                //
                $name_en[] = $item->name;
                $name_ru[] = $item->name_ru;
                $name_uk[] = $item->name_uk;
                //
                $field->arr[$item->pivot->sort] = $item->id;
                //
                $ru[$item->pivot->sort] = [
                    'id'   => $item->id,
                    'name' => $item->name_ru,
                ];
                //
                $uk[$item->pivot->sort] = [
                    'id'   => $item->id,
                    'name' => $item->name_uk,
                ];
                //
            }
            //
            $field->locale['en'] = implode(', ', $name_en);
            $field->locale['ru'] = implode(', ', $name_ru);
            $field->locale['uk'] = implode(', ', $name_uk);
        }
        unset($this->users_vid_diyalnosti_table);

        return $field;
    }

    /* public function getPaymentMethodsAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setPoAttribute($value)
    {
        $this->users_po_table()->detach();
        if ($value) {
            foreach ($value as $k => $item) {
                $this->users_po_table()->attach($this->id, [
                    'po_id' => $item,
                    'sort'  => $k,
                ]);
            }
        }
    }

    /* public function setVidDiyalnostiAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_po_table()
    {
        return $this->belongsToMany(\Modules\Taxonomy\Entities\TaxonomyTerm::class, 'users_po', 'user_id', 'po_id')
                    ->withPivot('po_id', 'sort');
    }

    /* public function getVidDiyalnostiAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getPoAttribute()
    {
        $field = new \stdClass;
        $field->title = 'po';
        $field->values = [];
        $field->arr = [];
        $field->locale = [];
        if ($this->users_po_table->count()) {
            $ru = [];
            $uk = [];
            $name_en = [];
            $name_ru = [];
            $name_uk = [];
            foreach ($this->users_po_table as $item) {
                $field->values[$item->pivot->sort] = [
                    'id'   => $item->id,
                    'name' => $item->name,
                ];
                //
                $name_en[] = $item->name;
                $name_ru[] = $item->name_ru;
                $name_uk[] = $item->name_uk;
                //
                $field->arr[$item->pivot->sort] = $item->id;
                //
                $ru[$item->pivot->sort] = [
                    'id'   => $item->id,
                    'name' => $item->name_ru,
                ];
                //
                $uk[$item->pivot->sort] = [
                    'id'   => $item->id,
                    'name' => $item->name_uk,
                ];
                //
            }
            //
            $field->locale['en'] = implode(', ', $name_en);
            $field->locale['ru'] = implode(', ', $name_ru);
            $field->locale['uk'] = implode(', ', $name_uk);
        }
        unset($this->users_po_table);

        return $field;
    }

    /* public function setPoAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setWebAttribute($value)
    {
        $this->users_web_table()->detach();
        if ($value) {
            foreach ($value as $k => $item) {
                if (!empty($item['link'])) {
                    $this->users_web_table()->attach($this->id, [
                        'link' => $item['link'],
                        'soc'  => $k,
                    ]);
                }
            }
        }
    }

    /* public function getPoAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_web_table()
    {
        return $this->belongsToMany(\Modules\User\Entities\User::class, 'users_web')->withPivot('link', 'soc');
    }

    /* public function setWebAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getWebAttribute()
    {
        $field = new \stdClass;
        $field->title = 'web';
        $field->values = [];
        if ($this->users_web_table->count()) {
            foreach ($this->users_web_table as $item) {
                $field->values[$item->pivot->soc] = $item->pivot->link;
            }
        }
        unset($this->users_web_table);

        return $field;
    }

    /* public function getWebAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setRoleAttribute($value)
    {
        $this->roles()->detach();
        if ($value) {
            foreach ($value as $item) {
                $role = \Modules\User\Entities\Role::findOrFail($item);
                if ($role) {
                    $this->attachRole($role);
                }
            }
        }
    }

    /* public function setRoleAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getRoleAttribute()
    {
        $field = new \stdClass;
        $field->title = 'role';
        $field->value = $this->roles;
        $field->arr = $this->roles->pluck('id');
        unset($this->roles);

        return $field;
    }

    /* public function getRoleAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function setNotificationAttribute($value)
    {
        $this->users_notification_table()->detach();
        if ($value) {
            foreach ($value as $k => $item) {
                $this->users_notification_table()->attach($this->id, [
                    'notification_id' => $item,
                    'sort'            => $k,
                ]);
            }
        }
    }

    /* public function setRoleAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function users_notification_table()
    {
        return $this->belongsToMany(\Modules\Taxonomy\Entities\TaxonomyTerm::class, 'users_notification', 'user_id', 'notification_id')
                    ->withPivot('notification_id', 'sort');
    }

    /* public function getNotificationAttribute
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getNotificationAttribute()
    {
        $field = new \stdClass;
        $field->title = 'notification';
        $field->values = [];
        $field->arr = [];
        if ($this->users_notification_table->count()) {
            foreach ($this->users_notification_table as $item) {
                $field->values[$item->pivot->sort] = [
                    'id'   => $item->id,
                    'name' => $item->name,
                ];
                $field->arr[$item->pivot->sort] = $item->id;
            }
        }

        unset($this->users_notification_table);

        return $field;
    }

    /* public function getLastWorks
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getLastWorksAttribute()
    {
        $faker = Faker::create('uk_UA');
        $field = new \stdClass;
        $field->items = [];
        foreach (range(0, 2) as $i) {
            $field->items[] = [
                'link'  => '/',
                'image' => $faker->numberBetween($min = 1, $max = 151),
            ];
        }
        $field->count = $faker->numberBetween($min = 0, $max = 9000);

        return $field;
    }

    /* public function getSocialLinksAttribute
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getSocialLinksAttribute()
    {
        $field = new \stdClass;
        $field->values = [];

        if ($this->social->count()) {
            foreach ($this->social as $item) {
                $field->values[] = [
                    'id'      => $item->id,
                    'network' => $item->network,
                    'profile' => $item->profile,
                ];
            }
        }
        unset($this->social);

        return $field;
    }

    /* public function friendsOfMine
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function friendsOfMine()
    {
        return $this->belongsToMany(\Modules\User\Entities\User::class, 'friendships', 'sender_id', 'recipient_id')
                    ->withPivot('created_at');
    }

    /* public function friendOf
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function friendOf()
    {
        return $this->belongsToMany(\Modules\User\Entities\User::class, 'friendships', 'recipient_id', 'sender_id')
                    ->withPivot('created_at');
    }

    /* public function getFriendsQuery
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function friendsReq()
    {
        //return $this->friendsOfMine()->wherePivot('status', 0)->get()->merge($this->friendOf()->wherePivot('status', 0)->get());
        return $this->getFriendsQuery()->where('status', 0)->orderBy('created_at', 'DESC');
    }

    /* public function friendsReq
    * @param
    *-----------------------------------
    *|Hootlex\Friendships\Models\Friendship
    *-----------------------------------
    */

    public function getFriendsQuery()
    {
        $query = \Hootlex\Friendships\Models\Friendship::where(function($query) {
            $query->where(function($q) {
                $q->whereSender($this);
            })->orWhere(function($q) {
                $q->whereRecipient($this);
            });
        });

        return $query;
    }

    /* public function getMyFirends
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */

    public function getMyFirends()
    {
        return $this->getFriendsQuery()->where('status', 1)->orderBy('created_at', 'DESC');
    }

    /* public function getPendingBothFriendships
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getPendingBothFriendships()
    {
        $user = $this;
        $req = $user->getAllFriendships()->where('status', 0)->filter(function($val) use ($user) {
            return $val->sender_id !== $user->id || $val->recipient_id !== $user->id;
        })->sortByDesc('created_at');
        //
        $items = [];
        foreach ($req as $item) {
            $items[] = ($item->sender_id !== $user->id) ? $item->sender_id : $item->recipient_id;
        }
        //
        $data = self::whereIn('users.id', $items);

        return $data;
    }

    /* public function getLocationAttribute
    * @param 
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function getLocationAttribute()
    {
        $location = [];
        $sng = collect([
            'Россия',
            'Украина',
            'Белоруссия',
            'Казахстан',
            'Армения',
            'Киргизия',
            'Молдавия',
            'Таджикистан',
            'Russia',
            'Ukraine',
            'Byelorussia',
            'Kazakhstan',
            'Armenia',
            'Kyrgyzstan',
            'Moldavia',
            'Tajikistan',
            'Росія',
            'Україна',
            'Білорусія',
            'Казахстан',
            'Вірменія',
            'Киргизія',
            'Молдавія',
            'Таджикистан',
        ]);

        if ($this->users_address_table->count()) {
            $location['address'] = $this->address->value['address'];
            $location['address_short'] = $this->address->value['country'] . ', ' . $this->address->value['city'];
            $location['country'] = $this->address->value['country'];
            $location['locality'] = $this->address->value['city'];
            $location['geometry'] = [
                'lat' => $this->address->value['lat'],
                'lng' => $this->address->value['lng'],
            ];
            $location['world'] = $sng->contains($this->address->value['country']) ? 'sng' : 'world';
        }

        return $location;
    }

}
