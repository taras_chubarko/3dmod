<?php

namespace Modules\User\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace App\Repositories;
 */
interface UserRepository extends RepositoryInterface
{
    public function socialLogin($request);
    //
    public function get($id);
    //
    public function block($slug);
    //
    public function findOrFail($id);
}
