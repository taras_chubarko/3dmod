<?php

namespace Modules\User\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\User\Repositories\UserRepository;
use Modules\User\Entities\User;
use Modules\User\Entities\UserSocial;
use Modules\User\Entities\Role;
use Modules\User\Events\UserSocialRegisterEvent;
use Faker\Factory as Faker;

/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /* public function socialLogin($request)
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function socialLogin($request)
    {
        $soc = UserSocial::where('email', $request->email)->first();
        if(!$soc)
        {
            $role = Role::findOrFail(3); //Покупець

            $pass = str_random(6);
            $user = $this->model->create([
                'name'      => $request->first_name,
                'sname'     => $request->last_name,
                'email'     => $request->email,
                'password'  => bcrypt($pass),
            ]);
            $user->attachRole($role);

            UserSocial::create([
                'user_id' 		=> $user->id,
                'manual' 		=> $request->manual,
                'email' 		=> $request->email,
                'phone' 		=> $request->phone,
                'city' 			=> $request->city,
                'network' 		=> $request->network,
                'identity' 		=> $request->identity,
                'photo_big' 	=> $request->photo_big,
                'profile' 		=> $request->profile,
                'last_name' 	=> $request->last_name,
                'uid' 			=> $request->uid,
                'photo' 		=> $request->photo,
                'verified_email' 	=> $request->verified_email,
                'first_name' 		=> $request->first_name,
            ]);
            \Auth::loginUsingId($user->id);
            return $user;
        }
        else
        {
            $user = $this->model->where('email', $request->email)->first();
            \Auth::loginUsingId($user->id);
            return $user;
        }
    }
    //
    public function get($id = null)
    {
        $user_id = ($id) ? $id : auth()->user()->id;
        $user = $this->model->find($user_id);
        $faker = Faker::create('uk_UA');
        //
        $roles = [];
        foreach ($user->roles as $role)
        {
            $roles[] = [
                'id' => $role->id,
                'name' => $role->name,
            ];
        }
        //
        $data = [
            'id'        => $user->id,
            'name'      => $user->name,
            'roles'     => $roles,
            'slug'      => $user->slug,
            'location'  => $user->location,
            'pix'       => $faker->numberBetween(0, 1000),
        ];
        return $data;
    }

    /* public function block
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function block($slug = null)
    {
        $slug = ($slug) ? $slug : auth()->user()->slug;
        $user = $this->model->where('slug', $slug)->first();
        $item = [
            'id'        => $user->id,
            'fullname'  => $user->full_name,
            'rating'    => 0,
            'activity'  => 0,
            'reviews'   => 0,
            'address'   => $user->address,
            'registred' => $user->created_at->format('d.m.Y'),
        ];
        return $item;
    }

    /* public function findOrFail($id)
    * @param
    *-----------------------------------
    *|
    *-----------------------------------
    */
    public function findOrFail($id)
    {
        $user = $this->model->where('id', $id)->first();
        return $user;
    }


}
