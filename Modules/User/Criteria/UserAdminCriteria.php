<?php

namespace Modules\User\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserAdminCriteria
 * @package namespace App\Criteria;
 */
class UserAdminCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $segments = \Request::segments();
        
        $model = $model->orderBy('users.created_at', 'DESC')->where('id', '<>', 0);
        
        //$model = $model->leftJoin('role_users', 'users.id', '=', 'role_users.user_id');
        //
        //if($segments[2] == 'customer')
        //{
        //    $model = $model->where('role_users.role_id', 4);
        //}
        //
        //if($segments[2] == 'executor')
        //{
        //    $model = $model->where('role_users.role_id', 3);
        //}
        //
        //if($segments[2] == 'moderator')
        //{
        //    $model = $model->where('role_users.role_id', 2);
        //}
        //
        //if($segments[2] == 'admin')
        //{
        //    $model = $model->where('role_users.role_id', 1);
        //}
        //
        //if(\Request::has('id'))
        //{
        //    $model = $model->where('users.id', \Request::get('id'));
        //}
        //
        //if(\Request::has('email'))
        //{
        //    $model = $model->where('users.email', \Request::get('email'));
        //}
        //
        //if(\Request::has('location'))
        //{
        //    $model = $model->whereHas('location', function($query){
        //        $query->where('location', \Request::get('location'));
        //    });
        //}
        
        return $model;
    }
}
