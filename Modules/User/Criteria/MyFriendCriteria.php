<?php

namespace Modules\User\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;
use Modules\User\Entities\MyFriends;

/**
 * Class MyFriendCriteria
 * @package namespace App\Criteria;
 */
class MyFriendCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user = auth()->user();
        $id = $user->getFriends()->pluck('id');
        $model = $model->whereIn('users.id', $id);

        if(\Request::has('name'))
        {
            $name = explode(' ',\Request::get('name'));

            if(empty($name[1]))
            {
                $model = $model->where('users.name', 'like', '%'.$name[0].'%');
                $model = $model->orWhere('users.sname', 'like', '%'.$name[0].'%');
                $model = $model->whereIn('users.id', $id);
            }
            else
            {
                $model = $model->where('users.name', 'like', '%'.$name[0].'%');
                $model = $model->where('users.sname', 'like', '%'.$name[1].'%');
                $model = $model->whereIn('users.id', $id);
            }
        }
        else
        {
            $model = $model->whereIn('users.id', $id);
        }

        $model = $model->leftjoin('friendships', function($join){
            $join->on('users.id','=','friendships.sender_id'); // i want to join the users table with either of these columns
            $join->orOn('users.id','=','friendships.recipient_id');
        });
        $model = $model->orderBy('friendships.updated_at', 'DESC');

        $model = $model->groupby('users.id');



        $model = $model->select('users.*');

//        $model = $model->whereHas('friendsOfMine', function ($q){
//            //$q->orderBy('friendships.created_at', 'DECS');
//        });

//        $model = $model->whereHas('friendOf', function ($q){
//            //$q->orderBy('friendships.created_at', 'DECS');
//        });




        return $model;
    }
}
