<?php

namespace Modules\User\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;
use Illuminate\Http\Request;

/**
 * Class MyFriendCriteria
 * @package namespace App\Criteria;
 */
class AddFriendCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $user = auth()->user();

        $req = $user->getFriendRequests()->pluck('sender_id');
        $notInID = $user->getFriends()->pluck('id')->merge($req);
        $notInID = $notInID->push($user->id);

        $model = $model->whereNotIn('id', $notInID);

        if(\Request::has('name'))
        {
            $name = explode(' ',\Request::get('name'));

            if(empty($name[1]))
            {
                $model = $model->where('name', 'like', '%'.$name[0].'%');
                $model = $model->orWhere('sname', 'like', '%'.$name[0].'%');
            }
            else
            {
                $model = $model->where('name', 'like', '%'.$name[0].'%');
                $model = $model->where('sname', 'like', '%'.$name[1].'%');

            }
        }
        //
        if(\Request::has('country') && \Request::has('city'))
        {
            if(\Request::get('country') != '' && \Request::get('city') != '')
            {
                $model = $model->whereHas('users_address_table', function($q)
                {
                    $q->where('country', \Request::get('country'));
                    $q->where('city', \Request::get('city'));
                });
            }
        }
        //
        if(\Request::has('po'))
        {
            $model = $model->whereHas('users_po_table', function($q)
            {
                $q->whereIn('po_id', \Request::get('po'));
            });
        }
        
        return $model;
    }
}
