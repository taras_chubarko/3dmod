<?php

namespace Modules\Groups\Types;

use GraphQL\Type\Definition\Type;
use M1naret\GraphQL\Support\Facades\GraphQL;
use M1naret\GraphQL\Support\Type as GraphQLType;
use Modules\Groups\Entities\GroupMongo;
use Modules\Groups\Enums\GraphQL\GroupInvitesGraphQLEnum;
use Modules\Groups\Enums\GraphQL\GroupTypesGraphQLEnum;
use Modules\Notifi\Entities\Notifi;

/**
 * Class InviteType
 *
 * @package Modules\Groups\Types
 */
class InviteType extends GraphQLType
{
    /** @var string */
    const NAME = 'invite';

    /** @var array */
    protected $attributes = [
        'name'        => self::NAME,
        'description' => 'Тип данных приглашение',
        'model'       => Notifi::class,
    ];

    /**
     * @return array
     */
    public function fields() : array
    {
        return [
            'request_id' => [
                'type' => Type::string(),
            ],
            'message'    => [
                'type' => Type::string(),
            ],
            'type'       => [
                'type' => Type::string(),
            ],
            'owner'      => [
                'type'       => GraphQL::type(UserType::NAME),
            ],
        ];
    }

    /**
     * @param Notifi $notify
     * @param        $args
     *
     * @return mixed
     */
    protected function resolveTypeField(Notifi $notify, $args)
    {
        return $notify->type;
    }

    /**
     * @param Notifi $notify
     * @param        $args
     *
     * @return string
     */
    protected function resolveMessageField(Notifi $notify, $args)
    {
        return $notify->message;
    }

}
