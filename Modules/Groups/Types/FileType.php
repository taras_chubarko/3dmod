<?php

namespace Modules\Groups\Types;

use GraphQL\Type\Definition\Type;
use M1naret\GraphQL\Support\Facades\GraphQL;
use M1naret\GraphQL\Support\Type as GraphQLType;
use Modules\Filem\Entities\Filem;
use Modules\Groups\Entities\GroupMongo;
use Modules\Groups\Enums\GraphQL\GroupInvitesGraphQLEnum;
use Modules\Groups\Enums\GraphQL\GroupTypesGraphQLEnum;

/**
 * Class FileType
 *
 * @package Modules\Groups\Types
 */
class FileType extends GraphQLType
{
    /** @var string  */
    const NAME = 'file';

    /** @var array  */
    protected $attributes = [
        'name'        => self::NAME,
        'description' => 'Тип данных файл',
        'model'       => Filem::class,
    ];

    /**
     * @return array
     */
    public function fields() : array
    {
        return [
            'url'  => [
                'type'       => Type::string(),
                'selectable' => false,
                'alias'      => true,
                'always'     => 'filename,uri',
                'args' => [
                    'width'  => Type::int(),
                    'height' => Type::int(),
                ],
            ],
            'filename' => [
                'type'  => Type::string(),
            ],
            'ext'  => [
                'type' => Type::string(),
            ],
        ];
    }

    /**
     * @param Filem $file
     * @param       $args
     *
     * @return string
     */
    protected function resolveUrlField(Filem $file, $args)
    {
        return route('filem.filem', [
            $file->uri,
            array_get($args, 'width', 100) . 'x' . array_get($args, 'height', 100),
            $file->filename,
        ]);
    }

}
