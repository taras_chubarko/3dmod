<?php

namespace Modules\Groups\Types;

use GraphQL\Type\Definition\Type;
use M1naret\GraphQL\Support\Type as GraphQLType;
use Modules\User\Entities\User;
use M1naret\GraphQL\Support\Facades\GraphQL;

/**
 * Class UserType
 *
 * @package Modules\Groups\Types
 */
class UserType extends GraphQLType
{
    const NAME = 'user';

    protected $attributes = [
        'name'        => self::NAME,
        'description' => 'Тип данных пользователь',
        'model'       => User::class,
    ];

    public function fields() : array
    {
        return [
            'id'      => [
                'type'        => Type::int(),
                'description' => 'ID пользователя',
            ],
            'email'   => [
                'type'        => Type::string(),
                'description' => 'Email пользователя',
            ],
            'name'    => [
                'type'        => Type::string(),
                'description' => 'Имя',
            ],
            'sname'   => [
                'type'        => Type::string(),
                'description' => 'Фамилия',
            ],
            'login'   => [
                'type'        => Type::string(),
                'description' => 'Логин',
            ],
            'friends' => [
                'type'        => Type::listOf(GraphQL::type(UserType::NAME)),
                'description' => 'Друзья',
            ],
        ];
    }

}
