<?php

namespace Modules\Groups\Types;

use GraphQL\Type\Definition\Type;
use M1naret\GraphQL\Support\Facades\GraphQL;
use M1naret\GraphQL\Support\Type as GraphQLType;
use Modules\Groups\Entities\GroupMongo;
use Modules\Groups\Enums\GraphQL\GroupInvitesGraphQLEnum;
use Modules\Groups\Enums\GraphQL\GroupTypesGraphQLEnum;

/**
 * Class GroupType
 *
 * @package Modules\Groups\Types
 */
class GroupType extends GraphQLType
{
    /** @var string */
    const NAME = 'group';

    /** @var array */
    protected $attributes = [
        'name'        => self::NAME,
        'description' => 'Тип данных группа',
        'model'       => GroupMongo::class,
    ];

    /**
     * @return array
     */
    public function fields() : array
    {
        return [
            'name'        => [
                'type'        => Type::string(),
                'description' => 'Название группы',
            ],
            'description' => [
                'type'        => Type::string(),
                'description' => 'Описание группы',
            ],
            'type'        => [
                'type' => GraphQL::type(GroupTypesGraphQLEnum::NAME),
            ],
            'invite_type' => [
                'type' => GraphQL::type(GroupInvitesGraphQLEnum::NAME),
            ],
            'is_forum'    => [
                'type'        => Type::boolean(),
                'description' => 'Признак, является ли форумом',
            ],
            'is_active'   => [
                'type'        => Type::boolean(),
                'description' => 'Признак, активна ли',
            ],
            'image'       => [
                'type' => GraphQL::type(FileType::NAME),
            ],
            'owner'       => [
                'type' => GraphQL::type(UserType::NAME),
            ],
            'members'     => [
                'type'    => Type::listOf(GraphQL::type(UserType::NAME)),
                'args'    => [
                    'limit' => Type::int(),
                ],
                'resolve' => function(GroupMongo $group, array $args = []) {
                    return $group->members(array_get($args, 'limit'))->get();
                },
            ],
        ];
    }

    /**
     * @param GroupMongo $group
     * @param            $args
     *
     * @return mixed
     */
    protected function resolveIsActiveField(GroupMongo $group, $args)
    {
        return array_get($group, 'is_active', false);
    }

    /**
     * @param GroupMongo $group
     * @param            $args
     *
     * @return mixed
     */
    protected function resolveIsForumField(GroupMongo $group, $args)
    {
        return array_get($group, 'is_forum', false);
    }

}
