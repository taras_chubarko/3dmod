<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/22/2018
 * Time: 9:46 PM
 */

namespace Modules\Groups\Enums;

use Modules\Groups\Support\Enum;

class NotifyTypeEnum extends Enum
{

    const CANCELED = 3;

    const INVITE_TYPE = 'group_invite';
    const NOTIFY = 'notifi_8';

    const REQUEST = 'notifi_9';
    const REQUEST_TYPE = 'group_request';

}