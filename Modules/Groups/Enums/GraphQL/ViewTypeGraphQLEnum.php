<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/22/2018
 * Time: 10:00 PM
 */

namespace Modules\Groups\Enums\GraphQL;

use M1naret\GraphQL\Support\Type as GraphQLType;
use Modules\Groups\Enums\ViewTypeEnum;

class ViewTypeGraphQLEnum extends GraphQLType
{
    const NAME = 'view_type';

    protected $enumObject = true;

    protected $attributes = [
        'name'        => self::NAME,
        'description' => 'Типы приложения',
        'values'      => [
            'desktop' => [
                'value'       => ViewTypeEnum::DESKTOP,
                'description' => 'Пк версия',
            ],
            'mobile' => [
                'value'       => ViewTypeEnum::MOBILE,
                'description' => 'Мобильное приложение',
            ],
        ],
    ];

}