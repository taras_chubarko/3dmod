<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/22/2018
 * Time: 10:00 PM
 */

namespace Modules\Groups\Enums\GraphQL;

use Modules\Groups\Enums\GroupInvitationEnum;
use M1naret\GraphQL\Support\Type as GraphQLType;

class GroupInvitesGraphQLEnum extends GraphQLType
{
    const NAME = 'group_invites_types_enum';

    protected $enumObject = true;

    protected $attributes = [
        'name'        => self::NAME,
        'description' => 'Типы приглашений в группу',
        'values'      => [
            'all_members' => [
                'value'       => GroupInvitationEnum::ALL_MEMBERS,
                'description' => 'Приглашать могут все учатсники группы',
            ],
            'admins_mods' => [
                'value'       => GroupInvitationEnum::ADMINS_MODS,
                'description' => 'Приглашать могу админы и модераторы',
            ],
            'admins'      => [
                'value'       => GroupInvitationEnum::ADMINS,
                'description' => 'Приглашать могут только админы',
            ],
        ],
    ];

}