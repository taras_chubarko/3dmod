<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/22/2018
 * Time: 10:00 PM
 */

namespace Modules\Groups\Enums\GraphQL;

use Modules\Groups\Enums\GroupPrivacyEnum;
use M1naret\GraphQL\Support\Type as GraphQLType;

class GroupTypesGraphQLEnum extends GraphQLType
{
    const NAME = 'group_types_enum';

    protected $enumObject = true;

    protected $attributes = [
        'name'        => self::NAME,
        'description' => 'Типы групп',
        'values'      => [
            'public'              => [
                'value'       => GroupPrivacyEnum::PUBLIC,
                'description' => 'Публичная группа',
            ],
            'private'              => [
                'value'       => GroupPrivacyEnum::PRIVATE,
                'description' => 'Приватная группа',
            ],
            'hidden'              => [
                'value'       => GroupPrivacyEnum::HIDDEN,
                'description' => 'Скрытая группа',
            ],
        ],
    ];

}