<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/22/2018
 * Time: 10:00 PM
 */

namespace Modules\Groups\Enums\GraphQL;

use Modules\Groups\Enums\GroupPrivacyEnum;
use M1naret\GraphQL\Support\Type as GraphQLType;
use Modules\Groups\Enums\NotifyTypeEnum;

class InviteTypesGraphQLEnum extends GraphQLType
{
    const NAME = 'invite_types';

    protected $enumObject = true;

    protected $attributes = [
        'name'        => self::NAME,
        'description' => 'Типы приглашений',
        'values'      => [
            'request'              => [
                'value'       => NotifyTypeEnum::REQUEST_TYPE,
                'description' => 'Запрос на участие в группе',
            ],
            'invite'              => [
                'value'       => NotifyTypeEnum::INVITE_TYPE,
                'description' => 'Приглашение в чужую группу',
            ],
        ],
    ];

}