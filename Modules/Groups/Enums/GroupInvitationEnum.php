<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/22/2018
 * Time: 9:46 PM
 */

namespace Modules\Groups\Enums;

use Modules\Groups\Support\Enum;

class GroupInvitationEnum extends Enum
{

    const ALL_MEMBERS = 1;

    const ADMINS_MODS = 2;

    const ADMINS = 3;

}