<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/22/2018
 * Time: 9:46 PM
 */

namespace Modules\Groups\Enums;

use Modules\Groups\Support\Enum;

class ViewTypeEnum extends Enum
{

    const DESKTOP = 1;

    const MOBILE = 2;

}