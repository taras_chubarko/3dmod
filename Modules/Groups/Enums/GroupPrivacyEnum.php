<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/22/2018
 * Time: 9:46 PM
 */

namespace Modules\Groups\Enums;

use Modules\Groups\Support\Enum;

class GroupPrivacyEnum extends Enum
{

    const PUBLIC = 1;

    const PRIVATE = 2;

    const HIDDEN = 3;

}