<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/24/2018
 * Time: 7:19 PM
 */

namespace Modules\Groups\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class TranslatableScopeTrait implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model   $model
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $translatableAttributes = $model->getTranslatableAttributes();

        $columns = $builder->getQuery()->columns;
        if (\is_array($columns)) {
            foreach ($columns as $column) {
                if (\is_string($column) && \in_array($column, $translatableAttributes, true)) {
                    $builder->addSelect("{$column}_trans");
                }
            }
        }
    }

}