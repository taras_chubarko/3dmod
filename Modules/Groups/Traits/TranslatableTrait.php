<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/24/2018
 * Time: 7:17 PM
 */

namespace Modules\Groups\Traits;

use Modules\Groups\Exceptions\AttributeIsNotTranslatableException;
use Illuminate\Support\Str;

trait TranslatableTrait
{
    protected $translatableFieldsSuffix = '_trans';

    private $withAllTranslations = true;

    /**
     * @throws \InvalidArgumentException
     */
    protected static function bootTranslatable()
    {
        static::addGlobalScope(new TranslatableScopeTrait());
    }

    /**
     * @param  string $attribute Attribute name
     *
     * @return mixed
     *
     * @throws AttributeIsNotTranslatableException
     */
    public function getAttribute($attribute)
    {
        if ($this->isTranslated($attribute)) {
            return $this->getTranslation($attribute, config('app.locale'));
        }

        /** @noinspection PhpUndefinedClassInspection */
        return parent::getAttribute($attribute);
    }

    /**
     * @param  string $attribute Attribute name
     * @param  string $value     Text value in default locale.
     *
     * @return void
     *
     * @throws AttributeIsNotTranslatableException
     */
    public function setAttribute($attribute, $value)
    {
        /** @noinspection CallableParameterUseCaseInTypeContextInspection */
        if (!\is_array($value) && $this->isTranslatable($attribute)) {
            $locale = config('app.locale');
            $this->setTranslation($attribute, $locale, $value);

            return;
        }

        /** @noinspection PhpUndefinedClassInspection */
        parent::setAttribute($attribute, $value);
    }

    /**
     * @param string $attribute
     * @param string $locale
     *
     * @return mixed
     *
     * @throws AttributeIsNotTranslatableException
     */
    public function getTranslation($attribute, $locale)
    {
        $locale = $this->normalizeLocale($attribute, $locale);

        $translations = $this->getTranslations($attribute);

        $translation = array_get($translations, $locale);
        if (!$translation && $locale !== config('app.fallback_locale')) {
            $translation = array_get($translations, config('app.fallback_locale'), '');
        }

        /** @noinspection PhpUndefinedMethodInspection */
        if ($this->hasGetMutator($attribute)) {
            /** @noinspection PhpUndefinedMethodInspection */
            return $this->mutateAttribute($attribute, $translation);
        }

        return $translation?:parent::getAttribute($attribute);
    }

    /**
     * @param $attribute
     *
     * @return array
     *
     * @throws AttributeIsNotTranslatableException
     */
    public function getTranslations($attribute)
    {
        $this->guardAgainstUntranslatableAttribute($attribute);

        $transAttr = $attribute . $this->translatableFieldsSuffix;

        if ($this->isTranslated($attribute)) {
            $translations = json_decode($this->attributes[$transAttr], true);
            if (!empty($translations) && json_last_error() === JSON_ERROR_NONE) {
                return $translations;
            }
        }

        /** @noinspection PhpUndefinedClassInspection */
        return [
            config('app.fallback_locale') => parent::getAttribute($attribute),
        ];
    }

    /** @noinspection ReturnTypeCanBeDeclaredInspection */
    /**
     * @param string $attribute
     * @param string $locale
     * @param        $value
     *
     * @return $this
     *
     * @throws AttributeIsNotTranslatableException
     */
    public function setTranslation($attribute, $locale, $value)
    {
        if ($locale === config('app.fallback_locale')) {
            $this->attributes[$attribute] = $value;
        }

        $this->guardAgainstUntranslatableAttribute($attribute);

        $translations = $this->getTranslations($attribute);

        /** @noinspection PhpUndefinedMethodInspection */
        if ($this->hasSetMutator($attribute)) {
            $method = 'set' . Str::studly($attribute) . 'Attribute';
            $this->{$method}($value, $locale);
            /** @noinspection PhpUndefinedFieldInspection */
            $value = $this->attributes[$attribute];
        }

        $translations[$locale] = $value;

        $transAttr = $attribute . $this->translatableFieldsSuffix;

        $this->attributes[$transAttr] = $this->asJson($translations);

        return $this;
    }

    /** @noinspection ReturnTypeCanBeDeclaredInspection */
    /**
     * @param string $attribute
     * @param array  $translations
     *
     * @return $this
     *
     * @throws AttributeIsNotTranslatableException
     */
    public function setTranslations($attribute, array $translations)
    {
        $this->guardAgainstUntranslatableAttribute($attribute);

        foreach ($translations as $locale => $translation) {
            $this->setTranslation($attribute, $locale, $translation);
        }

        return $this;
    }

    /** @noinspection ReturnTypeCanBeDeclaredInspection */
    /**
     * @param string $attributes
     * @param string $locale
     *
     * @return self
     *
     * @throws AttributeIsNotTranslatableException
     */
    public function forgetTranslation($attributes, $locale)
    {
        $translations = $this->getTranslations($attributes);

        unset($translations[$locale]);

        $this->setTranslations($attributes, $translations);

        return $this;
    }

    /**
     * Check if an attribute is new translatable.
     *
     * @param string $attribute
     *
     * @return boolean
     */
    public function isTranslatable($attribute)
    {
        return \in_array($attribute, $this->getTranslatableAttributes(), true);
    }

    /**
     * Get new translatable attributes
     *
     * @return array
     */
    public function getTranslatableAttributes()
    {
        return (!empty($this->translatable) && \is_array($this->translatable)) ? $this->translatable : [];
    }

    /**
     * @param string $attribute
     * @param string $locale
     *
     * @return string
     *
     * @throws AttributeIsNotTranslatableException
     */
    protected function normalizeLocale($attribute, $locale)
    {
        return \in_array($locale, $this->getTranslatedLocales($attribute), true) ?
            $locale :
            config('app.fallback_locale');
    }

    /**
     * @param string $attribute
     *
     * @return array
     *
     * @throws AttributeIsNotTranslatableException
     */
    public function getTranslatedLocales($attribute)
    {
        return array_keys($this->getTranslations($attribute) ?: []);
    }

    /**
     * @param string $key
     */
    protected function guardAgainstUntranslatableAttribute($key)
    {
        if (!$this->isTranslatable($key)) {
            /** @noinspection PhpParamsInspection */
            throw AttributeIsNotTranslatableException::make($key, $this);
        }
    }

    /**
     * @param $attribute
     *
     * @return bool
     */
    public function isTranslated($attribute)
    {
        $transAttr = $attribute . $this->translatableFieldsSuffix;

        return $this->isTranslatable($attribute)
               && !empty($this->attributes[$transAttr]);
    }

    /**
     * Convert the model instance to an array.
     *
     * @return array
     *
     * @throws AttributeIsNotTranslatableException
     */
    public function toArray()
    {
        if (!$this->withAllTranslations){
            return parent::toArray();
        }

        $attributes = $this->attributesToArray();
        $attributes['translations'] = [];
        foreach ($this->getTranslatableAttributes() as $attribute) {
            $attributes[$attribute] = $this->getAttribute($attribute);
            $attributes['translations'][$attribute] = $this->getTranslations($attribute);
        }

        return array_merge($attributes, $this->relationsToArray());
    }

    /**
     * @param $attribute
     *
     * @return mixed
     */
    public function getRawAttribute($attribute)
    {
        return array_get($this->attributes, $attribute, '');
    }

    public function withAllTranslations($value)
    {
        $this->withAllTranslations = $value;

        return $this;
    }

    public function getTranslatableFieldType($attribute) {
        return array_get($this->translatableCasts, $attribute, 'text');
    }

}