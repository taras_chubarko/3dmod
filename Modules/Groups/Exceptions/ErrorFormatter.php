<?php

namespace Modules\Groups\Exceptions;

use GraphQL\Error\Error;
use M1naret\GraphQL\Error\ValidationError;
use M1naret\GraphQL\GraphQL;

/**
 * Class ErrorFormatter
 *
 * @package Modules\Groups\Exceptions
 */
class ErrorFormatter extends GraphQL
{

    /**
     * @param Error $e
     *
     * @return array
     */
    public static function formatError(Error $e) : array
    {
        $previous = $e->getPrevious();
        /** @var array $error */
        $error = [
            'code'    => $previous ? $previous->getCode() : $e->getCode(),
            'message' => $e->getMessage(),
        ];

        if ($previous && $previous instanceof ValidationError) {
            $error['validation'] = $previous->getValidatorMessages();
        }

        return $error;
    }

}