<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/24/2018
 * Time: 7:22 PM
 */

namespace Modules\Groups\Exceptions;

use Exception;
use Illuminate\Database\Eloquent\Model;


class AttributeIsNotTranslatableException extends Exception
{
    public static function make($key, Model $model)
    {
        return new static("Attribute `{$key}` is not the translatable attributes");
    }

}