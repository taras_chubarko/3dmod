<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/24/2018
 * Time: 6:58 PM
 */

namespace Modules\Groups\Mutations;

use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Upload\UploadType;
use M1naret\GraphQL\Support\Facades\GraphQL;
use M1naret\GraphQL\Support\Mutation;
use M1naret\GraphQL\Support\SelectFields;
use Modules\Filem\Repositories\FilemRepository;
use Modules\Filem\Repositories\FilemRepositoryEloquent;
use Modules\Groups\Entities\GroupMongo;
use Modules\Groups\Enums\GraphQL\GroupInvitesGraphQLEnum;
use Modules\Groups\Enums\GraphQL\GroupTypesGraphQLEnum;
use Modules\Groups\Types\GroupType;
use Modules\User\Entities\User;

class GroupsMutation extends Mutation
{
    /** @var string */
    const NAME = 'groups';

    /** @var array */
    protected $attributes = [
        'name' => self::NAME,
    ];

    /**
     * @return array
     */
    public function args() : array
    {
        return [
            [
                'name' => 'id',
                'type' => Type::int(),
            ],
            [
                'name' => 'invites',
                'type' => Type::listOf(Type::int()),
            ],
            [
                'name' => 'name',
                'type' => Type::nonNull(Type::string()),
            ],
            [
                'name' => 'description',
                'type' => Type::string(),
            ],
            [
                'name' => 'type',
                'type' => Type::nonNull(GraphQL::type(GroupTypesGraphQLEnum::NAME)),
            ],
            [
                'name' => 'invite_type',
                'type' => Type::nonNull(GraphQL::type(GroupInvitesGraphQLEnum::NAME)),
            ],
            [
                'name' => 'is_forum',
                'type' => Type::boolean(),
            ],
        ];
    }

    /**
     * @param array $args
     *
     * @return bool
     */
    public function authorize(array $args) : bool
    {
        return !empty(request()->user());
    }

    /**
     * @param array $args
     *
     * @return array
     */
    public function rules(array $args = []) : array
    {
        return [
            'name'        => 'required|string|min:5',
            'description' => 'sometimes|required|string|min:20',
            'type'        => 'sometimes|required|integer',
            'invite_type' => 'sometimes|required|integer',
            'is_forum'    => 'sometimes|required|boolean',
        ];
    }

    /**
     * @return ObjectType
     */
    public function type()
    {
        return GraphQL::type(GroupType::NAME);
    }

    /**
     * @param array $args
     *
     * @return array
     */
    public function prepareRequest(array $args) : array
    {
        /** @var User $user */
        $user = request()->user();

        $args['is_forum'] = array_get($args, 'is_forum', false);
        $args['user_id'] = $user->id;

        return $args;
    }

    /**
     * @param              $root
     * @param              $args
     * @param SelectFields $fields
     *
     * @return GroupMongo
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        /** @var GroupMongo $group */
        if ($id = array_get($args, 'id')) {
            $group = GroupMongo::query()->find($id);
        } else {
            $group = new GroupMongo;
        }

        if (!$group) {
            return null;
        }

        if ($image = request()->get('image')) {
            /** @var FilemRepositoryEloquent $fileRepository */
            $fileRepository = app(FilemRepository::class);

            $file = $fileRepository->make($image, 'groups/image', $args['user_id']);

            $args['image_id'] = $file->id;
        }
        $group->fill($args);

        $group->save();

        // TODO: нужно узнать как будет приходить с фрона

        return $group;
    }

}