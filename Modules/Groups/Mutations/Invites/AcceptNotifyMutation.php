<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/24/2018
 * Time: 6:58 PM
 */

namespace Modules\Groups\Mutations\Invites;

use GraphQL\Type\Definition\BooleanType;
use GraphQL\Type\Definition\Type;
use M1naret\GraphQL\Support\Mutation;
use M1naret\GraphQL\Support\SelectFields;
use Modules\Groups\Entities\GroupMongo;
use Modules\Groups\Enums\GroupInvitationEnum;
use Modules\Groups\Enums\GroupPrivacyEnum;
use Modules\Groups\Enums\NotifyTypeEnum;
use Modules\Groups\Enums\UserRolesEnum;
use Modules\Notifi\Entities\Notifi;
use Modules\Notifi\Repositories\NotifiRepositoryEloquent;
use Modules\User\Entities\User;

/**
 * Class AcceptNotifyMutation
 *
 * @package Modules\Groups\Mutations\Invites
 */
class AcceptNotifyMutation extends Mutation
{
    /** @var string */
    const NAME = 'accept_notify';

    /** @var array */
    protected $attributes = [
        'name' => self::NAME,
    ];

    /**
     * @return array
     */
    public function args() : array
    {
        return [
            [
                'name' => 'notify_id',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }

    /**
     * @param array $args
     *
     * @return bool
     */
    public function authorize(array $args) : bool
    {
        return !empty(request()->user());
    }

    /**
     * @param array $args
     *
     * @return array
     */
    public function rules(array $args = []) : array
    {
        return [
            'notify_id' => 'required|string',
        ];
    }

    /**
     * @return BooleanType
     */
    public function type()
    {
        return Type::boolean();
    }

    /**
     * @param              $root
     * @param              $args
     * @param SelectFields $fields
     *
     * @return boolean
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        /** @var User $user */
        $user = request()->user();

        $notifyId = array_get($args, 'notify_id');

        /** @var Notifi $notify */
        $notify = Notifi::query()
                        ->where('id', $notifyId)
                        ->where('to_user_id', $user->id)
                        ->where('status', '!=', NotifyTypeEnum::CANCELED)
                        ->first();

        if (!$notify) {
            throw new \RuntimeException('Приглашение не найдено');
        }

        switch ($notify->type) {
            case NotifyTypeEnum::INVITE_TYPE:
                $this->acceptInviteGroup($notify);
                break;
            case NotifyTypeEnum::REQUEST_TYPE:
                $this->acceptRequestGroup($notify, $user);
                break;
        }

        return true;
    }

    /**
     * @param Notifi $notify
     */
    public function acceptInviteGroup(Notifi $notify)
    {
        /** @var GroupMongo $group */
        $group = GroupMongo::query()->find($notify->request_id);

        if (!$group) {
            throw new \RuntimeException('Не найдена группа');
        }

        $group->members()->attach([$notify->to_user_id]);
    }

    /**
     * @param Notifi $notify
     * @param User   $user
     */
    public function acceptRequestGroup(Notifi $notify, User $user)
    {
        /** @var GroupMongo $group */
        $group = GroupMongo::query()->find($notify->request_id);

        if (!$group) {
            throw new \RuntimeException('Не найдена группа');
        }

        switch ($group->invite_type) {
            case GroupInvitationEnum::ADMINS:
                $checkPermission = $user->hasRole(UserRolesEnum::ADMIN);
                break;
            case GroupInvitationEnum::ADMINS_MODS:
                $checkPermission = $user->hasRole(UserRolesEnum::MODERATOR);
                break;
            default:
                $checkPermission = true;
                break;
        }

        if (!$checkPermission) {
            throw new \RuntimeException('Недостаточно прав');
        }

        $group->members()->attach([$notify->from_user_id]);
    }

}