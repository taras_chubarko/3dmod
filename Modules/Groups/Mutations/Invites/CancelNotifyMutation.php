<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/24/2018
 * Time: 6:58 PM
 */

namespace Modules\Groups\Mutations\Invites;

use GraphQL\Type\Definition\BooleanType;
use GraphQL\Type\Definition\Type;
use M1naret\GraphQL\Support\Mutation;
use M1naret\GraphQL\Support\SelectFields;
use Modules\Groups\Entities\GroupMongo;
use Modules\Groups\Enums\GroupInvitationEnum;
use Modules\Groups\Enums\GroupPrivacyEnum;
use Modules\Groups\Enums\NotifyTypeEnum;
use Modules\Groups\Enums\UserRolesEnum;
use Modules\Notifi\Entities\Notifi;
use Modules\Notifi\Repositories\NotifiRepositoryEloquent;
use Modules\User\Entities\User;

/**
 * Class CancelNotifyMutation
 *
 * @package Modules\Groups\Mutations\Invites
 */
class CancelNotifyMutation extends Mutation
{
    /** @var string */
    const NAME = 'cancel_notify';

    /** @var array */
    protected $attributes = [
        'name' => self::NAME,
    ];

    /**
     * @return array
     */
    public function args() : array
    {
        return [
            [
                'name' => 'notify_id',
                'type' => Type::nonNull(Type::string()),
            ],
        ];
    }

    /**
     * @param array $args
     *
     * @return bool
     */
    public function authorize(array $args) : bool
    {
        return !empty(request()->user());
    }

    /**
     * @param array $args
     *
     * @return array
     */
    public function rules(array $args = []) : array
    {
        return [
            'notify_id' => 'required|string',
        ];
    }

    /**
     * @return BooleanType
     */
    public function type()
    {
        return Type::boolean();
    }

    /**
     * @param              $root
     * @param              $args
     * @param SelectFields $fields
     *
     * @return boolean
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        /** @var User $user */
        $user = request()->user();

        $notifyId = array_get($args, 'notify_id');

        /** @var Notifi $notify */
        $notify = Notifi::query()
                        ->where('id', $notifyId)
                        ->where('to_user_id', $user->id)
                        ->where('status', '!=', NotifyTypeEnum::CANCELED)
                        ->first();

        if (!$notify) {
            throw new \RuntimeException('Приглашение не найдено');
        }

        $notify->status = NotifyTypeEnum::CANCELED;

        return $notify->save();
    }

}