<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/24/2018
 * Time: 6:58 PM
 */

namespace Modules\Groups\Mutations;

use GraphQL\Type\Definition\BooleanType;
use GraphQL\Type\Definition\Type;
use M1naret\GraphQL\Support\Mutation;
use M1naret\GraphQL\Support\SelectFields;
use Modules\Groups\Entities\GroupMongo;
use Modules\Groups\Enums\GroupInvitationEnum;
use Modules\Groups\Enums\GroupPrivacyEnum;
use Modules\Groups\Enums\NotifyTypeEnum;
use Modules\Groups\Enums\UserRolesEnum;
use Modules\Notifi\Repositories\NotifiRepositoryEloquent;
use Modules\User\Entities\User;

class SendInvitesMutation extends Mutation
{
    /** @var string */
    const NAME = 'send_invites';

    /** @var array */
    protected $attributes = [
        'name' => self::NAME,
    ];

    /**
     * @return array
     */
    public function args() : array
    {
        return [
            [
                'name' => 'group_id',
                'type' => Type::nonNull(Type::string()),
            ],
            [
                'name' => 'users',
                'type' => Type::listOf(Type::int()),
            ],
        ];
    }

    /**
     * @param array $args
     *
     * @return bool
     */
    public function authorize(array $args) : bool
    {
        return !empty(request()->user());
    }

    /**
     * @param array $args
     *
     * @return array
     */
    public function rules(array $args = []) : array
    {
        return [
            'group_id' => 'required|string',
            'users'    => 'required|array',
            'users.*'  => 'sometimes|required|integer',
        ];
    }

    /**
     * @return BooleanType
     */
    public function type()
    {
        return Type::boolean();
    }

    /**
     * @param              $root
     * @param              $args
     * @param SelectFields $fields
     *
     * @return boolean
     */
    public function resolve($root, $args, SelectFields $fields)
    {
        /** @var User $user */
        $user = request()->user();

        $groupId = array_get($args, 'group_id');

        /** @var GroupMongo $group */
        $group = GroupMongo::query()->find($groupId);

        if (!$group) {
            throw new \RuntimeException('Группа не найдена');
        }

        switch ($group->invite_type) {
            case GroupInvitationEnum::ADMINS:
                $checkPermission = $user->hasRole(UserRolesEnum::ADMIN);
                break;
            case GroupInvitationEnum::ADMINS_MODS:
                $checkPermission = $user->hasRole(UserRolesEnum::MODERATOR);
                break;
            default:
                $checkPermission = true;
                break;
        }

        if (!$checkPermission) {
            throw new \RuntimeException('Недостаточно прав для приглашения в группу');
        }

        /** @var NotifiRepositoryEloquent $notify */
        $notify = app(NotifiRepositoryEloquent::class);

        $notifyData = [
            'from_user_id' => $user->id,
            'type'         => NotifyTypeEnum::INVITE_TYPE,
            'message'      => $group->type === GroupPrivacyEnum::PUBLIC ? NotifyTypeEnum::NOTIFY :
                NotifyTypeEnum::REQUEST,
            'request_id'   => $groupId,
        ];

        if ($users = array_get($args, 'users')) {
            foreach ($users as $recipient) {
                $notifyData['to_user_id'] = $recipient;
                $notify->add($notifyData);
            }
        }

        return true;
    }

}