<?php

namespace Modules\Groups\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Modules\User\Entities\User;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class GroupMongo
 *
 * @property integer group_id
 * @property integer user_id
 * @package Modules\Groups\Entities
 */
class GroupMemberMongo extends Eloquent implements Transformable
{
    use TransformableTrait;

    /** @var string */
    protected $connection = 'mongodb';

    /** @var string */
    protected $collection = 'group_members';

    /** @var array  */
    protected $guarded = ['_id'];

    /** @var array */
    protected $dates = [
        'created_at',
    ];

    /** @var array */
    protected $fillable = [
        'user_id',
        'group_id',
    ];

    /**
     * @return BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(GroupMongo::class);
    }
}
