<?php

namespace Modules\Groups\Entities;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\DB;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
use Jenssegers\Mongodb\Relations\BelongsToMany;
use Modules\Filem\Entities\Filem;
use Modules\Groups\Traits\TranslatableTrait;
use Modules\User\Entities\User;
use MongoDB\Collection;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class GroupMongo
 *
 * @property User       owner
 * @property Collection members
 * @property string     name
 * @property string     description
 * @property integer    type
 * @property integer    invite_type
 * @property boolean    is_forum
 * @property integer    is_active
 * @property integer    image_id
 * @package Modules\Groups\Entities
 */
class GroupMongo extends Eloquent implements Transformable
{
    use TransformableTrait, Sluggable, SoftDeletes, TranslatableTrait;

    /** @var int */
    const ACTIVE = 1;
    /** @var int */
    const INACTIVE = 0;

    /** @var string */
    protected $connection = 'mongodb';

    /** @var string */
    protected $collection = 'groups';

    /** @var array */
    protected $guarded = ['_id'];

    /** @var array */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /** @var array */
    protected $fillable = [
        'name',
        'description',
        'type',
        'invite_type',
        'is_forum',
        'is_active',
        'user_id',
        'image_id',
    ];

    /** @var array */
    protected $translatable = [
        'name',
        'description',
    ];

    /**
     * @return array
     */
    public function sluggable() : array
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive(Builder $query)
    {
        return $query->where('is_active', self::ACTIVE);
    }

    /**
     * @return BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function image()
    {
        return $this->belongsTo(Filem::class, 'image_id');
    }

    /**
     * @param int $limit
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function members($limit = 10)
    {
        return $this->belongsToMany(User::class, 'group_members', 'group_id')->limit($limit);
    }

}
