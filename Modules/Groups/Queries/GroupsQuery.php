<?php

namespace Modules\Groups\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use M1naret\GraphQL\Support\Facades\GraphQL;
use M1naret\GraphQL\Support\PaginationType;
use M1naret\GraphQL\Support\Query;
use M1naret\GraphQL\Support\SelectFields;
use Modules\Groups\Entities\GroupMongo;
use Modules\Groups\Enums\GraphQL\ViewTypeGraphQLEnum;
use Modules\Groups\Enums\GroupPrivacyEnum;
use Modules\Groups\Enums\UserRolesEnum;
use Modules\Groups\Types\GroupType;
use Modules\User\Entities\User;

/**
 * Class GroupsQuery
 *
 * @package Modules\Groups\Queries
 */
class GroupsQuery extends Query
{
    const NAME = 'groups';

    protected $attributes = [
        'name' => self::NAME,
    ];

    /**
     * @return array
     */
    public function args() : array
    {
        return [
            'id'          => [
                'name' => 'id',
                'type' => Type::int(),
            ],
            'ids'         => [
                'name' => 'ids',
                'type' => Type::listOf(Type::int()),
            ],
            'slug'        => [
                'name' => 'slug',
                'type' => Type::string(),
            ],
            'is_active'   => [
                'name' => 'is_active',
                'type' => Type::boolean(),
            ],
            'invite_type' => [
                'type' => GraphQL::type(ViewTypeGraphQLEnum::NAME),
            ],
        ];
    }

    /**
     * @return PaginationType
     */
    public function type() : PaginationType
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return GraphQL::paginate(GroupType::NAME);
    }

    /**
     * @param              $root
     * @param              $args
     * @param SelectFields $selectFields
     * @param ResolveInfo  $resolveInfo
     *
     * @return LengthAwarePaginator
     *
     * @throws \InvalidArgumentException
     */
    public function resolve($root, $args, SelectFields $selectFields, ResolveInfo $resolveInfo) : LengthAwarePaginator
    {
        /** @var User $user */
        $user = request()->user();

        $adminRole = false;

        $user && $adminRole = $user->hasRole([UserRolesEnum::ADMIN, UserRolesEnum::MODERATOR]);

        /** @var Builder $query */
        $query = GroupMongo::query()
                           ->with($selectFields->getRelations());

        if ($id = array_get($args, 'id')) {
            $query->where('id', $id);
        }

        if ($slug = array_get($args, 'slug')) {
            $query->where('slug', $slug);
        }

        if (($isActive = array_get($args, 'is_active')) && !$adminRole) {
            $query->where('is_active', (int)$isActive);
        }

        if (\is_array($ids = array_get($args, 'ids')) && \count($ids)) {
            $query->whereIn('id', $ids);
        }

        if (!$adminRole) {
            $query->where('type', GroupPrivacyEnum::PUBLIC)
                  ->orWhere(function(Builder $query) use ($user) {
                      $query->whereHas('members', function(Builder $query) use ($user) {
                          $query->where('user_id', array_get($user, 'id'));
                      });
                  });
        }

        return $query->paginate(array_get($args, 'per_page'));
    }
}
