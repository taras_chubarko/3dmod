<?php

namespace Modules\Groups\Queries;

use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use M1naret\GraphQL\Support\Facades\GraphQL;
use M1naret\GraphQL\Support\PaginationType;
use M1naret\GraphQL\Support\Query;
use M1naret\GraphQL\Support\SelectFields;
use Modules\Groups\Entities\GroupMongo;
use Modules\Groups\Enums\GraphQL\InviteTypesGraphQLEnum;
use Modules\Groups\Enums\GroupPrivacyEnum;
use Modules\Groups\Enums\UserRolesEnum;
use Modules\Groups\Types\GroupType;
use Modules\Groups\Types\InviteType;
use Modules\Notifi\Entities\Notifi;
use Modules\User\Entities\User;

/**
 * Class InvitesQuery
 *
 * @package Modules\Groups\Queries
 */
class InvitesQuery extends Query
{
    const NAME = 'invites';

    protected $attributes = [
        'name' => self::NAME,
    ];

    /**
     * @return array
     */
    public function args() : array
    {
        return [
            'id' => [
              'type' => Type::int(),
            ],
            'ids' => [
                'type' => Type::listOf(Type::int()),
            ],
            'type' => [
                'type' => GraphQL::type(InviteTypesGraphQLEnum::NAME),
            ],
        ];
    }

    /**
     * @return PaginationType
     */
    public function type() : PaginationType
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return GraphQL::paginate(InviteType::NAME);
    }

    /**
     * @param              $root
     * @param              $args
     * @param SelectFields $selectFields
     * @param ResolveInfo  $resolveInfo
     *
     * @return LengthAwarePaginator
     *
     * @throws \InvalidArgumentException
     */
    public function resolve($root, $args, SelectFields $selectFields, ResolveInfo $resolveInfo) : LengthAwarePaginator
    {
        /** @var User $user */
        $user = request()->user();

        $adminRole = false;

        $user && $adminRole = $user->hasRole([UserRolesEnum::ADMIN]);

        /** @var Builder $query */
        $query = Notifi::query()
                           ->with($selectFields->getRelations());

        if ($id = array_get($args, 'id')) {
            $query->where('id', $id);
        }

        if ($type = array_get($args, 'type')) {
            $query->where('type', $type);
        }

        if ($ids = array_get($args, 'ids')) {
            $query->whereIn('id', $ids);
        }

        if (!$adminRole) {
            $query->where('to_user_id', $user->id);
        }

        return $query->paginate(array_get($args, 'per_page'));
    }
}
