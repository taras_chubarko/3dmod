<?php

namespace Modules\Groups\Queries;

use M1naret\GraphQL\Support\Facades\GraphQL;
use M1naret\GraphQL\Support\PaginationType;
use M1naret\GraphQL\Support\Query;
use M1naret\GraphQL\Support\SelectFields;
use Modules\Groups\Types\UserType;
use Modules\User\Entities\User;

/**
 * Class FriendsQuery
 *
 * @package Modules\Groups\Queries
 */
class FriendsQuery extends Query
{
    const NAME = 'friends';

    protected $attributes = [
        'name' => self::NAME,
    ];

    /**
     * @param array $args
     *
     * @return bool
     */
    public function authorize(array $args) : bool
    {
        return !empty(request()->user());
    }

    /**
     * @return PaginationType
     */
    public function type()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        return GraphQL::paginate(UserType::NAME);
    }

    /**
     * @param              $root
     * @param              $args
     * @param SelectFields $info
     *
     * @return mixed
     */
    public function resolve($root, $args, SelectFields $info)
    {
        /** @var User $user */
        $user = request()->user();

        return $user->getFriends(array_get($args, 'per_page', 10));
    }
}
