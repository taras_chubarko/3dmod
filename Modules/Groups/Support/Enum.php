<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 5/22/2018
 * Time: 9:54 PM
 */

namespace Modules\Groups\Support;

use Illuminate\Contracts\Logging\Log;

/**
 * Class Enum
 *
 * @package App\Support\Enums
 */
abstract class Enum
{
    /**
     * @var string
     */
    private $currentValue = '';

    /**
     * Enum constructor.
     *
     * @param $type
     *
     * @throws EnumException
     */
    final public function __construct($type)
    {
        if ($type === null) {
            $className = \get_class($this);

            $type = strtoupper($type);
            if (!$this::hasConst($type)) {
                throw new EnumException("Constant `$type` in enum $className not found");
            }

            $this->currentValue = \constant("{$className}::{$type}");
        }
    }

    final public function __toString() : string
    {
        /** @noinspection MagicMethodsValidityInspection */
        return $this->currentValue;
    }

    final protected static function isValidConstName($name) : bool
    {
        return self::hasConst($name);
    }

    final protected static function isValidConstValue($value) : bool
    {
        return self::hasConstValue($value);
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    final protected static function hasConst($name) : bool
    {
        return \defined(static::class . '::' . mb_strtoupper($name));
    }

    /**
     * @param $value
     *
     * @return bool
     */
    final protected static function hasConstValue($value) : bool
    {
        return \in_array($value, self::getConstants(), true);
    }

    /**
     * Get all constant from self class (use ReflectionClass access)
     *
     * @return array
     *
     */
    final public static function getConstants() : array
    {
        try {
            $oClass = new \ReflectionClass(static::class);
        } catch (\ReflectionException $exception) {
            Log::critical($exception->getMessage());

            return [];
        }

        return $oClass->getConstants();
    }

    /**
     * Get all integer constant values
     *
     * @return int[]
     */
    final protected static function getConstantNames() : array
    {
        return array_keys(self::getConstants());
    }

    /**
     * Get all names of constants
     *
     * @return string[]
     */
    final protected static function getConstantValues() : array
    {
        $constants = self::getConstants();

        return array_values($constants);
    }

    /**
     * @param $name
     *
     * @return mixed
     *
     * @throws EnumException
     */
    final public static function getConstantValue($name)
    {
        if (!self::hasConst($name)) {
            throw new EnumException('Enum constant ' . static::class . '::' . $name . ' - not found.');
        }

        return \constant(static::class . '::' . mb_strtoupper($name));
    }

    /**
     * @param $value
     *
     * @return string
     *
     * @throws EnumException
     */
    final protected static function getConstantName($value) : string
    {
        if (!self::hasConstValue($value)) {
            throw new EnumException("Enum does not have constant with value equal `$value`");
        }

        return array_get(array_flip(self::getConstants()), $value);
    }
}
