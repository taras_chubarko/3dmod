require('./bootstrap');

window.Vue = require('vue');

import { EventBus } from './event-bus';
Vue.prototype.event = EventBus;

import Vuetify from 'vuetify';
Vue.use(Vuetify, {
    theme: {
        primary: '#009688',
        secondary: '#b0bec5',
    }
});

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueSession from 'vue-session';
Vue.use(VueSession);

import tinymce from 'vue-tinymce-editor';
Vue.component('tinymce', tinymce);


//Vue.component('select-filters', require('./components/admin/SelectFilters.vue'));
// Vue.component('menu-left', require('./menu/MenuLeft.vue'));
// Vue.component('block-user', require('./blocks/BlockUser.vue'));
// Vue.component('modal', require('./modals/Modal.vue'));
Vue.component('main-section', require('./pages/Main.vue'));
/*
 * Router
 */
const router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'active',
    linkExactActiveClass: 'active',
    routes: require('./routesAdmin'),
});

/*
 * App
 */
const app = new Vue({
    el: '#app',
    router:router,
});