module.exports = [
    {
        path: '/admin',
        name: 'admin',
        component: Vue.extend(require('./pages/PageMain.vue')),
    },
    {
        path: '/admin/products/tariff',
        name: 'admin.products.tariff',
        component: Vue.extend(require('./products/PageProductTariff.vue')),
    },
    {
        path: '/admin/products/reklama',
        name: 'admin.products.reklama',
        component: Vue.extend(require('./products/PageProductReklama.vue')),
    },
    {
        path: '/admin/taxonomy',
        name: 'admin.taxonomy',
        component: Vue.extend(require('./taxonomy/PageTaxonomy.vue')),
    },
    {
        path: '/admin/taxonomy/:id',
        name: 'admin.taxonomy.id',
        component: Vue.extend(require('./taxonomy/PageTaxonomyTerm.vue')),
    },
    {
        path: '/admin/catalog',
        name: 'admin.catalog',
        component: Vue.extend(require('./catalog/PageCatalog.vue')),
    },
    {
        path: '/admin/catalog/:slug',
        name: 'admin.catalog.slug',
        component: Vue.extend(require('./catalog/PageCatalogItems.vue')),
    },
    {
        path: '/admin/catalog/:slug/:slug2',
        name: 'admin.catalog.slug2',
        component: Vue.extend(require('./catalog/PageCatalogChildItems.vue')),
    },
    // {
    //     path: '/*',
    //     component: Vue.extend(require('./components/pages/Page404.vue')),
    // },
]