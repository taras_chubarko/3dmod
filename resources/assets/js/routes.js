module.exports = [
    {
        path: '/',
        component: Vue.extend(require('./components/pages/Main.vue')),
    },
    {
        path: '/login',
        component: Vue.extend(require('./components/pages/Login.vue')),
        meta: {
            forGuests: true
        }
    },
    {
        path: '/sw/:id',
        component: Vue.extend(require('./components/user/PageUserSW.vue')),
    },
    /*
     * Профіль користувача для всіх
     */
    {
        path: '/user/:slug',
        name: 'user',
        component: Vue.extend(require('./components/user/PageUser.vue')),
    },
    /*
     * Профіль Активність
     */
    {
        path: '/profile/activity',
        component: Vue.extend(require('./components/user/profile/PageProfile.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/activity/all-notifications',
        component: Vue.extend(require('./components/user/notify/PageAllNotifications.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/activity/friends', 
        component: Vue.extend(require('./components/user/activity/PageActivityOfFriends.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/activity/groups',
        component: Vue.extend(require('./components/user/activity/PageGroupsActivity.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/activity/goods',
        component: Vue.extend(require('./components/user/activity/PageActivityGoods.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/activity/gallery',
        component: Vue.extend(require('./components/user/activity/PageActivityGallery.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/activity/work',
        component: Vue.extend(require('./components/user/activity/PageActivityWork.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/activity/pages',
        component: Vue.extend(require('./components/user/activity/PageActivityPages.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/activity/themes',
        component: Vue.extend(require('./components/user/activity/PageActivityThemes.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/activity/lessons',
        component: Vue.extend(require('./components/user/activity/PageActivityLessons.vue')),
        meta: {
            forUser: true
        }
    },
    /*
     * Профіль Налаштування
     */
    {
        path: '/profile/settings',
        component: Vue.extend(require('./components/user/settings/PageNalashtuvannya.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/settings/notification',
        component: Vue.extend(require('./components/user/settings/PageNalashtuvannyaNotification.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/settings/access',
        component: Vue.extend(require('./components/user/settings/PageNalashtuvannyaAccess.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/settings/finances',
        component: Vue.extend(require('./components/user/settings/PageNalashtuvannyaFinances.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/settings/finances/shop',
        component: Vue.extend(require('./components/user/settings/PageNalashtuvannyaFinancesShop.vue')),
        meta: {
            forUser: true
        }
    },
    /*
     * Друзі
     */
    {
        path: '/profile/friends',
        component: Vue.extend(require('./components/user/friends/PageDruzi.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/friends/online',
        component: Vue.extend(require('./components/user/friends/PageDruziVmerezi.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/friends/request',
        name: 'friends.request',
        component: Vue.extend(require('./components/user/friends/PageDruziZapyt.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/friends/add',
        component: Vue.extend(require('./components/user/friends/PageDruziAdd.vue')),
        meta: {
            forUser: true
        }
    },


    {
        path: '/profile/message',
        name: 'message',
        component: Vue.extend(require('./components/user/messages/PagePovidomlennya.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/message/:id',
        name: 'message.id',
        component: Vue.extend(require('./components/user/messages/PagePovidomlennya.vue')),
        meta: {
            forUser: true
        }
    },

    {
        path: '/profile/gallery',
        component: Vue.extend(require('./components/user/PageHalereya.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/products',
        component: Vue.extend(require('./components/user/products/PageProdukty.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/sale-buy',
        component: Vue.extend(require('./components/user/PageProdazhPokupka.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/bookmarks',
        component: Vue.extend(require('./components/user/PageZakladky.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/tariff-plan',
        component: Vue.extend(require('./components/user/PageTaryfnyyPlan.vue')),
        meta: {
            forUser: true
        }
    },

    {
        path: '/products/create',
        name: 'products.create',
        component: Vue.extend(require('./components/products/ProductStep0.vue')),
        meta: {
            forUser: true,
        }
    },
    {
        path: '/products/create/step-1',
        name: 'products.create.step.1',
        component: Vue.extend(require('./components/products/ProductStep1.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/products/create/step-2',
        name: 'products.create.step.2',
        component: Vue.extend(require('./components/products/ProductStep2.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/products/create/step-3',
        name: 'products.create.step.3',
        component: Vue.extend(require('./components/products/ProductStep3.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/products/create/step-4',
        name: 'products.create.step.4',
        component: Vue.extend(require('./components/products/ProductStep4.vue')),
        meta: {
            forUser: true
        }
    },
    {
        path: '/products/create/success',
        name: 'products.create.success',
        component: Vue.extend(require('./components/products/ProductStepSuccess.vue')),
        meta: {
            forUser: true
        }
    },

    {
        path: '/product/:slug',
        name: 'product.slug',
        component: Vue.extend(require('./components/products/PageProductView1.vue')),
        children:[
            {
                path: ':tab',
                name: 'product.slug.tab',
                component: Vue.extend(require('./components/products/PageProductView1.vue')),
            }
        ]
    },


    {
        path: '/test/upload',
        component: Vue.extend(require('./components/pages/TestUpload.vue')),
    },
    {
        path: '/icons',
        component: Vue.extend(require('./components/pages/PageIcons.vue')),
    },
    {
        path: '/*',
        component: Vue.extend(require('./components/pages/Page404.vue')),
    },
]