/*
 *
 */
Vue.filter('username', function(username) {
    if (username.last_name)
    {
        return username.first_name+' '+username.last_name;
    }
    else
    {
        return username.first_name
    }
});
/*
 *
 */
Vue.filter('striphtml', function (value) {
    var div = document.createElement("div");
    div.innerHTML = value;
    var text = div.textContent || div.innerText || "";
    return text;
});
