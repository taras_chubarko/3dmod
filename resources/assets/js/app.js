
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.jsonp = require('jsonp');
window.geocomplete = require("geocomplete");
window.nicescroll = require("nicescroll");
window.WaveSurfer = require("wavesurfer.js");

import VueRouter from 'vue-router';
import VueMask from 'v-mask';
import VueScrollTo from 'vue-scrollto';
import VueSweetAlert from 'vue-sweetalert';
import VueProgressBar from 'vue-progressbar';
import VN  from 'vue-notification';
import Msg from './packages/validate/Msg.js';
import Auth from './packages/auth/Auth.js';
import VueI18n from 'vue-i18n';


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*
 * use
 */
Vue.use(VueRouter);
Vue.use(VueMask);


Vue.use(VueScrollTo);
Vue.use(VueSweetAlert);
Vue.use(VN);
Vue.use(Msg);
Vue.use(VueProgressBar, {
    color: '#34A853',
    failedColor: '#EA4335',
    thickness: '5px'
});
Vue.use(Auth);
Vue.use(VueI18n);

import AnimatedVue from 'animated-vue';
Vue.use(AnimatedVue);

import VueHead from 'vue-head';
Vue.use(VueHead);


import {VueMasonryPlugin} from 'vue-masonry';
Vue.use(VueMasonryPlugin);

import ValidForm from './packages/validate/ValidForm.js';
Vue.use(ValidForm);

import VueTT from 'vue-directive-tooltip';
Vue.use(VueTT);

const MetaCtrlEnter = require('meta-ctrl-enter');
Vue.use(MetaCtrlEnter);

import Storage from 'vue-web-storage';
Vue.use(Storage);

import VueSession from 'vue-session';
Vue.use(VueSession);

import LoadScript from 'vue-plugin-load-script';
Vue.use(LoadScript);

import VueLazyload from 'vue-lazyload';
Vue.use(VueLazyload, {
    preLoad: 0,
    error: '/uploads/default/no-image-available2.jpg',
    loading: '/uploads/default/setting.gif',
    attempt: 1,
    // the default is ['scroll', 'wheel', 'mousewheel', 'resize', 'animationend', 'transitionend']
    listenEvents: [ 'scroll' ]
});

import VueImg from 'v-img';
Vue.use(VueImg);

import VueGeolocation from 'vue-browser-geolocation';
Vue.use(VueGeolocation);

import Geocoder from "@pderas/vue2-geocoder";
Vue.use(Geocoder, {
    googleMapsApiKey: 'AIzaSyA-D9ab5aN_5JZGP05u5izNtgvS0c-gezI',
});

import VueSelect from './packages/vue-select';
Vue.use(VueSelect);

import VueCarousel from 'vue-carousel';
Vue.use(VueCarousel);

import { ApolloClient } from 'apollo-client';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import VueApollo from 'vue-apollo';
import { setContext } from 'apollo-link-context';
import { createUploadLink } from 'apollo-upload-client';

const authLink = setContext((_, { headers }) => {
    const token = 'f918582d46fac49c0da249f978206bbb5a52b068';
    return {
        headers: {
            'x-authorization': token,
            'Content-Type': 'application/json'
        }
    }
});

const httpLink = new HttpLink({
    // URL to graphql server, you should use an absolute URL here
    uri: '/graphql',
    headers: {
        'x-authorization': 'f918582d46fac49c0da249f978206bbb5a52b068',
        //'Content-Type': 'multipart/form-data'
        //'Content-Type': 'application/json'
    }
});

const linkUpload = createUploadLink({
    uri: '/graphql',
    headers: {
        'x-authorization': 'f918582d46fac49c0da249f978206bbb5a52b068',
        //'Content-Type': 'application/json'
    }
});
// create the apollo client
const apolloClient = new ApolloClient({
    //link: authLink.concat(httpLink),
    link: linkUpload,
    cache: new InMemoryCache()
});
// install the vue plugin
Vue.use(VueApollo);

const apolloProvider = new VueApollo({
    defaultClient: apolloClient
});

/*
 * Components
 */
Vue.component('header-top', require('./components/layouts/HeaderTop.vue'));
Vue.component('header-center', require('./components/layouts/HeaderCenter.vue'));
Vue.component('header-catalog', require('./components/layouts/HeaderCatalog2.vue'));
Vue.component('footers', require('./components/layouts/Footer.vue'));
Vue.component('loading', require('./components/layouts/Loading.vue'));
Vue.component('modal', require('./components/layouts/Modal.vue'));
//Blocks
Vue.component('change-lang', require('./components/blocks/ChangeLang.vue'));
Vue.component('block-find', require('./components/blocks/Find.vue'));
Vue.component('block-find-with-photo', require('./components/blocks/FindWithPhoto.vue'));
Vue.component('block-top', require('./components/blocks/BlockTop.vue'));
Vue.component('block-new', require('./components/blocks/BlockNew.vue'));
Vue.component('block-group-week', require('./components/blocks/BlockGroupWeek.vue'));
Vue.component('block-gall', require('./components/blocks/BlockGall.vue'));
Vue.component('block-work', require('./components/blocks/BlockWork.vue'));
Vue.component('block-login', require('./components/blocks/BlockLogin.vue'));
Vue.component('block-user', require('./components/blocks/BlockUser.vue'));
Vue.component('block-mess', require('./components/blocks/BlockMess.vue'));
Vue.component('block-notify', require('./components/blocks/BlockNotify.vue'));
Vue.component('block-cart', require('./components/blocks/BlockCart.vue'));
Vue.component('block-string', require('./components/blocks/BlockString.vue'));
Vue.component('block-profile-my', require('./components/user/profile/BlockProfileMy.vue'));
Vue.component('menu-profile-my', require('./components/user/profile/MenuProfile.vue'));
Vue.component('menu-aktyvnist', require('./components/user/activity/MenuAktyvnist.vue'));
Vue.component('menu-settings', require('./components/user/settings/MenuSettings.vue'));
Vue.component('field-select-multiple', require('./components/fields/FieldSelectMultiple.vue'));
Vue.component('field-select-multiple2', require('./components/fields/FieldSelectMultiple2.vue'));
Vue.component('field-select-multiple3', require('./components/fields/FieldSelectMultiple3.vue'));
Vue.component('field-select-one', require('./components/fields/FieldSelectOne.vue'));
Vue.component('field-select-one2', require('./components/fields/FieldSelectOne2.vue'));
Vue.component('field-address', require('./components/fields/FieldAddress.vue'));
Vue.component('field-vid-diyalnosti', require('./components/fields/FieldVidDiyalnosti.vue'));
Vue.component('field-birthday-day', require('./components/fields/FieldBirthdayDay.vue'));
Vue.component('block-profile-user', require('./components/user/profile/BlockProfileUser.vue'));
//
// user profile
/*
 * Router
 */
const router = new VueRouter({
    mode: 'history',
    routes: require('./routes'),
    linkExactActiveClass: 'active',
});

import { EventBus } from './components/event-bus';

Vue.prototype.event = EventBus;

Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
};


router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.forGuests)) {
        if (Vue.auth.isAuth()) {
            next({
                path: '/'
            });
        }
        else
        {
            next();
        }
    }
    else if (to.matched.some(record => record.meta.forOrder)) {
        var order = localStorage.getItem('order');
        if (order) {
            next();
        }
        else
        {
            next({
                path: '/'
            });
        }
    }
    else if (to.matched.some(record => record.meta.forUser)) {
        if (!Vue.auth.isAuth()) {
            next({
                path: '/'
            });
        }
        else
        {
            next();
        }
    }
    else
    {
        next();
    }
});
Vue.filter('newstring', function (value) {
    return value.replace(/(?:\r\n|\r|\n)/g, '<br />');
});

import ens from './locales/en.json';
import rus from './locales/ru.json';
import uks from './locales/uk.json';

var messages = {
    'uk' : uks,
    'ru' : rus,
    'en' : ens,
}


const i18n = new VueI18n({
    locale: (localStorage.getItem('currentLang')) ? localStorage.getItem('currentLang') : 'uk', // set locale
    fallbackLocale: 'en',
    messages, // set locale messages
});

Vue.prototype.$locale = {
    change (lang) {
        i18n.locale = lang;
    },
    current() {
        return i18n.locale;
    }
}

import Validate from './packages/validate/Validate.js';
Vue.use(Validate, {
    i18n: i18n,
    currentLang: i18n.locale,
    EventBus: EventBus,
});

import months from './components/data/month';

Vue.prototype.$bithday = {
    days () {
        var day = [];
        for (var i = 1; i < 32; i++) {
            day.push({id: i, name: {en: i, ru: i, uk: i}});
        }
        return day;
    },
    months()
    {
        return months;
    },
    years()
    {
        var startYear = 1950;
        var currentYear = new Date().getFullYear(), years = [];
        startYear = startYear || 1980;
        while ( startYear <= currentYear ) {
            years.push({id: startYear, name: {en: startYear, ru: startYear, uk: startYear}});
            startYear++;
        }
        return years;
    }
}

Vue.prototype.$dosvid = {
    items()
    {
        var dosvid = [];
        for (var i = 1; i < 11; i++) {
            dosvid.push({id: i, name: {en: i, ru: i, uk: i}});
        }
        return dosvid;
    }
}

Vue.prototype.$slug = {
    set(text)
    {
        const a = 'àáäâèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;'
        const b = 'aaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh------'
        const p = new RegExp(a.split('').join('|'), 'g')

        return text.toString().toLowerCase()
            .replace(/\s+/g, '-')           // Replace spaces with -
            .replace(p, c => b.charAt(a.indexOf(c)))     // Replace special chars
            .replace(/&/g, '-and-')         // Replace & with 'and'
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '')             // Trim - from end of text
    }
}

Vue.prototype.$query = {
    encode(data)
    {
        let ret = [];
        for (let d in data)
            ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
        return ret.join('&');
    }
};

Vue.prototype.$trans = {
    t(data)
    {
        let item = '';

        item = i18n.t(data);

        EventBus.$on('lang', function (data) {
            item = i18n.t(data);
        });

        return item;
    }
};

/*
 * filters
 */
Vue.filter('Vidhuk', function(val) {
    if (val == 0) {
        return '<span>'+val+'</span> '+i18n.t('Vidhukiv');
    }
    else if (val == 1) {
        return '<span>'+val+'</span> '+i18n.t('Vidhuk');
    }
    else if (val == 2 || val == 3 || val == 4) {
        return '<span>'+val+'</span> '+i18n.t('Vidhuky');
    }
    else if (val >= 5) {
        return '<span>'+val+'</span> '+i18n.t('Vidhukiv');
    }
    else
    {
        return '<span>'+val+'</span> '+i18n.t('Vidhukiv');
    }
});

require('./filters');

import VueEcho from 'vue-echo';
Vue.use(VueEcho, {
    broadcaster: 'socket.io',
    host: 'wss://clever.in.ua:6001'
});

import VTooltip from 'v-tooltip';
Vue.use(VTooltip);



Array.prototype.fieldAddress = function(obj) {
    var item = null;
    _.forEach(this, function (v) {
        if(v.types.contains(obj))
        {
            item = v.long_name;
        }
    });
    return item;
};

Vue.prototype.$sng = [
    'Россия', 'Украина', 'Белоруссия', 'Казахстан', 'Армения', 'Киргизия', 'Молдавия', 'Таджикистан',
    'Russia', 'Ukraine', 'Byelorussia', 'Kazakhstan', 'Armenia', 'Kyrgyzstan', 'Moldavia', 'Tajikistan',
    'Росія', 'Україна', 'Білорусія', 'Казахстан', 'Вірменія', 'Киргизія', 'Молдавія', 'Таджикистан'
];


/*
 * App
 */
const app = new Vue({
    el: '#app',
    apolloProvider,
    router:router,
    i18n: i18n,
    data: {
        sitename: '3D Models',
        currentBlock: 'block-find',
        headerClass: 'front-header',
        bg: '',
    },
    // beforeRouteEnter (to, from, next) {
    //     next(vm => vm.getGeo());
    // },
    methods:{
        getGeo: function () {
            this.$getLocation().then(coordinates => {
                var latLngObj = {
                    lat: coordinates.lat,
                    lng: coordinates.lng,
                };
                //
                this.setGeo(latLngObj);
            }).catch(error => {
                axios.post('/api/v1/get/geo').then(response => {
                    this.setGeo(response.data);
                });
            });
        },
        setGeo: function (latLngObj) {
            this.$geocoder.setDefaultMode('lat-lng');    // this is default
            this.$geocoder.send(latLngObj, response => {
                var address = response.results[0];
                var location = {
                    address: address.formatted_address,
                    address_short: address.address_components.fieldAddress('country')+', '+address.address_components.fieldAddress('locality'),
                    locality: address.address_components.fieldAddress('locality'),
                    country: address.address_components.fieldAddress('country'),
                    geometry: address.geometry.location,
                    world: this.$sng.contains(address.address_components.fieldAddress('country')) ? 'sng' : 'world',
                };
                this.$session.set('location', location);
            });
        }
    },
    mounted() {
        //localStorage.setItem('currentLang', 'укр');
        var lng = localStorage.getItem('currentLang');
        if (!lng) {
            localStorage.setItem('currentLang', 'uk');
        }
        this.$auth.whatch();

        var timeInMs = Date.now();

        if(this.$auth.isAuth())
        {
            if(this.headerClass !== 'front-header')
            {
                this.bg = "url(/profile/bg/" + this.$auth.getUser().id + "?"+timeInMs+")";
            }
        }
        this.getGeo();
        //



    }
});


