import gql from 'graphql-tag';

export const UPLOAD_IMAGE = gql`mutation($file:file_upload!){AllCgStorageUpload(file:$file)}`;
//export const UPLOAD_IMAGE = gql`mutation {AllCgStorageUpload(file:file_upload)}`;
// export const UPLOAD_IMAGE = gql`mutation {
//   groups(name: "Group 10", type: public, invite_type: all_members ) {
//     name
//     description
//     type
//     invite_type
//     is_forum
//   }
// }`;