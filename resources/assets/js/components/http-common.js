//import axios from 'axios';

export const HTTP = axios.create({
    baseURL: 'https://api.vipabonent.ru/v1',
    headers: {
        'Authorization':'387d8289-68d2-41ff-afb2-941f451c7778',
        'Accept': 'application/json',
	'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept',
        'crossDomain': true,
    },
    mode: 'no-cors',
    responseType: 'json',
})