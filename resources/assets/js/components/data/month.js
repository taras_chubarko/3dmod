export default {
    0: {
        id: 1,
        name: {
            en: 'january',
            ru: 'январь',
            uk: 'січень',
        }
    },
    1: {
        id: 2,
        name: {
            en: 'february',
            ru: 'февраль',
            uk: 'лютий',
        }
    },
    2: {
        id: 3,
        name: {
            en: 'march',
            ru: 'март',
            uk: 'березень',
        }
    },
    3: {
        id: 4,
        name: {
            en: 'april',
            ru: 'апрель',
            uk: 'квітень',
        }
    },
    4: {
        id: 5,
        name: {
            en: 'may',
            ru: 'май',
            uk: 'травень',
        }
    },
    5: {
        id: 6,
        name: {
            en: 'june',
            ru: 'июнь',
            uk: 'червень',
        }
    },
    6: {
        id: 7,
        name: {
            en: 'july',
            ru: 'июль',
            uk: 'липень',
        }
    },
    7: {
        id: 8,
        name: {
            en: 'august',
            ru: 'август',
            uk: 'серпень',
        }
    },
    8: {
        id: 9,
        name: {
            en: 'september',
            ru: 'серпень',
            uk: 'вересень',
        }
    },
    9: {
        id: 10,
        name: {
            en: 'october',
            ru: 'октябрь',
            uk: 'жовтень',
        }
    },
    10: {
        id: 11,
        name: {
            en: 'november',
            ru: 'ноябрь',
            uk: 'листопад',
        }
    },
    11: {
        id: 12,
        name: {
            en: 'december',
            ru: 'декабрь',
            uk: 'грудень',
        }
    }
}

//Січень Лютий Березень Квітень Травень Червень Липень Серпень Вересень Жовтень Листопад Грудень
//січень лютий березень квітень травень червень липень серпень вересень жовтень листопад грудень
//январь февраль март апрель май июнь июль август сентябрь октябрь ноябрь декабрь
//january february march april may june july august september october november december