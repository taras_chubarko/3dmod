import { EventBus } from '../../components/event-bus.js';

export default function (Vue)
{
    var fields = [];
    var errors = [];

    var locale = {
        uk:{
            req: "Поле :field обов'язково для заповнення.",
            min: "Поле :field має бути не менше :characters символів.",
            email: "Поле :field має містити @ та крапку.",
            confirm: "Поля паролей не співпадають.",
            my: "Поле :field вказано не вірно."
        },
        en:{
            req: "Field :field is required.",
            min: "Field: field must be at least :characters characters.",
            email: "Field :field should contain @ and a dot.",
            confirm: "Password fields do not match.",
            my: "Field :field is indicated not right."
        },
        ru:{
            req: "Поле :field обязательно для заполнения.",
            min: "Поле :field должно быть не менее :characters символов.",
            email: "Поле :field должно содержать @  и точку.",
            confirm: "Поля паролей не совпадают.",
            my: "Поле :field указано не верно."
        },
    };

    var currentLang = (localStorage.getItem('currentLang')) ? localStorage.getItem('currentLang') : 'ru';

    EventBus.$on('lang', function (lang) {
        currentLang = lang;
    });


    Vue.validform = {
        validate(e, scope, validate)
        {
            var form = $(e.target);//$('#'+scope);

            //console.log(form);
            errors = [];
            var err = [];

            $.each(validate.rules, function (k,v) {
                //console.log(validate.messages[k]);
                //console.log(v);
                if(v == 'required')
                {
                    if(form.find('[name="'+k+'"]').val() == '')
                    {
                        $('[name="'+k+'"]').addClass('has-error').parent().addClass('has-error');

                        if(validate.messages && validate.messages[k] !== undefined)
                        {
                            err.push(validate.messages[k]);
                        }
                        else
                        {
                            err.push('Поле '+k+' обязательно для заполнения.');
                        }
                    }
                    errors[scope] = err;
                }

                if(v.required)
                {
                    if(form.find('[name="'+k+'"]').val() == '' || form.find('[name="'+k+'"]').val() == '<p><br></p>')
                    {
                        $('[name="'+k+'"]').addClass('has-error').parent().addClass('has-error');
                        if(validate.messages && validate.messages[k] !== undefined)
                        {
                            err.push(validate.messages[k]);
                        }
                        else
                        {
                            err.push(trans(v.label, 'req'));
                        }
                    }
                    errors[scope] = err;
                }

                if(v.min)
                {
                    if(form.find('[name="'+k+'"]').val() !== '' && form.find('[name="'+k+'"]').val().length < v.min)
                    {
                        $('[name="'+k+'"]').addClass('has-error').parent().addClass('has-error');
                        if(validate.messages && validate.messages[k] !== undefined)
                        {
                            err.push(validate.messages[k]);
                        }
                        else
                        {
                            err.push(trans(v.label, 'min', v.min));
                        }
                    }
                    errors[scope] = err;
                }

                if(v.email)
                {
                    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

                    if(form.find('[name="'+k+'"]').val() !== '' && !pattern.test(form.find('[name="'+k+'"]').val()))
                    {
                        $('[name="'+k+'"]').addClass('has-error').parent().addClass('has-error');
                        if(validate.messages && validate.messages[k] !== undefined)
                        {
                            err.push(validate.messages[k]);
                        }
                        else
                        {
                            err.push(trans(v.label, 'email'));
                        }
                    }
                    errors[scope] = err;
                }

                if(v.radio)
                {
                    var ratio = form.find('[name="'+k+'"]:checked').length;
                    if(ratio == 0)
                    {
                        $('[name="'+k+'"]').addClass('has-error').parent().addClass('has-error');
                        if(validate.messages && validate.messages[k] !== undefined)
                        {
                            err.push(validate.messages[k]);
                        }
                        else
                        {
                            err.push(trans(v.label, 'req'));
                        }
                    }
                    else
                    {
                        $('[name="'+k+'"]').removeClass('has-error').parent().removeClass('has-error');
                    }
                    errors[scope] = err;
                }

                if(v.checkbox)
                {
                    var checkbox = form.find('[name="'+k+'"]:checked').length;
                    if(checkbox == 0)
                    {
                        $('[name="'+k+'"]').addClass('has-error').parent().addClass('has-error');
                        if(validate.messages && validate.messages[k] !== undefined)
                        {
                            err.push(validate.messages[k]);
                        }
                        else
                        {
                            err.push(trans(v.label, 'req'));
                        }
                    }
                    else
                    {
                        $('[name="'+k+'"]').removeClass('has-error').parent().removeClass('has-error');
                    }
                    errors[scope] = err;
                }

                if(v.confirm)
                {
                    if(form.find('[name="'+k+'"]').val() != '' && form.find('[name="'+v.confirm+'"]').val() != '')
                    {
                        if(form.find('[name="'+k+'"]').val() !== form.find('[name="'+v.confirm+'"]').val())
                        {
                            $('[name="'+k+'"]').addClass('has-error').parent().addClass('has-error');
                            if(validate.messages && validate.messages[k] !== undefined)
                            {
                                err.push(validate.messages[k]);
                            }
                            else
                            {
                                err.push(trans(v.label, 'confirm'));
                            }
                        }
                    }
                    errors[scope] = err;
                }

                if(v.required_if)
                {
                    //console.log(v.required_if);
                    if(v.required_if.checkbox)
                    {
                        var checkbox = form.find('[name="'+v.required_if.field+'"]:checked').length;
                        //console.log(checkbox);

                        if(checkbox == 0)
                        {
                            $('[name="'+k+'"]').addClass('has-error').parent().addClass('has-error');
                            if(validate.messages && validate.messages[k] !== undefined)
                            {
                                err.push(validate.messages[k]);
                            }
                            else
                            {
                                err.push(trans(v.label, 'req'));
                            }
                        }
                        else
                        {
                            $('[name="'+k+'"]').removeClass('has-error').parent().removeClass('has-error');
                        }
                    }
                }

                if(v.my)
                {
                    var vl = form.find('[name="'+k+'"]').val().split('/');
                    var m = ['01','02','03','04','05','06','07','08','09','10','11','12'];
                    var y = new Date().getFullYear().toString().substr(-2);
                    //y = y-2;


                    if(!_.includes(m, vl[0]) || vl[1] >y    )
                    {
                        $('[name="'+k+'"]').addClass('has-error').parent().addClass('has-error');
                        if(validate.messages && validate.messages[k] !== undefined)
                        {
                            err.push(validate.messages[k]);
                        }
                        else
                        {
                            err.push(trans(v.label, 'my'));
                        }
                    }

                }

            });

            if(errors[scope].length > 0)
            {
                return false;
            }
            else{
                $('.has-error').removeClass('has-error');
                return true;
            }


        },
        getErrors(scope) {
            $('.has-error').on('click', function () {
                $(this).removeClass('has-error');
            });
            return errors[scope];
        }
    };

    function trans(field, type, characters = null) {
        var f = '"'+field+'"';
        if(characters)
        {
            return locale[currentLang][type].replace(/:field/, f).replace(/:characters/, characters);
        }
        else
        {
            return locale[currentLang][type].replace(/:field/, f);
        }

    }


    Object.defineProperties(Vue.prototype, {
        $validform: {
            get() {
                return Vue.validform
            }
        }
    });
}