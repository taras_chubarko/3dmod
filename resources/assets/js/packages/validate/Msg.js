import { EventBus } from '../../components/event-bus.js';

export default function (Vue)
{
    var currentLang = (localStorage.getItem('currentLang')) ? localStorage.getItem('currentLang') : 'ru';

    EventBus.$on('lang', function (lang) {
        currentLang = lang;
    });

    Vue.msg = {
        setErrors(errors, notify, title) {
            var arr = [];
            $.each(errors.all(), function(k,v){
                arr.push('<li>'+v+'</li>');
            });
            
            notify({
                type: 'error',
                title: title, 
                text: '<ul>'+arr.join("")+'</ul>',
            });
            
            $.each(errors.items, function(n,m){
                $('[name='+m.field+']').parent().addClass('has-error');
            });
            
            $('.has-error').on('click', function(){
                $(this).removeClass('has-error');
            });
        },
        err(errors, notify, title)
        {
            var arr = [];
            $.each(errors, function(k,v){
                arr.push('<li>'+v+'</li>');
            });
            
            notify({
                type: 'error',
                title: title, 
                text: '<ul>'+arr.join("")+'</ul>',
            });
        },
        errLocale(errors, notify, title)
        {
            var arr = [];
            if(errors.response.status == 442)
            {
                $.each(errors.response.data[currentLang], function(k,v){
                    arr.push('<li>'+v+'</li>');
                });
            }else
            {
                arr.push('<li>'+errors.response.statusText+'</li>');
            }

            notify({
                type: 'error',
                title: title,
                text: '<ul>'+arr.join("")+'</ul>',
            });
        },
        axiosErr(errors, notify, i18n)
        {
            var arr = [];
            if(errors.response.status == 442)
            {
                if(_.isArray(errors.response.data.error))
                {
                    _.forEach(errors.response.data.error, function (v) {
                        arr.push('<li>'+v+'</li>');
                    });
                }
                else
                {
                    arr.push('<li>'+i18n.t(errors.response.data.error)+'</li>');
                }

            }else
            {
                arr.push('<li>'+errors.response.statusText+'</li>');
            }

            notify({
                type: 'error',
                title: i18n.t('error'),
                text: '<ul>'+arr.join("")+'</ul>',
            });
        }
    }
    
    Object.defineProperties(Vue.prototype, {
        $msg: {
            get() {
                return Vue.msg
            }
        }
    });
}