const Validate = {
    install(Vue, options) {

        //var session = Vue.prototype.$session;
        var notify = Vue.prototype.$notify;
        var i18n = options.i18n;
        var currentLang = options.currentLang;
        var EventBus = options.EventBus;
        var elements = [];
        var errorsKey = [];

        EventBus.$on('lang', function (data) {
            currentLang = data;
        });

        var locale = {
            uk:{
                req: "Поле :field обов'язково для заповнення.",
                min: "Поле :field має бути не менше :characters символів.",
                email: "Поле :field має містити @ та крапку.",
                confirm: "Поля паролей не співпадають.",
                my: "Поле :field вказано не вірно."
            },
            en:{
                req: "Field :field is required.",
                min: "Field: field must be at least :characters characters.",
                email: "Field :field should contain @ and a dot.",
                confirm: "Password fields do not match.",
                my: "Field :field is indicated not right."
            },
            ru:{
                req: "Поле :field обязательно для заполнения.",
                min: "Поле :field должно быть не менее :characters символов.",
                email: "Поле :field должно содержать @  и точку.",
                confirm: "Поля паролей не совпадают.",
                my: "Поле :field указано не верно."
            },
        };

        var errors = [];

        Vue.prototype.$validate = {
            rules (rules, data) {
                var vm = this;
                errors = [];
                errorsKey = [];
                //
                _.forEach(rules, function (rule, key) {

                    if(rule.required)
                    {
                        if(_.isNull(data[key]))
                        {
                            errors.push(vm.trans(rule.label, 'req'));
                            errorsKey.push(key);
                        }
                        //
                        if(_.isArray(data[key]) && data[key].length == 0)
                        {
                            errors.push(vm.trans(rule.label, 'req'));
                            errorsKey.push(key);
                        }
                        //
                        if(key.indexOf('.') !== -1)
                        {
                            var ar = key.split('.');
                            if(ar.length == 2)
                            {
                                if(data[ar[0]][ar[1]] == undefined)
                                {
                                    errors.push(vm.trans(rule.label, 'req'));
                                    errorsKey.push(key);
                                }
                            }
                        }
                        //
                        if(_.isObject(data[key]))
                        {
                            var e = false;
                            _.forEach(data[key], function (v) {
                                if(!v)
                                {
                                    e = true;
                                }
                            })

                            if(e)
                            {
                                errors.push(vm.trans(rule.label, 'req'));
                                errorsKey.push(key);
                            }
                        }
                    }
                    //
                    if(rule.min)
                    {
                        if(data[key])
                        {
                            if(data[key].length < rule.min)
                            {
                                errors.push(vm.trans(rule.label, 'min', rule.min));
                                errorsKey.push(key);
                            }
                        }
                    }
                    //
                    if(rule.clearHtml)
                    {
                        var str = data[key];
                        if(str)
                        {
                            var noHtml = str.replace(/<[^>]+>/ig, '');
                            if(!noHtml.length)
                            {
                                errors.push(vm.trans(rule.label, 'req'));
                                errorsKey.push(key);
                            }
                        }
                    }
                    //
                    if(rule.default)
                    {
                        var ar = rule.default.split('|');
                        if(ar.indexOf(data[key]) !== -1)
                        {
                            errors.push(vm.trans(rule.label, 'req'));
                            errorsKey.push(key);
                        }
                    }
                    //
                    if(rule.dyn && rule.required)
                    {
                        if(data[key] == undefined)
                        {
                            errors.push(vm.trans(rule.label, 'req'));
                            errorsKey.push(key);
                        }
                    }

                });
                //
                //console.log(data);
                //console.log(rules);
                //console.log(errors);

                if(errors.length > 0)
                {
                    vm.setErrors(errors);
                    return false;
                }
                else
                {
                    vm.clearError();
                    return true;
                }
            },
            setErrors(errors){
                var li = [];
                _.forEach(errors, function (err) {
                    li.push('<li>'+err+'</li>');
                })
                //
                notify({
                    type: 'error',
                    title: i18n.t('error'),
                    text: '<ul>'+li.join("")+'</ul>',
                });
                //
                _.forEach(errorsKey, function (v) {
                    $(elements[v]).addClass('has-error');
                    $(elements[v]).on('click', function () {
                        $(this).removeClass('has-error');
                    });
                });
            },
            clearError(){
                _.forEach(elements, function (v) {
                    $(v).removeClass('has-error');
                });
            },
            trans (field, type, characters = null) {
                var f = '"'+i18n.t(field)+'"';
                if(characters)
                {
                    return locale[currentLang][type].replace(/:field/, f).replace(/:characters/, characters);
                }
                else
                {
                    return locale[currentLang][type].replace(/:field/, f);
                }
            }
        }

        Vue.directive('errors', {
            bind : function (el, binding) {
                elements[binding.value] = el;
            }
        });

    }
};

export default Validate;