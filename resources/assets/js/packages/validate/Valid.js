
export default function (Vue)
{
    var fields = [];
    var errors = [];

    var locale = {
        uk:{
            req: "Поле :field обов'язково для заповнення.",
        },
        en:{
            req: "Field :field is required.",
        },
        ru:{
            req: "Поле :field обязательно для заполнения.",
        },
    };

    var currentLang = localStorage.getItem('currentLang');

    Vue.perevirka = {
        validate(scope)
        {
            errors = [];
            $('.has-error').removeClass('has-error');
            fields.forEach(function (item, index) {
                //console.log(item);
                //console.log(document.forms[scope][item.el.name].value);
                if(item.binding.arg === 'req')
                {
                    // text
                    if(item.type == 'text')
                    {
                        if(document.forms[scope][item.name].value == '')
                        {
                            $('[name="'+item.name+'"]').parent().addClass('has-error');
                            errors.push(trans(item.el.dataset.vvAs, 'req'));
                        }
                    }
                    // select-multiple
                    if(item.type == 'select-multiple')
                    {
                        if(document.forms[scope][item.name].value == '')
                        {
                            $('[name="'+item.name+'"]').parent().addClass('has-error');
                            errors.push(trans(item.el.dataset.vvAs, 'req'));
                        }
                    }
                    // select-one
                    if(item.type == 'select-one')
                    {
                        if(document.forms[scope][item.name].value == '')
                        {
                            $('[name="'+item.name+'"]').parent().addClass('has-error');
                            errors.push(trans(item.el.dataset.vvAs, 'req'));
                        }
                    }
                    // checkbox
                    if(item.type == 'checkbox')
                    {
                        var countCheck = [];
                        var options = document.getElementsByName(item.name);
                        options.forEach(function (p1, p2, p3) {
                            if(p1.checked == true)
                            {
                                countCheck.push(p1.value);
                            }
                        })
                        if(countCheck.length == 0)
                        {
                            $('[name="'+item.name+'"]').parent().addClass('has-error');
                            errors.push(trans(item.el.dataset.vvAs, 'req'));
                        }
                    }
                    // radio
                    if(item.type == 'radio')
                    {
                        var countCheck = [];
                        var options = document.getElementsByName(item.name);
                        options.forEach(function (p1, p2, p3) {
                            if(p1.checked == true)
                            {
                                countCheck.push(p1.value);
                            }
                        })
                        if(countCheck.length == 0)
                        {
                            $('[name="'+item.name+'"]').parent().addClass('has-error');
                            errors.push(trans(item.el.dataset.vvAs, 'req'));
                        }
                    }


                }
            })

            if(errors.length > 0)
            {
                return false;
            }
            else{
                return true;
            }

        },
        getErrors() {
            $('.has-error').on('click', function () {
                //var cbx = $(this).find('[type="checkbox"]').get(0).name;
                //$('[name="'+cbx+'"]').parent().removeClass('has-error');
                //var rad = $(this).find('[type="radio"]').get(0).name;
                //$('[name="'+rad+'"]').parent().removeClass('has-error');
                $(this).removeClass('has-error');
            });
            return errors;
        }
    };

    function trans(field, type) {
        var f = '"'+field+'"';
        return locale[currentLang][type].replace(/:field/, f);
    }
    

    Vue.directive('perevirka', {
        inserted: function (el, binding) {
            fields.push({el: el, binding: binding, type:el.type, name: el.name });
        }
    });



    Object.defineProperties(Vue.prototype, {
        $perevirka: {
            get() {
                return Vue.perevirka
            }
        }
    });
}