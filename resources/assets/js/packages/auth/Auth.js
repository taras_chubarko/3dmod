window.axios = require('axios');

export default function (Vue)
{
    Vue.auth = {
        setToken(token, expiration) {
            localStorage.setItem('token', token);
            localStorage.setItem('expiration', expiration);
        },
        //
        getToken() {
            var token = localStorage.getItem('token');
            var expiration = localStorage.getItem('expiration');
            //
            if (!token || !expiration ) {
                return null;
            }
            //
            var ts = Math.round((new Date()).getTime() / 1000);
            if (ts > parseInt(expiration)) {
                this.destroyToken();
                return null;
            }
            else
            {
                return token;
            }

        },
        //
        destroyToken() {
            localStorage.removeItem('token');
            localStorage.removeItem('expiration');
            localStorage.removeItem('geo');
            //window.location.href = '/'; 
        },
        //
        isAuth() {
            //if (this.getToken()) {
            if(window.Laravel){
                return true;
            }
            else{
                return false;
            }
        },
        whatch() {
            var expiration = localStorage.getItem('expiration');
            var logout = false;

            function checksession() {
                var ts = Math.round((new Date()).getTime() / 1000);
                if (ts >= parseInt(expiration)) {
                    logout = true;
                }
            }

            function out() {
                if(logout)
                {
                    axios.post('/api/v1/user/logout').then(response => {
                        localStorage.removeItem('token');
                        localStorage.removeItem('expiration');
                        localStorage.removeItem('geo');
                        logout = false;
                        window.location.href = '/';
                    });
                }
            } out();

            checksession();

            setInterval(function () {
                checksession();
                out();
            }, 3000);


        },
        getUser()
        {
            var user = [];
            if (window.Laravel) {
                user = Laravel.user;
                return user;
            }
            else{
                return false;
            }
        },
        logout()
        {
            localStorage.removeItem('token');
            localStorage.removeItem('expiration');
            localStorage.removeItem('geo');
        },
        inRole(role)
        {
            if(Laravel)
            {
                var val = false;
                _.forEach(Laravel.roles, function (v) {
                    if(v.name == role)
                    {
                        val = true;
                    }
                    else
                    {
                        val = false;
                    }
                });
                return val;
            }
        }
    }

    Object.defineProperties(Vue.prototype, {
        $auth: {
            get() {
                return Vue.auth
            }
        }
    });
}