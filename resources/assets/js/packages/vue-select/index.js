import VuseSelectOne from './VuseSelectOne.vue';
import { events }    from './events';

const VueSelect = {
    install(Vue, options) {
        Vue.component('v-select-one', VuseSelectOne);
    }
};

export default VueSelect;


