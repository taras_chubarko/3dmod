var app = require('http').createServer()
var io = require('socket.io')(app);
var fs = require('fs');

var Redis = require('ioredis');
var redis = new Redis();

redis.subscribe('notifi.1');

redis.on('message', function(channel, message) {
    const event = JSON.parse(message);
    //io.emit(event.event, channel, event.data);
    io.emit(event.event, channel, event.data);
});

io.on('connection', function (socket) {
    console.log('A user connected');
    
});

app.listen(8010);

