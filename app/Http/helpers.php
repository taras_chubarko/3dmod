<?php
use Khill\Duration\Duration;

function get_duration($path)
{
    $client = new \Spatie\Dropbox\Client(config('app.dropbox_token'));
    $media = $client->rpcEndpointRequest('files/get_metadata', ['path' => $path, 'include_media_info' => true]);

    $duration = new Duration($media['media_info']['metadata']['duration']/1000);

    $duration = $duration->formatted();
    $duration = explode(':', $duration);

    if(!empty($duration[2]))
    {
        $duration = implode(':', $duration);
    }
    elseif(!empty($duration[1]))
    {
        $duration = implode(':', $duration);
    }
    else
    {
        $duration = '0:'.$duration[0];
    }

    return $duration;
}

function format_duration($secs, $delimiter = ':')
{
    $seconds = $secs % 60;
    //$minutes = floor($secs / 60);
    //$hours   = floor($secs / 3600);

    $seconds = str_pad($seconds, 2, "0", STR_PAD_LEFT);
    //$minutes = str_pad($minutes, 2, "0", STR_PAD_LEFT).$delimiter;
    //$hours   = ($hours > 0) ? str_pad($hours, 2, "0", STR_PAD_LEFT).$delimiter : '';

    return $seconds;
}
