<?php

namespace App\Http\Middleware;

use Closure;

class AuthMDW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (\Sentinel::guest())
        {
            return redirect()->guest('/');
        }
        else
        {
            return $next($request);
        }

        //return $next($request);
    }
}
